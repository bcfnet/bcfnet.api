<?php

class Dados_perfil_model extends CI_Model {

    private $tableName =  'dados_perfil';

	public function __construct() {
		parent::__construct();
	}

	public function fetch($id) {
        $db = $this->load->database('bcfadm', true);
        $query = $db->get_where($this->tableName, ['CodigoCliente' => $id]);
        $result = $query->row();

        return count($result) > 0 ? $result : [];
    }

    public function update($login, $data = []) {
        $db = $this->load->database('bcfadm', true);
        $id = empty(!$data['CodigoCliente']) ? $data['CodigoCliente'] : null;

        if (!empty($id)) {
            $query = $db->get_where($this->tableName, ['CodigoCliente' => $id]);
            $result = $query->row();

            if (!empty($result)) {
                // update
                $db->where('CodigoCliente', $id);
            
                foreach($data as $key => $value) {
                    if (!empty($value)) {
                        $db->set($key, $value);
                    }
                }

                $db->update($this->tableName);

                if($db->trans_status() === true){
                    $db->trans_commit();
                    return [
                        'CodigoCliente' => $id,
                        'error' => 0,
                        'status' => 'O Dado de Perfil foi atualizado.'
                    ];
                }else{
                    $db->trans_rollback();
                    return [
                        'CodigoCliente' => $id,
                        'error' => 0,
                        'status' => 'O Dado de Perfil não pôde ser atualizado.'
                    ];
                }

            } else {
                $inserted = $db->insert($this->tableName, $data);

                if ($inserted) {
                    return [
                        'CodigoCliente' => $id,
                        'error' => 0,
                        'status' => 'O Dado de Perfil foi atualizado.'
                    ];
                }
            }
        }

        return [
            'CodigoCliente' => 0,
            'error' => 1,
            'status' => 'O Dado de Perfil não pôde ser atualizado.'
        ];
    }

}

?>