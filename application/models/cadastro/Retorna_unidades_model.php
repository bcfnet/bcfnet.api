<?php

class Retorna_unidades_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login) {
		$query = $this->db->query("EXEC SITE_RetornaUnidades '{$login}'");

		$result = $query->result();

		return $result;
	}

}

?>