<?php

class Administrativo_model extends CI_Model {

    private $types = array(
		0 => "admin",
		1 => "sindico",
		2 => "condomino",
		3 => "proprietario",
		4 => "locacao",
		5 => "administracao",
	);

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $categoria, $subcategoria) {
        $db = $this->load->database('bcfadm', true);
        $login_type = $this->get_login_type($login);
        $arvore = $categoria."#".$subcategoria."#";
        $today = date('d/m/Y');


    	switch ($login_type)
    	{
    		case "condomino":
    			$query = "SELECT * FROM arquivos WHERE arvore REGEXP '$arvore' AND arvore NOT LIKE '%#Devedores#%' AND STR_TO_DATE(data_expiracao, '%d/%m/%Y') >= STR_TO_DATE('".$today."', '%d/%m/%Y') AND ( (login_internet = '{$login}' AND status != 'C') OR (login_internet = '{$login}' AND permissao = 'P') )";
    		break;
    		case "sindico":
                case "gerente":
    			$query = "SELECT * FROM arquivos WHERE arvore REGEXP '$arvore' AND STR_TO_DATE(data_expiracao, '%d/%m/%Y') >= STR_TO_DATE('".$today."', '%d/%m/%Y') AND ( (login_internet = '{$login}' OR login_internet = '{$login}') AND status != 'C')";
    		break;
            case "admin":
                    $query = "SELECT * FROM arquivos WHERE arvore REGEXP '$arvore' AND STR_TO_DATE(data_expiracao, '%d/%m/%Y') >= STR_TO_DATE('".$today."', '%d/%m/%Y') AND ( (login_internet = '{$login}' OR login_internet = '{$login}' OR SUBSTRING(login_internet, 1, 4) = '".$login."' ) AND status != 'C')";
            break;
            case "locacao":
                    $query = "SELECT * from arquivos WHERE arvore REGEXP '$arvore' AND STR_TO_DATE(data_expiracao, '%d/%m/%Y') >= STR_TO_DATE('".$today."', '%d/%m/%Y') AND login_internet = '{$login}' AND status != 'C'";
            break;
            default:
            $query = "SELECT * FROM arquivos WHERE arvore REGEXP '$arvore' AND STR_TO_DATE(data_expiracao, '%d/%m/%Y') >= STR_TO_DATE('".$today."', '%d/%m/%Y') AND ( (login_internet = '{$login}' OR login_internet = '{$login}') AND status != 'C')";
            break;
    	}

        $orderBy = " ORDER BY STR_TO_DATE(data_inicial, '%d/%m/%Y') DESC";

        $query .= $orderBy;

    	$query = $db->query($query);

    	return $query->result();
    }
    
    private function get_login_type($login) {
		$type = $this->types[5];
		$char = mb_substr($login, 0, 1);

		if ($login === "bcfadm") {
			$type = $this->types[0];
		} else {
			$type = $this->types[$char];
		}
		return $type;
	}

}

?>