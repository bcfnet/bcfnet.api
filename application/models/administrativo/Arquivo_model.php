<?php

class Arquivo_model extends CI_Model {

    private $tableName =  'arquivos';

    private $types = array(
		0 => "admin",
		1 => "sindico",
		2 => "condomino",
		3 => "proprietario",
		4 => "locacao",
		5 => "administracao",
	);

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $id = null) {
        $db = $this->load->database('bcfadm', true);
        $login_type = $this->get_login_type($login);

        return $this->get_all_files_by_login($login, $login_type, true, $id);
    }

    public function create($login, $data = []) {

        $db = $this->load->database('bcfadm', true);

        $data = [
            'login_internet' => $login,
            'arvore' => implode('#', [$data['pasta'], $data['tipo'], $data['descricao']]),
            'permissao' => $data['acesso'],
            'data_expiracao' => $data['data_expiracao'],
            'data_inicial' => $data['data_inicial'],
            'data_final' => $data['data_final'],
            'data_inclusao' => $data['data_inclusao'],
            'status' => $data['status'],
            'campo_tipo' => '4',
            'fromSite' => 1,
            'link_arquivo' => !empty($data['link_arquivo']) ? $data['link_arquivo'] : ''
            
        ];

        if (!empty($data['link_arquivo'])) {
            $upload = $this->upload_file($data['link_arquivo']);
            
            if (empty($upload['error'])) {
                $data['link_arquivo'] = str_replace('./', '', $upload['filepath']);
            }
        }

        $inserted = $db->insert($this->tableName, $data);

        return [
            'id' => $db->insert_id(),
            'error' => 0,
            'status' => $inserted
        ];
    }

    public function update($login, $id, $data = []) {
        $db = $this->load->database('bcfadm', true);

        $data = [
            'login_internet' => $login,
            'arvore' => implode('#', [$data['pasta'], $data['tipo'], $data['descricao']]),
            'permissao' => $data['acesso'],
            'data_expiracao' => $data['data_expiracao'],
            'data_inicial' => $data['data_inicial'],
            'data_final' => $data['data_final'],
            'data_inclusao' => $data['data_inclusao'],
            'status' => $data['status'],
            'campo_tipo' => '4',
            'fromSite' => 1,
            'link_arquivo' => !empty($data['link_arquivo']) ? $data['link_arquivo'] : ''
            
        ];
        
        if (!empty($id)) {
            $db->where('id', $id);
            
            foreach($data as $key => $value) {
                if (!empty($value)) {
                    $db->set($key, $value);
                }
            }

            if (!empty($data['link_arquivo'])) {
                /* if (file_exists($data['link_arquivo'])) {
                    unlink($data['link_arquivo']);
                } */

                $upload = $this->upload_file($data['link_arquivo']);
                
                if (empty($upload['error'])) {
                    $db->set('link_arquivo', str_replace('./', '', $upload['filepath']));
                }
            }

            $db->update($this->tableName);

            if($db->trans_status() === true){
                $db->trans_commit();
                return [
                    'id' => $id,
                    'error' => 0,
                    'status' => 'Documento foi atualizado com sucesso'
                ];
            }else{
                $db->trans_rollback();
                return [
                    'id' => $id,
                    'error' => 1,
                    'status' => 'Documento não foi atualizado'
                ];
            }
        } else {
            return [
                'id' => $id,
                'error' => 0
            ];
        }
    }

    public function delete($login, $id) {
        $db = $this->load->database('bcfadm', true);

        $query = $db->get_where($this->tableName, ['id' => $id, 'login_internet' => $login]);
        $result = $query->row();

        if (!empty($result)) {
            if (isset($result->link_arquivo)) {
                if (file_exists($result->link_arquivo)) {
                    unlink($result->link_arquivo);
                }
            }

            $deleted = $db->delete($this->tableName, ['id' => $id]);
            return [
                'error' => 0,
                'status' => $deleted
            ];
        }
        
        return [
            'error' =>1
        ];
    }

    private function upload_file($link_arquivo) {
        $path = './uploads/';

        if ( ! is_dir($path)) {
            mkdir($path, 0777, $recursive = true);
        }

        $allowedType = ['image/jpeg', 'application/pdf'];

        if (strpos($link_arquivo, 'data:image') !== false) {
            $extension = 'jpg';
            $base64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['link_arquivo']));
        } else if (strpos($link_arquivo, 'data:application/pdf') !== false) {
            $extension = 'pdf';
            $base64 = base64_decode(preg_replace('#^data:application/\w+;base64,#i', '', $_POST['link_arquivo']));
        } else {
            return [
                'error' => 1,
                'message' => 'O Anexo fornecido não é no formato permitido. Use .jpg ou .png'
            ];
        }

        $file = './uploads/'.uniqid().'.'.$extension;
        $success = file_put_contents($file, $base64);
        $data['link_arquivo'] = $file;

        if ($success) {
            return [
                'error' => null,
                'filepath' => $file
            ];
        } else {
            return [
                'error' => "Não foi possível anexar o arquivo ao servidor",
                'filepath' => null
            ];
        }
        
    }
    
    private function get_login_type($login) {
		$type = $this->types[5];
		$char = mb_substr($login, 0, 1);

		if ($login === "bcfadm") {
			$type = $this->types[0];
		} else {
			$type = $this->types[$char];
		}
		return $type;
    }
    
    private function get_all_files_by_login($login, $login_type, $fromSite = TRUE, $id = null) {
        $db = $this->load->database('bcfadm', true);
        $today = date('d/m/Y');
        $where = '';

        if (!empty($id)) {
            $where = " AND id = {$id}";
        }

        if ($login_type == "condomino") {

        $sql = "SELECT * FROM arquivos WHERE login_internet = '{$login}' {$where}";
            
        } else {
            $sqlFromSite = "";

            if ($login_type != 'proprietario') {
                if ($fromSite) {
                    $sqlFromSite = " AND fromSite = 1";
                }
            } else if ($login_type == 'proprietario') {
                $sqlFromSite .= " AND arvore LIKE '%#Posicao da Carteira%' AND STR_TO_DATE(data_expiracao, '%d/%m/%Y') >= STR_TO_DATE('".$today."', '%d/%m/%Y') ORDER BY id DESC";
            }

            $sql = "SELECT * FROM arquivos WHERE login_internet REGEXP '{$login}' {$sqlFromSite} {$where}";
        }
        $query = $db->query($sql);  
        return $query->result();
    }

}

?>