<?php

class Mensagem_model extends CI_Model {

    private $tableName = 'mensagens';
    private $instance;
    private $types = array(
		0 => "admin",
		1 => "sindico",
		2 => "condomino",
		3 => "proprietario",
		4 => "locacao",
		5 => "gerente",
	);

	public function __construct() {
          parent::__construct();
          $this->init();
    }

    public function init() {
          $this->instance = $this->load->database('bcfadm', true);
    }

	public function fetch($login, $receiver, $id = null, $deleted = 0) {
          $where = [];
          $or_where = [];     

          if (!empty($id)) {
               $where = ['id' => $id, 'deletada' => $deleted];
          } else {
               $where = ['deletada' => $deleted];
          }

          if (!empty($receiver)) {
               $or_where = ['destinatario' => $receiver, 'login' => $login];
               $this->instance->or_where($or_where);
          }

          $this->instance->order_by('data_envio DESC');

          $query = $this->instance->get_where($this->tableName, $where);
          $result = $query->result();

          if (count($result) > 0) {      
                $results = [];      

                for ($i = 0; $i < sizeof($result); $i++) {
                        if ($deleted === $result[$i]->deletada) {
                            $results[] = $result[$i];
                        }
                }
               return $results;
          }
         
         return [];
    }

    public function create($login, $data = []) {

        if (!empty($_POST['link_arquivo'])) {
            if (!file_exists('./uploads/anexos')) {
                mkdir('./uploads/anexos');
            }

            $allowedType = ['image/jpeg', 'application/pdf'];
            $link_arquivo = $_POST['link_arquivo']; 

            if (strpos($link_arquivo, 'data:image') !== false) {
                $extension = 'jpg';
                $base64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['link_arquivo']));
            } else if (strpos($link_arquivo, 'data:application/pdf') !== false) {
                $extension = 'pdf';
                $base64 = base64_decode(preg_replace('#^data:application/\w+;base64,#i', '', $_POST['link_arquivo']));
            } else {
                return [
                    'error' => 1,
                    'status' => 'O Anexo fornecido não é no formato permitido. Use .jpg ou .png'
                ];
            }

            $file = './uploads/anexos/'.uniqid().'.'.$extension;
            $success = file_put_contents($file, $base64);
            $data['link_arquivo'] = $file;
            
            $inserted = $this->instance->insert($this->tableName, $data);
        } else {
            $inserted = $this->instance->insert($this->tableName, $data);

            if (!$inserted) {
                return [
                        'error' => 1,
                        'status' => 'A Mensagem não pôde ser publicada.'
                ];
            }
        }

        return [
            'error' => 0,
            'status' => 'A Mensagem foi publicada.'
        ];
    }
    
    public function delete($login, $id, $force = 0, $all = 0) {
        $where = ['login' => $login, 'id' => $id];

        $query = $this->instance->get_where($this->tableName, $where);
        
        $result = $query->row();

        if (!empty($result)) {
            if (isset($result->link_arquivo)) {
                if (file_exists($result->link_arquivo)) {
                    unlink($result->link_arquivo);
                }
            }
            if ($force === 1) {
                $deleted = $this->instance->delete($this->tableName, ['id' => $id]);
            } else {
                $deleted = $this->instance->update($this->tableName, ['deletada' => 1], $where);
            }

            if ($deleted) {
                return [
                        'error' => 0,
                        'status' => "A Mensagem foi $id deletada com sucesso."
                ];
            } else {
                return [
                        'error' => 1,
                        'status' => 'A Mensagem não pôde ser deletada.'
                ];
            }
        }
        
        return [
            'error' => 1,
            'status' => 'A Mensagem não pôde ser deletada.'
        ];
    }

    private function get_login_type($login) {
        $type = $this->types[5];
        $char = mb_substr($login, 0, 1);

        if (trim($login) === "bcfadm") {
            $type = $this->types[0];
        } else if(is_numeric($login)) {
            $type = $this->types[$char];
        }
        return $type;
    }

}

?>