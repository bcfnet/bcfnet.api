<?php

class Graficos_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login) {
        $db = $this->load->database('bcfadm', true);
       	$query = $db->query("SELECT * FROM graficos 
            WHERE login_condominio = '{$login}' 
			ORDER BY STR_TO_DATE(mes_referencia, '%m/%Y') DESC LIMIT 24");

        $result = $query->result();
        
		return $result;
	}

}

?>