<?php

class Gerente_model extends CI_Model {

     private $instance;
     private $tableName = 'gerentes';

	public function __construct() {
          parent::__construct();
          $this->initialize();
     }

     public function initialize() {
          $this->instance = $this->load->database('bcfadm', true);
     }

	public function fetch($login) {
          $db = $this->load->database('bcfadm', true);

          $query = $db->get_where('admin', ['login_internet' => $login]);

          $result = $query->result();
          
          if (count($result) > 0) {
               $results = null;
               $query = $db->select('*')->order_by('id DESC')->get_where($this->tableName);

               $result = $query->result();

               foreach ($result as $key => $value) {
                    $results[$key] = $value;
                    // $results[$key]['clientes'] = implode(', ', unserialize($value->clientes));
               }
               
               return $results;
          } 
          
          return [];        
     }

     public function create($login, $data = []) {

          $db = $this->load->database('bcfadm', true);
  
          if (!empty($data['clientes']) && is_array($data['clientes'])) {
               $data['clientes'] = serialize();
          } else if (!empty($data['clientes'])) {
            $data['clientes'] = serialize(explode(',', $data['clientes']));
          }

          $inserted = $db->insert($this->tableName, $data);
  
          if (!$inserted) {
              return [
                  'error' => 1,
                  'status' => 'O gerente não pôde ser cadastrado.'
              ];
          }
  
          return [
              'error' => 0,
              'status' => 'O gerente foi publicado.'
          ];
     }
    
     public function get($login, $id = null) {
          $db = $this->load->database('bcfadm', true);

          $query = $db->get_where('admin', ['login_internet' => $login]);

          $result = $query->result();
          
          if (count($result) > 0) {
               $where = ['situacao' => 'A'];

               if (!empty($id)) {
                    $where = ['situacao' => 'A', 'id' => $id];
               }

               $query = $db->select('*')->order_by('id DESC')->get_where('condominios', $where);

               $result = $query->result();

               return $result;
          } 

          return [];        
     }

     public function update($login, $id, $data = []) {
          $db = $this->load->database('bcfadm', true);
          $valid = sizeof($this->get($login)) > 0 ? true : false;

          if (!$valid) {
               return [];
          }
          
          if (!empty($id)) {
               $db->where('id', $id);
               
               foreach($data as $key => $value) {
                    if (!empty($value)) {
                         if ($key === 'clientes') {
                            if (!empty($data['clientes'] && is_array($data['clientes']))) {
                                $value = serialize($data['clientes']);
                            } else if (!empty($data['clientes'])) {
                                $value = serialize(explode(',', $data['clientes']));
                            }
                         }
                         $db->set($key, $value);
                    }
               }
     
               $db->update($this->tableName);
     
               if($db->trans_status() === true){
                    $db->trans_commit();
                    return [
                         'error' => 0,
                         'status' => "O Gerente foi atualizado."
                    ];
               } else {
                    $db->trans_rollback();
               }
          } 

          return [
               'error' => 1,
               'status' => "O Gerente não pôde ser atualizado."
          ];
      }
     
     public function delete($login, $id) {
          $db = $this->load->database('bcfadm', true);
  
          $query = $db->get_where('admin', ['login_internet' => $login]);
          $result = $query->row();
  
          if (!empty($result)) {  
              $deleted = $db->delete($this->tableName, ['id' => $id]);
              
              if ($deleted) {
                    return [
                         'error' => 0,
                         'status' => 'O Gerente foi deletado com sucesso.'
                    ];
              }
          }
          
          return [
              'error' => 1,
              'status' => 'O Gerente não pôde ser deletado.'
          ];
     }

     public function filter($login, $id) {
          $db = $this->load->database('bcfadm', true);

          $query = $db->get_where('admin', ['login_internet' => $login]);

          $result = $query->result();
          
          if (count($result) > 0) {
               $where = [];

               $db->like('clientes', $id);

               $query = $db->select('*')->order_by('nome ASC')->get_where($this->tableName, $where);

               $result = $query->result();

               return $result;
          } 

          return [];    
     }

}

?>