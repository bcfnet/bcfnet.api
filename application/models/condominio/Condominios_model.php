<?php

class Condominios_model extends CI_Model {

    private $tableName = 'condominios';

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login) {
        $db = $this->load->database('bcfadm', true);

       	$query = $db->get_where('admin', ['login_internet' => $login]);

        $result = $query->result();
        
        if (count($result) > 0) {
            $query = $db->select('id, login_internet, nome')->order_by('nome ASC')->get_where('condominios', ['situacao' => 'A']);

            $result = $query->result();
            
		    return $result;
        } else {
            $query = $db->get_where('gerentes', ['login' => $login]);

            $result = $query->result();
            
            if (count($result) > 0) {
                $condominios = implode(', ', unserialize($result[0]->clientes));

                $query = $db->select('id, login_internet, nome')->order_by('nome ASC')->get_where('condominios', "situacao = 'A' AND login_internet IN({$condominios})");

                $result = $query->result();

                return $result;

            }
            
            return [];
            //return [];
        }
        
    }
    
    public function get($login, $id = null) {
        $db = $this->load->database('bcfadm', true);

       	$query = $db->get_where('admin', ['login_internet' => $login]);

        $result = $query->result();
        
        if (count($result) > 0) {
            $where = ['situacao' => 'A'];

            if (!empty($id)) {
                $where = ['situacao' => 'A', 'id' => $id];
            }

            $query = $db->select('*')->order_by('nome ASC')->get_where('condominios', $where);

            $result = $query->result();

		    return $result;
        } 

        return [];        
	}

}

?>