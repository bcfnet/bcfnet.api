<?php

class Album_model extends CI_Model {

    private $tableName = 'imagens';
    private $instance;

	public function __construct() {
        parent::__construct();
        $this->init();
    }

    public function init() {
        $this->instance = $this->load->database('bcfadm', true);
    }

	public function fetch($login) {

       	$query = $this->instance->get_where($this->tableName, ['login_condominio' => $login]);

        $result = $query->result();
        
        if (count($result) > 0) {
            $result = $query->result();
            
		    return $result;
        }
        
        return [];
    }

    public function create($login, $data = []) {

        $db = $this->load->database('bcfadm', true);

        $data = [
            'login_condominio' => $login
        ];

        if (isset($_FILES['arquivo']) && is_array($_FILES['arquivo'])) {
            $arquivo = $_FILES['arquivo'];

            for ($i = 0; $i < sizeof($arquivo); $i++) {
                if (!empty($arquivo["tmp_name"][$i])) {

                    $upload = $this->upload_file($data, $i);
                    
                    if (empty($upload['error'])) {
                        $data['large'] = $upload['filepath'];
                        $data['small'] = $upload['filepath'];
                    }
                    $inserted = $db->insert($this->tableName, $data);
                }
            }
        } else {
            if (!empty($_POST['arquivo'])) {
                if (!file_exists('./uploads/fotos')) {
                    mkdir('./uploads/fotos');
                }
                $base64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['arquivo']));
                $file = './uploads/fotos/'.uniqid().'.jpg';
                $success = file_put_contents($file, $base64);
                $data['large'] = $file;
                $data['small'] = $file;
    
                $inserted = $db->insert($this->tableName, $data);
            } else {
                return [
                    'error' => 1,
                    'status' => 'Por gentileza, adicione uma foto para ser enviada.'
                ];
            }
            
        }

        return [
            'error' => 0,
            'status' => 'Toda(s) a(s) foto(s) foram publicadas com sucesso.'
        ];
    }
    
    public function delete($login, $id) {

        $query = $this->instance->get_where($this->tableName, ['id' => $id, 'login_condominio' => $login]);
        $result = $query->row();

        if (!empty($result)) {
            if (isset($result->link_arquivo)) {
                if (file_exists($result->link_arquivo)) {
                    unlink($result->link_arquivo);
                }
            }

            $deleted = $this->instance->delete($this->tableName, ['id' => $id]);
            return [
                'error' => 0,
                'status' => "A foto foi $id deletada com sucesso."
            ];
        }
        
        return [
            'error' => 1,
            'status' => 'Ocorreu um error ao deletar a foto.'
        ];
    }

    private function upload_file($name, $i) {
        $path = 'uploads/fotos';

        if ( ! is_dir($path)) {
            mkdir($path, 0777, $recursive = true);
        }

        // Fake upload...
        $_FILES['userfile']['name']     = $_FILES['arquivo']['name'][$i];
        $_FILES['userfile']['type']     = $_FILES['arquivo']['type'][$i];
        $_FILES['userfile']['tmp_name'] = $_FILES['arquivo']['tmp_name'][$i];
        $_FILES['userfile']['error']    = $_FILES['arquivo']['error'][$i];
        $_FILES['userfile']['size']     = $_FILES['arquivo']['size'][$i];

        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload('userfile')) {
            return [
                'error' => $this->upload->display_errors(),
                'filepath' => null
            ];
        } else {
            $data = $this->upload->data();
            $path = $config['upload_path'] . '/' . $data["file_name"];

            return [
                'error' => null,
                'filepath' => $path
            ];
        }
        
    }

    private function upload_file_single($name) {
        $path = './uploads/fotos';

        if ( ! is_dir($path)) {
            mkdir($path, 0777, $recursive = true);
        }

        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($name)) {
            return [
                'error' => $this->upload->display_errors(),
                'filepath' => null
            ];
        } else {
            $data = $this->upload->data();
            $path = $config['upload_path'] . '/' . $data["file_name"];

            return [
                'error' => null,
                'filepath' => $path
            ];
        }
        
    }

}

?>