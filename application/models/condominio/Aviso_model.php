<?php

class Aviso_model extends CI_Model {

    private $tableName = 'avisos';
    private $instance;
    private $types = array(
		0 => "admin",
		1 => "sindico",
		2 => "condomino",
		3 => "proprietario",
		4 => "locacao",
		5 => "gerente",
	);

	public function __construct() {
        parent::__construct();
        $this->init();
    }

    public function init() {
        $this->instance = $this->load->database('bcfadm', true);
    }

	public function fetch($login, $id = null) {
        $role = $this->get_login_type($login);
        $where = [];

        if ($role == 'admin') {
            $where = "condominio = '' AND condomino = ''";
        } else if ($role == 'sindico') {
            $where = "condominio = '$login'";
        } else if ($role == 'condomino') {
            $condominio = substr($login, strlen($login) + 4, strlen($login));
            $where = "condomino = '$login'  OR  condominio = '10001'";
        } else if ($role == 'gerente') {
            $where = "condominio = '' AND (visualizacao = 'proprietarios' OR visualizacao = 'todos')";
        }

        $today = date('d/m/Y');

        if (!empty($id)) {
            $where .= " AND id = $id";
        }

        $where .= " AND STR_TO_DATE(data_expiracao, '%d/%m/%Y') >= STR_TO_DATE('$today', '%d/%m/%Y')";

        $query = $this->instance->get_where($this->tableName, $where);
        $result = $query->result();
        
        if (count($result) > 0) {            
		    return $result;
        }
        
        return [];
    }

    public function create($login, $data = []) {

        $db = $this->load->database('bcfadm', true);

        $inserted = $db->insert($this->tableName, $data);

        if (!$inserted) {
            return [
                'error' => 1,
                'status' => 'O aviso não pôde ser publicado.'
            ];
        }

        return [
            'error' => 0,
            'status' => 'O aviso foi publicado.'
        ];
    }

    public function update($login, $id, $data = []) {

        if (!empty($id)) {
            $this->instance->where('id', $id);
            
            foreach($data as $key => $value) {
                if (!empty($value)) {
                    $this->instance->set($key, $value);
                }
            }

            $this->instance->update($this->tableName);

            if($this->instance->trans_status() === true){
                $this->instance->trans_commit();
                return [
                    'id' => $id,
                    'status' => "O Aviso $id foi atualizado com sucesso.",
                    'error' => 0
                ];
            } else {
                $this->instance->trans_rollback();
                return [
                    'id' => $id,
                    'status' => "O Aviso $id não pôde ser atualizado.",
                    'error' => 1
                ];
            }
        } else {
            return [
                'id' => $id,
                'status' => "O Aviso $id não pôde ser atualizado.",
                'error' => 1
            ];
        }
    }
    
    public function delete($login, $id) {
        $role = $this->get_login_type($login);
        $where = [];

        if ($role == "sindico" || $role == "gerente") {
            $where = ['id' => $id, 'condominio' => $login];
        } else if ($role == "admin") {
            $where = ['id' => $id];
        } else {
            $where = ['id' => $id, 'condomino' => $login];
        }

        $query = $this->instance->get_where($this->tableName, $where);
        
        $result = $query->row();

        if (!empty($result)) {
            if (isset($result->link_arquivo)) {
                if (file_exists($result->link_arquivo)) {
                    unlink($result->link_arquivo);
                }
            }

            $deleted = $this->instance->delete($this->tableName, ['id' => $id]);
            return [
                'error' => 0,
                'status' => "O aviso $id deletado com sucesso."
            ];
        }
        
        return [
            'error' => 1,
            'status' => 'O aviso não pôde ser deletado.'
        ];
    }

    public function get_total_avisos_administradora($login) {
        $role = $this->get_login_type($login);

        if ($role == "sindico" || $role == "admin" || $role = "gerente") {
            $strMySQL = "SELECT * FROM avisos WHERE condominio = '' AND  condomino = '' AND (visualizacao = 'sindicos' OR visualizacao = 'todos')";
        }
        else if ($role == "sindico") {
            $strMySQL = "SELECT * FROM avisos WHERE condominio = '' AND  condomino = '' AND (visualizacao = 'condominos' OR visualizacao = 'todos')";
        }
        else if ($role == "locacao") {
            $strMySQL = "SELECT * FROM avisos WHERE condominio = '' AND  condomino = '' AND (visualizacao = 'inquilinos' OR visualizacao = 'todos')";
        }

        $today = date('d/m/Y');

        $strMySQL .= " AND STR_TO_DATE(data_expiracao, '%d/%m/%Y') >= STR_TO_DATE('".$today."', '%d/%m/%Y') ORDER BY STR_TO_DATE(data_inclusao, '%d/%m/%Y')";

        $query = $this->instance->query($strMySQL);
        
        return [
            'Total' => count($query->result()),
            'Avisos' => $query->result()
        ];
    }

    public function get_total_avisos_condominio($login) {
        $role = $this->get_login_type($login);

        if ($role == "sindico" || $role == "admin" || $role = "gerente") {
            $sql = "SELECT * FROM avisos WHERE condominio = '$login' AND visualizacao = ''";
        }
        else if ($role == "condomino") {
            $sql = "SELECT * FROM avisos WHERE condominio = '$login' AND visualizacao = ''";
        }
        else if ($role == 'locacao') {
            $sql = "SELECT * FROM avisos WHERE condominio = '' AND (visualizacao = 'proprietarios' OR visualizacao = 'todos')";
        }/*
        else if ($tipoLogin == 6) {
            $strMySQL = "SELECT * FROM avisos WHERE condominio = '' AND (visualizacao = 'inquilinos' OR visualizacao = 'todos')";
        } */
        
        $today = date('d/m/Y');

        $sql .= " AND STR_TO_DATE(data_expiracao, '%d/%m/%Y') >= STR_TO_DATE('".$today."', '%d/%m/%Y') ORDER BY STR_TO_DATE(data_inclusao, '%d/%m/%Y')";

        $query = $this->instance->query($sql);

        return [
            'Total' => count($query->result()),
            'Avisos' => $query->result()
        ];
    }

    private function get_login_type($login) {
        $type = $this->types[5];
        $char = mb_substr($login, 0, 1);

        if (trim($login) === "bcfadm") {
            $type = $this->types[0];
        } else if(is_numeric($login)) {
            $type = $this->types[$char];
        }
        return $type;
    }

}

?>