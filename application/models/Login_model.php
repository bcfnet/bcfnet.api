<?php

class Login_model extends CI_Model {

	private $types = array(
		0 => "admin",
		1 => "sindico",
		2 => "condomino",
		3 => "proprietario",
		4 => "locacao",
		5 => "gerente",
	);

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $password) {

		$query = $this->db->query("EXEC SITE_LoginSenha '$login', 1");

		$result = $query->result();
		
		if ($this->is_admin($login)) {
			$result = $this->login($login, $password);
		}

		return $result;
	}

	private function login($login, $password, $type = 0) {
		$db = $this->load->database('bcfadm', true);
		$type = $this->get_login_type($type);

		switch($type) {
			case 'admin':
				$query = $db->get_where('admin', [
					'login_internet' => $login,
					'senha' => $password
				]);
				$permission = 0;
			break;
			case 'locacao':
				$query = $db->get_where('locacao', [
					'login_internet' => $login,
					'senha' => $password
				]);
				$permission = 4;
			break;
			case 'gerente':
				$query = $db->get_where('gerentes', [
					'login' => $login,
					'senha' => $password
				]);
				$permission = 5;
			break;
		}

		$result = $query->result();
		
		if (sizeof($result) > 0) {
			$result[0]->SituacaoCliente = 'A';
			$result[0]->CodigoCliente = $result[0]->id;
			$result[0]->NomeCliente = $result[0]->nome;
			$result[0]->NomeCondomino = $result[0]->nome;
			$result[0]->Senha = trim($password);
			$result[0]->SenhaUnidadeIM = trim($password);
			$result[0]->Telefone = '';
			$result[0]->E_Mail = '';
			$result[0]->NomeSindico = $result[0]->nome;
			$result[0]->NomeGerente = $result[0]->nome;
			$result[0]->Ramal = '';
			$result[0]->Permissao = $permission;
		}
		
		return $result;
	}
	

	private function get_login_type($login) {
		$type = $this->types[5];
		$char = mb_substr($login, 0, 1);

		if (trim($login) === "bcfadm") {
			$type = $this->types[0];
		} else if(is_numeric($login)) {
			$type = $this->types[$char];
		}
		return $type;
	}

	private function is_admin($login) {
		return $this->get_login_type($login) == 'admin';
	}

	private function is_locacao($login) {
		$db = $this->load->database('bcfadm', true);

		$char = mb_substr($login, 0, 1);
		
		if ($char == 3) {
			return;
		}

		$query = $db->get_where('locacao', [
			'login_internet' => trim($login)
		]);

		$result = $query->result();

		return count($result) > 0;
	}

	private function is_gerente($login) {
		// verificar se ele compara com o numero 5... implementar depois
		$db = $this->load->database('bcfadm', true);

		$query = $db->get_where('gerentes', [
			'login' => trim($login)
		]);

		$result = $query->result();
		return count($result) > 0;
		/*$result[0]->SituacaoCliente = 'A';
		$result[0]->CodigoCliente = $result[0]->id;
		$result[0]->NomeCliente = $result[0]->nome;
		$result[0]->NomeCondomino = $result[0]->nome;
		$result[0]->Senha = trim($result[0]->senha);
		$result[0]->SenhaUnidadeIM = trim($result[0]->senha);
		$result[0]->Telefone = $result[0]->telefone_movel;
		$result[0]->E_Mail = '';
		$result[0]->NomeSindico = $result[0]->nome;
		$result[0]->NomeGerente = $result[0]->nome;
		$result[0]->Ramal = '';
		
		return $result; */
	}

}

?>
