<?php

class Balancete_recpag_operacional_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $date) {
		$query = $this->db->query("EXEC SITE_RecPagOperacional '{$login}', '{$date}'");

		$result = $query->result();

		return $result;
	}

}

?>