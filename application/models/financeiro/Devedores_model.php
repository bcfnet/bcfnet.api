<?php

class Devedores_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $date) {
		$query = $this->db->query("
		IF (SELECT OBJECT_ID('TempDb..#SiteDevedores')) IS NULL
		  CREATE TABLE #SiteDevedores(CodigoFilial        VARCHAR(2)  NULL,
		   NomeFilial          VARCHAR(80) COLLATE Latin1_General_CI_AI NULL,
		   CodigoGerente       VARCHAR(4)  NULL,
		   NomeGerente         VARCHAR(80) COLLATE Latin1_General_CI_AI NULL,
		   CodigoCliente       VARCHAR(5)  NULL,
		   NomeCliente         VARCHAR(80) COLLATE Latin1_General_CI_AI NULL,
		   CodigoUnidade       VARCHAR(4)  NULL,
		   ComplementoUnidade  VARCHAR(50) COLLATE Latin1_General_CI_AI NULL,
		   NomeCondomino       VARCHAR(80) COLLATE Latin1_General_CI_AI NULL,
		   Telefone            VARCHAR(50) COLLATE Latin1_General_CI_AI NULL,
		   NumeroRecibo        INT         NULL,
		   DataVencimento      DATETIME    NULL,
		   SequenciaRateio     INT         NULL,
		   ValorRecibo         MONEY       NULL,
		   SituacaoCobrancaCMP VARCHAR(50) COLLATE Latin1_General_CI_AI NULL, 
		   SituacaoCobrancaCPL VARCHAR(50) COLLATE Latin1_General_CI_AI NULL)

		EXEC SITE_Devedores '".$login."', '".$date."'");

		$result = $query->result();

		return $result;
	}

}

?>