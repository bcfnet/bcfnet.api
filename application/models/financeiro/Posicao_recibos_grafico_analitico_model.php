<?php

class Posicao_recibos_grafico_analitico_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $date, $type) {
		$query = $this->db->query("EXEC SITE_PosicaoRecibosGraficoAnalitico '{$login}', '{$date}', '{$type}'");

		$result = $query->result();

		return $result;
	}

}

?>