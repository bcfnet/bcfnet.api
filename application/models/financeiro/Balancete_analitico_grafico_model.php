<?php

class Balancete_analitico_grafico_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $date, $type, $unit) {
		$query = $this->db->query("EXEC SITE_BalanceteAnaliticoGrafico '{$login}', '{$date}', '{$type}', '{$unit}'");

		$result = $query->result();

		return $result;
	}

}

?>