<?php

class Balancete_semi_sintetico_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $date, $type) {
		$query = $this->db->query("EXEC SITE_BalanceteSemiSintetico '{$login}', '{$date}', '{$type}'");

		$result = $query->result();

		return $result;
	}

}

?>