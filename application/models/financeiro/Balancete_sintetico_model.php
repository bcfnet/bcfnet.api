<?php

class Balancete_sintetico_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $date) {
		$query = $this->db->query(
		"IF (SELECT OBJECT_ID('TempDb..#FechamentoSaldos')) IS NULL 
		CREATE TABLE #FechamentoSaldos(Cliente       VARCHAR(5) NULL,
			Conta         VARCHAR(3) NULL,
			SaldoAnterior MONEY      NULL,
			Credito       MONEY      NULL,
			Debito        MONEY      NULL,
			SaldoFinal    MONEY      NULL)
		DELETE FROM #FechamentoSaldos

		IF (SELECT OBJECT_ID('TempDb..#TempBalanceteSintetico')) IS NULL 
		CREATE TABLE #TempBalanceteSintetico (Filial          VARCHAR(2) NULL,
			Gerente         VARCHAR(4) NULL,
			Cliente         VARCHAR(5) NULL,
			Conta           VARCHAR(3) NULL,
			Classificacao   VARCHAR(9) NULL,
			Tipo            INT NULL,
			OrdFechamento   INT NULL,
			Descricao       VARCHAR(255) COLLATE Latin1_General_CI_AS NULL,
			Valor           MONEY        NULL,
			TipoLancamento  CHAR(1)      NULL,
			DiaLancamento   INT          NULL,
			HistoricoPadrao VARCHAR(4)   NULL,
			Referencia      VARCHAR(7)   NULL,
			ComplUnidade    VARCHAR(200) COLLATE Latin1_General_CI_AS NULL)

		EXEC SITE_BalanceteSintetico '".$login."', '".$date."'");

		$result = $query->result();

		return $result;
	}

}

?>