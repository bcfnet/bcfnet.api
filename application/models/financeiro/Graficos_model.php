<?php

class Graficos_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $n = 12) {
		
		$months = array();
		$results = [];
		for ($i = 0; $i < $n; $i++) {
			$timestamp = mktime(0, 0, 0, date('n') - $i, 1);
			$date = date('m/Y', $timestamp);

			$query = $this->db->query("EXEC SITE_Graficos '{$login}', '{$date}'");
			$result = $query->result();
			$results[] = ['Mes_Referencia' => $date, 'Dados' => $result];
		}

		return $results;
	}

}

?>