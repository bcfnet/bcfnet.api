<?php

class Saldo_final_seis_meses_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $date) {
		$query = $this->db->query("EXEC SITE_SaldoFinal6Meses '{$login}', '{$date}'");

		$result = $query->result();

		return $result;
	}

}

?>