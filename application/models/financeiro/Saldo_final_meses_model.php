<?php

class Saldo_final_meses_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $date, $total) {
		$query = $this->db->query("EXEC SITE_SaldoFinalMeses '{$login}', '{$date}', {$total}");

		$result = $query->result();

		return $result;
	}

}

?>