<?php

class Balancete_analitico_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetch($login, $date) {
		$query = $this->db->query(
		"IF (SELECT OBJECT_ID('TempDb..#TempRecPagClass')) IS NULL
   CREATE TABLE #TempRecPagClass (Filial          VARCHAR(2)    NULL,
                                  Gerente         VARCHAR(4)     NULL,
                                  Cliente         VARCHAR(5)     NULL,
                                  Unidade         VARCHAR(80)    COLLATE Latin1_General_CI_AI NULL,
                                  Conta           VARCHAR(3)     NULL,
                                  Classificacao   VARCHAR(9)     NULL,
                                  Tipo            INT            NULL,
                                  OrdFechamento   INT            NULL,
                                  Descricao       VARCHAR(MAX)  COLLATE Latin1_General_CI_AI NULL,
                                  Descricao2      VARCHAR(MAX)  COLLATE Latin1_General_CI_AI NULL,
                                  Valor           MONEY          NULL,
                                  TipoLancamento  CHAR(1)        NULL,
                                  DiaLancamento   INT            NULL,
                                  HistoricoPadrao VARCHAR(4)     NULL,
                                  Referencia      VARCHAR(7)     NULL,
                                  ComplUnidade    VARCHAR(MAX)   COLLATE Latin1_General_CI_AI NULL,
                                  AgrupaReceita   BIT            NULL,
                                  Canal           VARCHAR(2)     NULL,
                                  Job             VARCHAR(12)    NULL,
                                  MesAno          VARCHAR(20)    NULL,
                                  ClienteOrigem   VARCHAR(5)     NULL)

IF (SELECT OBJECT_ID('TempDb..#ClientesAleatorios')) IS NULL
  CREATE TABLE #ClientesAleatorios (CodigoCliente VARCHAR(5)  NULL,
                                    NomeCliente   VARCHAR(80) NULL)

IF (SELECT OBJECT_ID('TempDb..#ClientesPermitidos')) IS NULL 
  CREATE TABLE #ClientesPermitidos (CodigoCliente VARCHAR(5), 
                                    FilialAtual   VARCHAR(2)) 

EXEC SITE_BalanceteAnalitico '".$login."', '".$date."'

SELECT TMP.Conta,
       TMP.Classificacao,
       TMP.Tipo,
       TMP.OrdFechamento,
       TMP.ClienteOrigem,
       TMP.Unidade,
       TMP.DiaLancamento,
    CASE 
       WHEN ISNULL(TMP.DiaLancamento, 0) <> 0 THEN
       SUBSTRING(LTRIM(RTRIM(ISNULL(TMP.Descricao, ''))), 4, LEN(RTRIM(TMP.Descricao)))
    ELSE RTRIM(TMP.Descricao)
    END As Descricao,
       CONVERT(CHAR(20), ' ') As SaldoInicial,
       CONVERT(CHAR(20), ' ') As Credito,
       CONVERT(CHAR(20), ' ') As Debito,
       TMP.ComplUnidade As ComplUnidade,
       TMP.Valor,
       TMP.TipoLancamento
FROM #TempRecPagClass TMP
     LEFT OUTER JOIN #ClientesPermitidos PR ON
     TMP.Cliente = PR.CodigoCliente 
     LEFT OUTER JOIN #ClientesAleatorios CA ON
     TMP.Cliente = CA.CodigoCliente 
WHERE ISNULL(TMP.Cliente, '') <> ''
  AND ISNULL(PR.CodigoCliente, '') <> ''
  AND ISNULL(CA.CodigoCliente, '') <> ''
  AND ISNULL(TMP.Tipo, 0) <> 2

UNION

SELECT TMP.Conta,
       TMP.Classificacao,
       TMP.Tipo,
       TMP.OrdFechamento,
       TMP.ClienteOrigem,
       TMP.Unidade,
       TMP.DiaLancamento,
       CONVERT(CHAR(65), SUBSTRING(RTRIM(TMP.Descricao),  1, 65))  As Descricao,
       CONVERT(CHAR(20), SUBSTRING(RTRIM(TMP.Descricao), 66, 20))  As SaldoInicial,
       CONVERT(CHAR(20), SUBSTRING(RTRIM(TMP.Descricao), 87, 21))  As Credito,
       CONVERT(CHAR(20), SUBSTRING(RTRIM(TMP.Descricao), 108, 21)) As Debito,
       TMP.ComplUnidade as ComplUnidade,
       TMP.Valor,
       TMP.TipoLancamento
FROM #TempRecPagClass TMP
     LEFT OUTER JOIN #ClientesPermitidos PR ON
     TMP.Cliente = PR.CodigoCliente 
     LEFT OUTER JOIN #ClientesAleatorios CA ON
     TMP.Cliente = CA.CodigoCliente 
WHERE ISNULL(TMP.Cliente, '') <> ''
  AND ISNULL(PR.CodigoCliente, '') <> ''
  AND ISNULL(CA.CodigoCliente, '') <> ''
  AND ISNULL(TMP.Tipo, 0) = 2
ORDER BY TMP.Conta,
         TMP.Tipo,
         TMP.Classificacao,
         TMP.OrdFechamento,
         TMP.ClienteOrigem,
         TMP.Unidade,
         TMP.DiaLancamento,
         Descricao");

		$result = $query->result();

		return $result;
	}

}

?>