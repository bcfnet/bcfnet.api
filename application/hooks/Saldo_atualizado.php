<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';

class Saldo_atualizado extends REST_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index_get()	{
		$this->load->model('financeiro/saldo_atualizado_model', 'saldo_atualizado');

		//print_r($this->saldo_atualizado->fetch());
		$saldo = $this->saldo_atualizado->fetch();

		echo mb_convert_encoding($saldo[0]->Conta, 'UCS-2LE', mb_detect_encoding($saldo[0]->Conta, mb_detect_order(), true));
	}
}