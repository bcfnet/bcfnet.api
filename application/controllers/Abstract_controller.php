<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';

class Abstract_Controller extends REST_Controller {

	protected $login;
	protected $token;
	protected $date;
	protected $permissions = array(1, 2);

	public function __construct() {
		parent::__construct();

		$this->login = $this->input->get('login');
		$this->token = $this->input->get('token');
		$this->month = $this->input->get('month');
		$this->year = $this->input->get('year');

		$this->date = $this->input->get('month') && $this->input->get('year') ? $this->input->get('month') . '/' . $this->input->get('year') : (date('m') . '/' . date('Y'));

		if($this->router->fetch_class() != 'login'){
			/*if($this->validate()){
				$this->_permission($this->permissions);
			} else {
				echo json_encode(array('status' => 403, 'message' => 'Acess unauthorized via browser'));
				exit;
			} */
		}

		

	}

	private function validate(){
		if(!empty($this->token)){
			$data = JWT::decode($this->token, JWT_SECRET_KEY);
			if($data->login == $this->login){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function _permission($level = array()){
		if(count($level) == 0 || count($level) == 2){
			return true;
		} else {
			for($i = 0; $i < count($level); $i++){
				if($level[$i] == 1){
					if(substr($this->login, 0, 1) == 1){
						return true;
					} else {
						exit;
					}
				} else if($level[$i] == 2){
					if(substr($this->login, 0, 1) == 2){
						return true;
					} else {
						exit;
					}
				}
			}
		}
	}

	protected function _encode_items($arr) {

		$data = [];

		foreach ($arr as $k => $v) {
			if (is_array($v)) {
				$data[$k] = $this->_encode_items($v);
			} else if (is_object($v)) {
				$data[$k] = $this->_encode_items(get_object_vars($v));
			} else {
				$key = mb_convert_encoding($k, 'Windows-1252', 'UTF-8');
				//$key = $k;
				$data[$key] = utf8_encode($v);
			}

		}

		return $data;
	}

	protected function _decode_items($arr){
		$data = [];

		foreach($arr as $k => $v){
			if(is_array($v)){
				$data[$k] = $this->_decode_items($arr);
			} else {
				$data[$k] = iconv(mb_detect_encoding($v, mb_detect_order(), true), "UTF-8",$v);
			}
		}
		return $data;
	}
}
