<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Departamento_pessoal extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /administrativo/departamento_pessoal Departamento Pessoal
     * @apiName departamento_pessoal
     * @apiGroup administrativo
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {String} token JWT_TOKEN
     * @apiParam {String} subcategoria Subcategoria (Ex.: Informações)
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     *  [{"id":"1629655","sequencial":"000002","campo_tipo":"4","login_internet":"10001","arvore":"Departamento Pessoal#Informa\u00c3\u00a7\u00c3\u00b5es#Boletim de Horas Extras","data_referencia":"","permissao":"R","data_expiracao":"09\/07\/2019","data_inclusao":"","data_inicial":" ","data_final":" ","status":"A","link_arquivo":"htmls\/0001BHE.PDF","fromSite":"0"},{"id":"1629656","sequencial":"000003","campo_tipo":"4","login_internet":"10001","arvore":"Departamento Pessoal#Informa\u00c3\u00a7\u00c3\u00b5es#Escala de revezamento","data_referencia":"","permissao":"R","data_expiracao":"09\/07\/2019","data_inclusao":"","data_inicial":" ","data_final":" ","status":"A","link_arquivo":"htmls\/0001EDR.PDF","fromSite":"0"},{"id":"1629657","sequencial":"000004","campo_tipo":"4","login_internet":"10001","arvore":"Departamento Pessoal#Informa\u00c3\u00a7\u00c3\u00b5es#Ficha Cadastral","data_referencia":"","permissao":"R","data_expiracao":"09\/07\/2019","data_inclusao":"","data_inicial":" ","data_final":" ","status":"A","link_arquivo":"htmls\/0001FCD.PDF","fromSite":"0"},{"id":"1629658","sequencial":"000005","campo_tipo":"4","login_internet":"10001","arvore":"Departamento Pessoal#Informa\u00c3\u00a7\u00c3\u00b5es#Folha de Pagamento","data_referencia":"","permissao":"R","data_expiracao":"09\/07\/2019","data_inclusao":"","data_inicial":" ","data_final":" ","status":"A","link_arquivo":"htmls\/0001FPG.PDF","fromSite":"0"},{"id":"1629659","sequencial":"000006","campo_tipo":"4","login_internet":"10001","arvore":"Departamento Pessoal#Informa\u00c3\u00a7\u00c3\u00b5es#Quadro de Hor\u00c3\u00a1rios","data_referencia":"","permissao":"R","data_expiracao":"09\/07\/2019","data_inclusao":"","data_inicial":" ","data_final":" ","status":"A","link_arquivo":"htmls\/0001QHT.PDF","fromSite":"0"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/administrativo/departamento_pessoal?login=10001&token=$YOUR_JWT_TOKEN&subcategoria=Informações
     */
	public function index_get()	{
		$this->load->model('administrativo/administrativo_model', 'administrativo');

		$data = $this->_encode_items($this->administrativo->fetch($this->login, 'Departamento Pessoal', $this->input->get('subcategoria')));

        echo json_encode($data);
        // http://www.bcfadm.com.br/bcfnet/uploads/5df25e4bcaab3b2808e9be703681c993.pdf
		exit;
	}
}