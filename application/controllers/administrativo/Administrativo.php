<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Administrativo extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /administrativo/administrativo Administrativo
     * @apiName administrativo
     * @apiGroup administrativo
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {String} token JWT_TOKEN
     * @apiParam {String} subcategoria Subcategoria (Ex.: Convocação de Assembleia)
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"id":"2895082","sequencial":"","campo_tipo":"4","login_internet":"10001","arvore":"Administrativo#Convoca\u00c3\u00a7\u00c3\u00a3o de Assembl\u00c3\u00a9ias#Edital de Convoca\u00c3\u00a7\u00c3\u00a3o para AGO no dia 01\/03\/2018","data_referencia":"","permissao":"P","data_expiracao":"25\/02\/2019","data_inclusao":"26\/02\/2018","data_inicial":"","data_final":"","status":"A","link_arquivo":"uploads\/3f5944edb66c3598397f249767da8ed2.pdf","fromSite":"1"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/administrativo/administrativo?login=10001&token=$YOUR_JWT_TOKEN&subcategoria=Atas%20de%20Assembléia
     */
	public function index_get()	{
		$this->load->model('administrativo/administrativo_model', 'administrativo');

		$data = $this->_encode_items($this->administrativo->fetch($this->login, 'Administrativo', $this->input->get('subcategoria')));

        echo json_encode($data);
        // http://www.bcfadm.com.br/bcfnet/uploads/5df25e4bcaab3b2808e9be703681c993.pdf
		exit;
	}
}