<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Arquivo extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /administrativo/arquivo Arquivo
     * @apiName arquivo
     * @apiGroup administrativo
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {Number} id ID do arquivo (opcional)
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"id":"2895082","sequencial":"","campo_tipo":"4","login_internet":"10001","arvore":"Administrativo#Convoca\u00c3\u00a7\u00c3\u00a3o de Assembl\u00c3\u00a9ias#Edital de Convoca\u00c3\u00a7\u00c3\u00a3o para AGO no dia 01\/03\/2018","data_referencia":"","permissao":"P","data_expiracao":"25\/02\/2019","data_inclusao":"26\/02\/2018","data_inicial":"","data_final":"","status":"A","link_arquivo":"uploads\/3f5944edb66c3598397f249767da8ed2.pdf","fromSite":"1"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/administrativo/arquivo?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('administrativo/arquivo_model', 'arquivo');

		$data = $this->_encode_items($this->arquivo->fetch($this->login, $this->input->get('id')));

        echo json_encode($data);
		exit;
    }
    /**
     * @api {post} /administrativo/arquivo/create Arquivo - Criar
     * @apiName arquivo_create
     * @apiGroup administrativo
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {String} pasta Pasta
     * @apiParam {String} tipo Tipo do Arquivo
     * @apiParam {String} descricao Descrição
     * @apiParam {String} permissao Permissão (Exemplo: P)
     * @apiParam {String} data_expiracao Data de Expiração (Formato: DD/MM/YYYY)
     * @apiParam {String} data_inicial Data Inicial (Formato: DD/MM/YYYY)
     * @apiParam {String} data_final Data Final (Formato: DD/MM/YYYY)
     * @apiParam {String} data_inclusao Data Inclusão (Formato: DD/MM/YYYY)
     * @apiParam {String} status Status (Formato: A ou C)
     * @apiParam {File} arquivo Arquivo (File) (Opcional)
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"id":"2922825","status":"1"}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/administrativo/arquivo/create?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function create_post() {
        $this->load->model('administrativo/arquivo_model', 'arquivo');

        $data = [
            'pasta' => $this->input->post('pasta'),
            'tipo' => $this->input->post('tipo'),
            'descricao' => $this->input->post('descricao'),
            'acesso' => $this->input->post('permissao'),
            'data_expiracao' => $this->input->post('data_expiracao'),
            'data_inicial' => $this->input->post('data_inicial'),
            'data_final' => $this->input->post('data_final'),
            'data_inclusao' => $this->input->post('data_inclusao'),
            'status' => $this->input->post('status'),
            'link_arquivo' => $this->input->post('link_arquivo'),
            'campo_tipo' => '4',
            'fromSite' => 1
        ];
        

		$data = $this->_encode_items($this->arquivo->create(empty($this->input->post('login')) ? $this->login : $this->input->post('login'), $data));

        echo json_encode($data);
		exit;
    }

    /**
     * @api {post} /administrativo/arquivo/update Arquivo - Atualizar
     * @apiName arquivo_update
     * @apiGroup administrativo
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiparam {Number} id ID do Arquivo
     * @apiParam {String} pasta Pasta
     * @apiParam {String} tipo Tipo do Arquivo
     * @apiParam {String} descricao Descrição
     * @apiParam {String} permissao Permissão (Exemplo: P)
     * @apiParam {String} data_expiracao Data de Expiração (Formato: DD/MM/YYYY)
     * @apiParam {String} data_inicial Data Inicial (Formato: DD/MM/YYYY)
     * @apiParam {String} data_final Data Final (Formato: DD/MM/YYYY)
     * @apiParam {String} data_inclusao Data Inclusão (Formato: DD/MM/YYYY)
     * @apiParam {String} status Status (Formato: A ou C)
     * @apiParam {String} token JWT_TOKEN
     * @apiParam {File} arquivo Arquivo (File) (Opcional)
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"error":"0","status":"1"}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/administrativo/arquivo/update?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function update_post() {
        $this->load->model('administrativo/arquivo_model', 'arquivo');

        $data = [
            'pasta' => $this->input->post('pasta'),
            'tipo' => $this->input->post('tipo'),
            'descricao' => $this->input->post('descricao'),
            'acesso' => $this->input->post('permissao'),
            'data_expiracao' => $this->input->post('data_expiracao'),
            'data_inicial' => $this->input->post('data_inicial'),
            'data_final' => $this->input->post('data_final'),
            'data_inclusao' => $this->input->post('data_inclusao'),
            'status' => $this->input->post('status'),
            'link_arquivo' => $this->input->post('link_arquivo'),
            'campo_tipo' => '4',    
            'fromSite' => 1
        ];

		$data = $this->_encode_items($this->arquivo->update(empty($this->input->post('login')) ? $this->login : $this->input->post('login'), $this->input->post('id'), $data));

        echo json_encode($data);
		exit;
    }

    /**
     * @api {post} /administrativo/arquivo/delete Arquivo - Deletar
     * @apiName arquivo_delete
     * @apiGroup administrativo
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {String} id ID do Arquivo
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"error":"0","status":"1"}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/administrativo/arquivo/delete?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function delete_post() {
        $this->load->model('administrativo/arquivo_model', 'arquivo');

		$data = $this->_encode_items($this->arquivo->delete(empty($this->input->post('login')) ? $this->login : $this->input->post('login'), $this->input->post('id')));

        echo json_encode($data);
		exit;
    }
}