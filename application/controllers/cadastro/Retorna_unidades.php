<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Retorna_unidades extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

    /**
     * @api {get} /cadastro/retorna_unidades Listar unidades do condomínio
     * @apiName retorna_unidades
     * @apiGroup cadastro
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"CodigoCliente":"00001","NomeCliente":"DONA NINA","EnderecoCondominio":"AV. HENRIQUE DUMONT, N\u00a7 152","Senha":"12WW","NomeSindico":"ANA CHRISTINA ADRI\u00c7O RODRIGUES CARVALHO","Email":"a.chriscarvalho@gmail.com","Tel_Sindico":"9 8661-2960","Codigo_da_Unidade":"0001","Complemento_Unidade":"101","Nome_do_Cliente":"DANIEL BAR E OUTROS","LoginUnidade":"200010001","SenhaUnidade":"4041","e_MailCondomino":"robertobar@globo.com"}]
     * 
     * @apiVersion 0.1.0
     * 
     * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/cadastro/retorna_unidades?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
        $this->load->model('cadastro/retorna_unidades_model', 'retorna_unidades');

		$data = $this->_encode_items($this->retorna_unidades->fetch($this->login));

		echo json_encode($data);
		exit;
	}
}