<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Dados_perfil extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
    }
    
    /**
     * @api {update} /cadastro/dados_perfil Dados Perfil
     * @apiName dados_perfil
     * @apiGroup cadastro
     * 
     * @apiParam {Number} CodigoCliente Código Cliente
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"CodigoCliente":"00001","NomeCliente":"DONA NINA","CPF_CNPJ":"01412380707","E_Mail":"carlos.eduardo@bcfadm.com.br","Telefone":"3543-290","Celular":"","Senha":"","CEP":"","Tipo_logradouro":"","Enderecos":"","Numero":"","Complemento":"","Bairro":"","Estado":"","Cidade":"","dt_update":"2018-04-18 13:11:56"}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/cadastro/dados_perfil/?CodigoCliente=00001&login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('cadastro/dados_perfil_model', 'dados_perfil');

        $data = $this->_encode_items($this->dados_perfil->fetch($this->input->get('login')));

        echo json_encode($data);
		exit;
    }

	/**
     * @api {update} /cadastro/dados_perfil Dados Perfil - Atualizar
     * @apiName dados_perfil_update
     * @apiGroup cadastro
     * 
     * @apiParam {Number} CodigoCliente Código Cliente
     * @apiParam {String} NomeCliente Nome do Cliente
     * @apiParam {String} CPF_CNPJ CPF ou CNPJ do Cliente
     * @apiParam {String} E_Mail E-mail para contato
     * @apiParam {String} Telefone Telefone para contato
     * @apiParam {String} Celular Celular para contato
     * @apiParam {String} Senha Senha de acesso
     * @apiParam {String} CEP CEP
     * @apiParam {String} Tipo_logradouro Tipo de Logradouro
     * @apiParam {String} Enderecos Endereço
     * @apiParam {String} Numero Número da residência
     * @apiParam {String} Complemento Complemento da residência
     * @apiParam {String} Bairro Bairro em que reside
     * @apiParam {String} Estado Estado em que reside
     * @apiParam {String} Cidade Cidade em que recide
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"CodigoCliente":"00001","error":"0", "message": "O Dado de Perfil foi atualizado"}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/cadastro/dados_perfil/update?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function update_post()	{
		$this->load->model('cadastro/dados_perfil_model', 'dados_perfil');

        $data = [
            'CodigoCliente' => $this->input->post('CodigoCliente'),
            'NomeCliente' => $this->input->post('NomeCliente'),
            'CPF_CNPJ' => $this->input->post('CPF_CNPJ'),
            'E_Mail' => $this->input->post('E_Mail'),
            'Telefone' => $this->input->post('Telefone'),
            'Celular' => $this->input->post('Celular'),
            'Senha' => $this->input->post('Senha'),
            'CEP' => $this->input->post('CEP'),
            'Tipo_logradouro' => $this->input->post('Tipo_logradouro'),
            'Enderecos' => $this->input->post('Enderecos'),
            'Numero' => $this->input->post('Numero'),
            'Complemento' => $this->input->post('Complemento'),
            'Bairro' => $this->input->post('Bairro'),
            'Estado' => $this->input->post('Estado'),
            'Cidade' => $this->input->post('Cidade'),
            'dt_update' => date("Y-m-d H:i:s", time())
        ];

        $data = $this->_encode_items($this->dados_perfil->update($this->login, $data));

        echo json_encode($data);
		exit;
    }
}