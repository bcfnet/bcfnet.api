<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Formas_pagamento extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
    }
    
    /**
     * @api {update} /cadastro/formas_pagamento Forma de Pagamento
     * @apiName formas_pagamento
     * @apiGroup cadastro
     * 
     * @apiParam {Number} CodigoCliente Código Cliente
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * []
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/cadastro/formas_pagamento/?CodigoCliente=00001&login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('cadastro/formas_pagamento_model', 'formas_pagamento');

        $data = $this->_encode_items($this->formas_pagamento->fetch($this->input->get('CodigoCliente')));

        echo json_encode($data);
		exit;
    }

	/**
     * @api {update} /cadastro/formas_pagamento Forma de Pagamento - Atualizar
     * @apiName formas_pagamento_update
     * @apiGroup cadastro
     * 
     * @apiParam {Number} CodigoCliente Código Cliente
     * @apiParam {String} NomeCliente Nome do Cliente
     * @apiParam {String} CPF_CNPJ CPF ou CNPJ do Cliente
     * @apiParam {String} Banco Nome do Banco (Ex.: Santander, Bradesco)
     * @apiParam {String} Banco_Digito Dígito do Banco (033, representa o Santander)
     * @apiParam {String} Agencia Agência do Titular da Conta
     * @apiParam {String} Digito_Agencia Dígito da Agência do Titular da Conta
     * @apiParam {String} Conta Conta Corrente do Titular
     * @apiParam {String} Digito_Conta Dígito da Conta Corrente do Titular
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"CodigoCliente":"00001","error":"0", "message": "A Forma de Pagamento foi atualizada"}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/cadastro/formas_pagamento/update?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function update_post()	{
		$this->load->model('cadastro/formas_pagamento_model', 'formas_pagamento');

        $data = [
            'CodigoCliente' => $this->input->post('CodigoCliente'),
            'NomeCliente' => $this->input->post('NomeCliente'),
            'CPF_CNPJ' => $this->input->post('CPF_CNPJ'),
            'Banco' => $this->input->post('Banco'),
            'Banco_Digito' => $this->input->post('Banco_Digito'),
            'Agencia' => $this->input->post('Agencia'),
            'Digito_Agencia' => $this->input->post('Digito_Agencia'),
            'Conta' => $this->input->post('Conta'),
            'Digito_Conta' => $this->input->post('Digito_Conta'),
            'dt_update' => date("Y-m-d H:i:s", time())
        ];

        $data = $this->_encode_items($this->formas_pagamento->update($this->login, $data));

        echo json_encode($data);
		exit;
    }
}