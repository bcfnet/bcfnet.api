<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Esqueci_minha_senha extends Abstract_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('email');
    }
    
    /**
     * @api {post} /esqueci_minha_senha Esqueci Minha Senha
     * @apiName esqueci_minha_senha
     * @apiGroup esqueci_minha_senha
     * 
     * @apiParam {Number} login Login do Usuário
     * 
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {"success":true, "error": ""}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -X POST -F 'login=10001' -i http://condolog.com.br/bcfnet/api/esqueci_minha_senha?token=$YOUR_JWT_TOKEN
     */

	public function index_post()	{
		$login = $this->input->post('login');

		$this->load->model('esqueci_minha_senha_model');

        $loginData = $this->esqueci_minha_senha_model->fetch($login);

		if (isset($loginData[0])) {
			$user = $loginData[0];
			
			if ($user->SituacaoCliente == 'A') {                
                $this->email->initialize();

                $this->email->from('no-reply@bcfadm.com.br', 'BCFNet');
                $this->email->to('diego.lopes.2402@gmail.com');
                // $this->email->to($user->e_MailSindico);

                $this->email->subject('BCFNet - Recuperação de Senha');
                $this->email->message(sprintf('Olá, %s, sua senha é <b>%s</b>.', $user->NomeCliente, $user->Senha));  

                $result = $this->email->send();

                var_dump($result);

                echo json_encode(['success' => $result, 'error' => '']); // todo: return result
                // echo $this->email->print_debugger();                
				exit;
			}
		} else {

		echo json_encode(array('success' => FALSE));
		exit;
		}
	}
}
