<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Swap extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {post} /swap Swap - Troca de Usuários temporária
     * @apiName swap
     * @apiGroup swap
     * 
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {Number} login_internet Login do condomínio
     * @apiParam {String} status Status do Switch (Precisa ser 'S' se NÃO QUISER RETORNAR COMO ADMINISTRADOR ou 'B' - BACK PARA RETORNAR COMO ADMINISTRADOR) 
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"success":"1","codigo":"00001","condominio":"DONA NINA","gerente":"Cl\u00c3\u00a1udio Bittencourt","telefone_gerente":"3543-2908","ramal_gerente":"","email_gerente":"claudio.bittencourt@bcfadm.com.br","sindico":"ANA CHRISTINA ADRI\u00c3\u0083O RODRIGUES CARVALHO","condomino":"","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjAwMDAxIiwibG9naW4iOiIxMDAwMSJ9.VNg4MR8RuQcyuGZex_zD5OqjyPDSAW1odfnwTkQeGY8"}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/swap?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_post()	{
        $this->load->model('swap_model', 'swap');
        $this->login = empty($this->input->post('login')) ? $this->login : $this->input->post('login');
        $this->login_internet = $this->input->post('login_internet');
        $this->status = $this->input->post('status');

        $data = $this->_encode_items($this->swap->fetch($this->login, $this->login_internet, $this->status));

        if (isset($data[0])) {
			$user = $data[0];
			
			if ($user['SituacaoCliente'] == 'A' && !empty($this->status) ) {
				$data = array(
					'success' => TRUE,
					'codigo' => $user['CodigoCliente'],
					'condominio' => $user['NomeCliente'],
					'gerente' => $user['NomeGerente'],
					'telefone_gerente' => $user['Telefone'],
					'ramal_gerente' => $user['Ramal'],
					'email_gerente' => $user['E_Mail'],
					'sindico' => $user['NomeSindico'],
                    'condomino' => $user['NomeCondomino'],
					'senha' => $user['Senha'],
					'permissao' => isset($user->Permissao) ? $user->Permissao : '', 
					'token' => JWT::encode(array(
						'id' => $user['CodigoCliente'],
						'login' => $this->login_internet
					), JWT_SECRET_KEY)
				);
				$data = json_encode($this->_encode_items($data));

				echo $data;
				exit;
			}
		} else {

		echo json_encode(array('success' => FALSE));
		exit;
        } 
    }
}