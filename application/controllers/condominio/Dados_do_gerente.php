<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Dados_do_gerente extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /condominio/dados_do_gerente Dados do gerente
     * @apiName dados_do_gerente
     * @apiGroup condominio
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"CodigoFilial":"01","NomeFilial":"Centro","CodigoGerente":"0125","NomeGerente":"Cl\u00e1udio Bittencourt","Telefone":"3543-2908","E_Mail":"claudio.bittencourt@bcfadm.com.br"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/dados_do_gerente?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('condominio/dados_do_gerente_model', 'dados_do_gerente');

		$data = $this->_encode_items($this->dados_do_gerente->fetch($this->login));

		echo json_encode($data);
		exit;
	}
}