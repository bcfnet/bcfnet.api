<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Mensagem extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /condominio/mensagem Mensagem
     * @apiName mensagem
     * @apiGroup mensagem
     * 
     * @apiParam {Number} login Login do Remetente (Obrigatório)
     * @apiParam {Number} destinatario Login do Destinatário (Obrigatório)
     * @apiParam {Number} id ID da Mensagem (opcional)
     * @apiParam {Number} deleted Status da Mensagem
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"id":"325","titulo":"Teste Titulo","data_inclusao":"26\/07\/2018","data_expiracao":"10\/10\/2018","visualizacao":"todos","mensagem":"Esse \u00c3\u00a9 um teste somente...","condominio":"10001","condomino":""},{"id":"326","titulo":"Teste Titulo","data_inclusao":"26\/07\/2018","data_expiracao":"10\/10\/2018","visualizacao":"","mensagem":"Esse \u00c3\u00a9 um teste somente...","condominio":"10001","condomino":""},{"id":"335","titulo":"NOVO TITULO 100","data_inclusao":"27\/07\/2018","data_expiracao":"10\/10\/2018","visualizacao":"todos","mensagem":"MENSAGEM NOVO","condominio":"10001","condomino":""}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/mensagem?destinatario=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('condominio/mensagem_model', 'mensagem');
        
		$data = $this->_encode_items($this->mensagem->fetch($this->login, $this->input->get('destinatario'), $this->input->get('id'), is_null($this->input->get('deleted')) ? 0 : $this->input->get('deleted')));

		echo json_encode($data);
		exit;
    }
    
    /**
     * @api {post} /condominio/mensagem/create Mensagem - Criar
     * @apiName criar_mensagem
     * @apiGroup mensagem
     * 
     * @apiParam {Number} login Login
     * @apiParam {Number} destinatario Destinatário
     * @apiParam {String} nome Nome Completo
     * @apiParam {String} email E-mail
     * @apiParam {String} assunto Assunto
     * @apiParam {String} mensagem Mensagem
     * @apiParam {File} link_arquivo Arquivo (File só são aceitos nos formatos JPG ou PNG)
     * @apiParam {Number} mensagem_id ID da mensagem principal, criando um relacionamento 1:1.
     * @apiParam {Boolean} lida Lida (Opcional)
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"error":"0","status":"A Mensagem foi publicada."}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/mensagem/create?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function create_post() {
          $this->load->model('condominio/mensagem_model', 'mensagem');

          $data = [
               'login' => $this->login,
               'destinatario' => $this->input->post('destinatario'),
               'nome' => $this->input->post('nome'),
               'email' => $this->input->post('email'),
               'assunto' => $this->input->post('assunto'),
               'mensagem' => $this->input->post('mensagem'),
               'mensagem_id' => $this->input->post('mensagem_id'),
               'lida' => $this->input->post('lida'),
               'link_arquivo' => $this->input->post('link_arquivo')
          ];

		$data = $this->_encode_items($this->mensagem->create(empty($this->input->post('login')) ? $this->login : $this->input->post('login'), $data));

          echo json_encode($data);
		exit;
    }
    
    /**
     * @api {post} /condominio/mensagem/delete Mensagem - Deletar
     * @apiName deletar_mensagem
     * @apiGroup mensagem
     * 
     * @apiParam {Number} ID da Mensagem
     * @apiParam {Number} login Login
     * @apiParam {Boolean} all Deseja deletar todas as mensagens? 0 = Não, 1 = Sim.
     * @apiParam {Boolean} force Forçar deletar a mensagem? 0 = Não, 1 = Sim. O Não é o Padrão.
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"error":"0","status":"A mensagem $id foi deletada."}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/mensagem/delete?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function delete_post() {
          $this->load->model('condominio/mensagem_model', 'mensagem');

          $force = empty($this->input->post('force')) ? 0 : 1;
          $all = empty($this->input->post('all') ? 0 : 1);

		$data = $this->_encode_items($this->mensagem->delete(empty($this->input->post('login')) ? $this->login : $this->input->post('login'), $this->input->post('id'), $force, $all));

          echo json_encode($data);
		exit;
    }
}