<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Aviso extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /condominio/aviso Aviso
     * @apiName aviso
     * @apiGroup aviso
     * 
     * @apiParam {Number} login Login do administrador
     * @apiParam {Number} ID do aviso (opcional) - É usado caso queira retornar somente um registro
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"id":"325","titulo":"Teste Titulo","data_inclusao":"26\/07\/2018","data_expiracao":"10\/10\/2018","visualizacao":"todos","mensagem":"Esse \u00c3\u00a9 um teste somente...","condominio":"10001","condomino":""},{"id":"326","titulo":"Teste Titulo","data_inclusao":"26\/07\/2018","data_expiracao":"10\/10\/2018","visualizacao":"","mensagem":"Esse \u00c3\u00a9 um teste somente...","condominio":"10001","condomino":""},{"id":"335","titulo":"NOVO TITULO 100","data_inclusao":"27\/07\/2018","data_expiracao":"10\/10\/2018","visualizacao":"todos","mensagem":"MENSAGEM NOVO","condominio":"10001","condomino":""}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/aviso?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('condominio/aviso_model', 'aviso');
        
		$data = $this->_encode_items($this->aviso->fetch($this->login, $this->input->get('id')));

		echo json_encode($data);
		exit;
    }
    
    /**
     * @api {post} /condominio/aviso/create Aviso - Criar
     * @apiName criar_aviso
     * @apiGroup aviso
     * 
     * @apiParam {Number} login Login
     * @apiParam {String} titulo Título
     * @apiParam {String} mensagem Mensagem
     * @apiParam {Number} condominio Login do Condomínio
     * @apiParam {Number} condomino Login do Condômino
     * @apiParam {String} data_expiracao Data de Expiração (Formato: (01/07/2019)) 
     * @apiParam {String} visualizacao Visualização (todos = Todos; sindicos = Somente para os Síndicos e condominos = Somente para os Condóminos)
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"error":"0","status":"O aviso foi publicado."}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/aviso/create?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function create_post() {
        $this->load->model('condominio/aviso_model', 'aviso');

        $data = [
            'titulo' => $this->input->post('titulo'),
            'mensagem' => $this->input->post('mensagem'),
            'condominio' => $this->input->post('condominio'),
            'condomino' => $this->input->post('condomino'),
            'data_inclusao' => date('d/m/Y'),
            'data_expiracao' => empty($this->input->post('data_expiracao')) ? date('d/m/Y') : $this->input->post('data_expiracao'),
            'visualizacao' => is_null($this->input->post('visualizacao')) ? '' : $this->input->post('visualizacao'),
        ];

		$data = $this->_encode_items($this->aviso->create(empty($this->input->post('login')) ? $this->login : $this->input->post('login'), $data));

        echo json_encode($data);
		exit;
    }

    /**
     * @api {post} /condominio/aviso/update Aviso - Atualizar
     * @apiName atualizar_aviso
     * @apiGroup aviso
     * 
     * @apiParam {Number} ID ID do Aviso
     * @apiParam {Number} login Login
     * @apiParam {String} titulo Título
     * @apiParam {String} mensagem Mensagem
     * @apiParam {String} data_expiracao Data de Expiração (Formato: (01/07/2019)) 
     * @apiParam {String} visualizacao Visualização (todos = Todos; sindicos = Somente para os Síndicos e condominos = Somente para os Condóminos)
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"error":"0","status":"O aviso $id foi atualizado."}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/aviso/create?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function update_post() {
        $this->load->model('condominio/aviso_model', 'aviso');

        $data = [
            'titulo' => $this->input->post('titulo'),
            'mensagem' => $this->input->post('mensagem'),
            'data_expiracao' => empty($this->input->post('data_expiracao')) ? date('d/m/Y') : $this->input->post('data_expiracao'),
            'visualizacao' => is_null($this->input->post('visualizacao')) ? '' : $this->input->post('visualizacao'),
        ];

		$data = $this->_encode_items($this->aviso->update(empty($this->input->post('login')) ? $this->login : $this->input->post('login'), $this->input->post('id'), $data));

        echo json_encode($data);
		exit;
    }
    
    /**
     * @api {post} /condominio/aviso/update Aviso - Deletar
     * @apiName deletar_aviso
     * @apiGroup aviso
     * 
     * @apiParam {Number} ID ID do Aviso
     * @apiParam {Number} login Login
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"error":"0","status":"O aviso $id foi deletado."}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/aviso/delete?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function delete_post() {
        $this->load->model('condominio/aviso_model', 'aviso');

		$data = $this->_encode_items($this->aviso->delete(empty($this->input->post('login')) ? $this->login : $this->input->post('login'), $this->input->post('id')));

        echo json_encode($data);
		exit;
    }

    /**
     * @api {get} /condominio/aviso/get_total_avisos_administradora Aviso - Quantidade de avisos da Administradora
     * @apiName total_avisos_administradora_aviso
     * @apiGroup aviso
     * 
     * @apiParam {Number} login Login
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"Total":"1","Avisos":[{"id":"313","titulo":"TESTANDO","data_inclusao":"05\/02\/2018","data_expiracao":"01\/10\/2018","visualizacao":"todos","mensagem":"","condominio":"","condomino":""}]}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/aviso/get_total_avisos_administradora?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function get_total_avisos_administradora_get() {
        $this->load->model('condominio/aviso_model', 'aviso');

		$data = $this->_encode_items($this->aviso->get_total_avisos_administradora($this->login));

		echo json_encode($data);
		exit;
    }

    /**
     * @api {get} /condominio/aviso/get_total_avisos_condominio Aviso - Quantidade de avisos do Condomínio
     * @apiName total_avisos_condominio_aviso
     * @apiGroup aviso
     * 
     * @apiParam {Number} login Login
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"Total":"0"}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/aviso/get_total_avisos_condominio?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function get_total_avisos_condominio_get() {
        $this->load->model('condominio/aviso_model', 'aviso');

		$data = $this->_encode_items($this->aviso->get_total_avisos_condominio($this->login));

		echo json_encode($data);
		exit;
    }
}