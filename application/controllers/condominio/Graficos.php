<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Graficos extends Abstract_controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /condominio/graficos Gráficos
     * @apiName graficos
     * @apiGroup condominio
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"id":"339115","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"06\/2018","valor":"614,84"},{"id":"339116","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"06\/2018","valor":"1620,59"},{"id":"337958","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"05\/2018","valor":"1585,41"},{"id":"337957","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"05\/2018","valor":"635,32"},{"id":"336808","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"04\/2018","valor":"1685,12"},{"id":"336807","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"04\/2018","valor":"2493,34"},{"id":"335683","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"03\/2018","valor":"2197,52"},{"id":"335684","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"03\/2018","valor":"1339,69"},{"id":"334554","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"02\/2018","valor":"1533,55"},{"id":"334553","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"02\/2018","valor":"1807,79"},{"id":"333433","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"01\/2018","valor":"1468,10"},{"id":"333434","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"01\/2018","valor":"1412,13"},{"id":"332303","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"12\/2017","valor":"1449,81"},{"id":"332304","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"12\/2017","valor":"65,23"},{"id":"331190","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"11\/2017","valor":"0,00"},{"id":"331189","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"11\/2017","valor":"1170,26"},{"id":"330035","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"10\/2017","valor":"556,01"},{"id":"330036","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"10\/2017","valor":"1311,88"},{"id":"328893","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"09\/2017","valor":"545,11"},{"id":"328894","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"09\/2017","valor":"1329,73"},{"id":"327761","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"08\/2017","valor":"545,03"},{"id":"327762","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"08\/2017","valor":"1231,08"},{"id":"326615","login_condominio":"10001","titulo":"AGUA E ESGOTO","mes_referencia":"07\/2017","valor":"526,24"},{"id":"326616","login_condominio":"10001","titulo":"LUZ E FOR\u00c3\u0087A","mes_referencia":"07\/2017","valor":"1292,40"}]
     *  
     * @apiVersion 0.1.0
     * 
     * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/graficos?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('condominio/graficos_model', 'graficos');

		$data = $this->_encode_items($this->graficos->fetch($this->login));
        
        echo json_encode($data);
		exit;
	}
}
