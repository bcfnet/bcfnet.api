<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Condominios extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /condominio/condominios Condomínios
     * @apiName condominios
     * @apiGroup condominio
     * 
     * @apiParam {Number} login Login do administrador
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"CodigoFilial":"01","NomeFilial":"Centro","CodigoGerente":"0125","NomeGerente":"Cl\u00e1udio Bittencourt","Telefone":"3543-2908","E_Mail":"claudio.bittencourt@bcfadm.com.br"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/condominios?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('condominio/condominios_model', 'condominio');

		$data = $this->_encode_items($this->condominio->fetch($this->login));

		echo json_encode($data);
		exit;
     }

    /**
     * @api {get} /condominio/condominios/get Condomínios - Listar Condomínios
     * @apiName condominios
     * @apiGroup condominio
     * 
     * @apiParam {Number} login Login do administrador
     * @apiParam {Number} ID do Condomínio (opcional) - Caso deseja listar somente um específico
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"id":"11","sequencial":"000536","campo_tipo":"1","login_internet":"10040","nome":"14 DEGRAUS","situacao":"A","endereco":"RUA FREI LEANDRO, 16","bairro":"JARDIM BOT\u00c3\u0082NICO","cidade":"RIO DE JANEIRO","estado":"RJ","cep":"22470210","telefone":"","cnpj":"68583210000193","login_sindico":"200400010","senha":"8066","gerente":"0167","nome_sindico":"LEONARDO VILAIN JO\u00c3\u0083O","logotipo":"","email_preenchido":"","telefone_preenchido":"","show_welcome":""}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/condominios/get?login=bcfadm&id=11&token=$YOUR_JWT_TOKEN
     */
     public function get_get() {
          $this->load->model('condominio/condominios_model', 'condominio');

		$data = $this->_encode_items($this->condominio->get($this->login, $this->input->get('id')));

		echo json_encode($data);
		exit;
     }
}