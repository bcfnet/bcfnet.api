<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Album extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /condominio/album Álbum
     * @apiName album
     * @apiGroup condominio
     * 
     * @apiParam {Number} login Login do administrador
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"CodigoFilial":"01","NomeFilial":"Centro","CodigoGerente":"0125","NomeGerente":"Cl\u00e1udio Bittencourt","Telefone":"3543-2908","E_Mail":"claudio.bittencourt@bcfadm.com.br"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/album?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('condominio/album_model', 'album');

		$data = $this->_encode_items($this->album->fetch($this->login));

		echo json_encode($data);
		exit;
    }
    
    /**
     * @api {post} /condominio/album/create Álbum - Criar
     * @apiName album_create
     * @apiGroup album
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {File[]} arquivo Arquivo (File)
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"error":"0","status":"Toda(s) a(s) foto(s) foram publicadas com sucesso."}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/condominio/album/create?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function create_post() {
        $this->load->model('condominio/album_model', 'album');

        $data = [
            'arquivo' => $this->input->post('arquivo')
        ];
        

		$data = $this->_encode_items($this->album->create(empty($this->input->post('login')) ? $this->login : $this->input->post('login'), $data));

        echo json_encode($data);
		exit;
    }

    /**
     * @api {post} /condominio/album/delete Album - Deletar
     * @apiName album_delete
     * @apiGroup album
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {String} id ID do Arquivo
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * {"error":"0","status":"A foto foi 299 deletada com sucesso."}
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/administrativo/arquivo/delete?login=10001&token=$YOUR_JWT_TOKEN
     */
    public function delete_post() {
        $this->load->model('condominio/album_model', 'album');

		$data = $this->_encode_items($this->album->delete(empty($this->input->post('login')) ? $this->login : $this->input->post('login'), $this->input->post('id')));

        echo json_encode($data);
		exit;
    }
}