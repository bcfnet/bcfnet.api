<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use mikehaertl\wkhtmlto\Pdf as htmlToPDF;

class PDF extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->driver('cache');
        $this->load->helper('url');
    }

    public function index() {
        $content = $this->input->post('content') ?: $this->input->get('content');        
        $style = $this->input->post('style');
        $filename = $this->input->post('filename');

        //$content = preg_replace('/<!--(.|\s)*?-->/', '', $content);
        //$content = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $content);

        $style = '
            <style>
                body {
                    font-family: Arial;
                }
                .print-invisible {
                    display: none;
                }
                .hgroup .title {
                    margin-bottom: 0;
                }
                .hgroup h5.subtitle {
                    margin-top: 0;
                    font-size: 16px !important;
                    margin-bottom: 10px;
                }

                ul { width: 100%; list-style: none; padding-left: 0;margin-top:0; }
                ul li {  width:100%; }
                .balancete__header {                                        
                    /*background: red;*/
                }
                .text-right {
                    text-align: right!important;
                }
                .table {
                    width: 100%;
                    max-width: 100%;
                    margin-bottom: 70px;
                    background-color: transparent;
                }
                .table>thead>tr>th {
                    color: #515365;
                    border-bottom: 1px solid #e6ecf5;
                }
                .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                    border-color: #e6ecf5;
                }            
                .table thead th {
                    vertical-align: bottom;
                    border-bottom: 2px solid #dee2e6;
                }            
                .table th, .table td {
                    padding: 10px;
                    vertical-align: top;
                    border-top: 1px solid #dee2e6;
                }
                .table-responsive .subheading {
                    background-color: #f6f6f6;
                }
                .table-responsive .subheading td, .table-responsive .subheading td a {
                    font-weight: bold;
                }
                .table-responsive .subheading2 td {
                    text-align: center;
                    font-weight: bold !important;
                }

                table {border-spacing: 0px; }
            </style>
        ';
               
        $html = '
        <!DOCTYPE html>
        <html lang="pt-br">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>BCF Net</title>
            ' . $style . '
        </head>
        <body>
            <div style="max-width: 980px; width: 100%; margin-right: auto; margin-left: auto; background: #fff">
                ' . $content . '
            </div>
        </body>
        </html>
        ';

        @$this->cache->file->save($filename, $html, 60);
        
        echo current_url() . '/get/' . $filename;
    }

    public function get($filename, $showHtml = 'false') {
        $content = $this->cache->file->get($filename);
        
        if ($content) {
            if ($showHtml == 'false') {
                $pdf = new htmlToPDF($content);
                $pdf->send();
            } else {                
                $print = '<script>window.print();</script></body>';

                $content = str_replace('</body>', $print, $content);

                echo $content;
                die();
            }

        } else {
            echo "<script>window.close();</script>";
        }       
        
    }
}
