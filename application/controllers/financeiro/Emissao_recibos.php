<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Emissao_recibos extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/emissao_recibos Emissão Recibos
     * @apiName emissao_recibos
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} number Número do recibo para consulta
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"Campo1":"Descri\u00e7\u00e3o da Conta","Campo2":"Valor","Campo3":" "},{"Campo1":"CONDOM\u00cdNIO","Campo2":" ","Campo3":" "},{"Campo1":"R E C E I T A S","Campo2":"","Campo3":""}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/emissao_recibos?login=10001&number=1031293&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/emissao_recibos_model', 'emissao_recibos');

		$data = $this->_encode_items($this->emissao_recibos->fetch($this->login, $this->input->get('number')));

		echo json_encode($data);
		exit;
	}
}