<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Balancete_comparativo extends Abstract_controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/balancete_comparativo Balancete Comparativo
     * @apiName balancete_comparativo
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"Campo1":"Dt Complemento","Campo2":"Valor","Campo3":" ","Campo4":" "},{"Campo1":"001 - CONDOM\u00cdNIO","Campo2":" ","Campo3":" ","Campo4":" "},{"Campo1":"R E C E I T A S","Campo2":"","Campo3":"","Campo4":"0"}]
     * 
     * @apiVersion 0.1.0
     * 
     * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_comparativo?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/balancete_comparativo_model', 'balancete_comparativo');

		$data = $this->_encode_items($this->balancete_comparativo->fetch($this->login, $this->date));

		echo json_encode($data);
		exit;
	}
}
