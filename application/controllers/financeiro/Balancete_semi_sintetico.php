<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Balancete_semi_sintetico extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/balancete_semi_sintetico Balancete Semi Sintético
     * @apiName balancete_semi_sintetico
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} type Tipo de consulta: C ou D
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"ClassificacaoAnteior":"101","Descicao":"ORDIN\u00b5RIAS","Valor":"21532.19"},{"ClassificacaoAnteior":"102","Descicao":"EXTRAORDINARIAS","Valor":"35000"},{"ClassificacaoAnteior":"110","Descicao":"APLICACOES FINANCEIRAS","Valor":"5.31"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_semi_sintetico?login=10001&month=03&year=2018&type=C&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/balancete_semi_sintetico_model', 'balancete_semi_sintetico');

		$data = $this->_encode_items($this->balancete_semi_sintetico->fetch($this->login, $this->date, $this->input->get('type')));

		echo json_encode($data);
		exit;
	}
}