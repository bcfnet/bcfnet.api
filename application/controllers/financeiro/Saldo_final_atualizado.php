<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Saldo_final_atualizado extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/saldo_final_atualizado Saldo Final Atualizado
     * @apiName saldo_final_atualizado
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * [{"MesAno":"03\/2018","SaldoAnterior":"9605.43","TotalDebito":"86237.54","TotalCredito":"96592.3","SaldoFinal":"19960.19"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/saldo_final_atualizado?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/saldo_final_atualizado_model', 'saldo_final_atualizado');

		$data = $this->_encode_items($this->saldo_final_atualizado->fetch($this->login, $this->date));

		echo json_encode($data);
		exit;
	}
}