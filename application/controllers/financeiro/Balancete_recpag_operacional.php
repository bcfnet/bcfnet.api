<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Balancete_recpag_operacional extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/balancete_regpag_operacional Balancete RecPag Operacional
     * @apiName balancete_recpag_operacional
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"ClassificacaoAnteior":"101","Descicao":"ORDIN\u00b5RIAS","Valor":"21532.19"},{"ClassificacaoAnteior":"102","Descicao":"EXTRAORDINARIAS","Valor":"35000"},{"ClassificacaoAnteior":"110","Descicao":"APLICACOES FINANCEIRAS","Valor":"5.31"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_recpag_operacional?login=10001&month=03&year=2018&type=C&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/balancete_recpag_operacional_model', 'balancete_regpag_operacional');
		$data = $this->_encode_items($this->balancete_regpag_operacional->fetch($this->login, $this->date));

		echo json_encode($data);
		exit;
	}
}