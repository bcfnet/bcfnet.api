<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Devedores extends Abstract_Controller {

	protected $permissions = array(1);

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/devedores Devedores
     * @apiName devedores
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"Campo1":"Total de Unidades:","Campo2":"0","Campo3":" ","Campo4":"Total do Condom\u00ednio","Campo5":"0,00"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/devedores?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/devedores_model', 'devedores');
		$data = $this->_encode_items($this->devedores->fetch($this->login, $this->date));

		echo json_encode($data);
	}
}
