<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Balancete_sintetico extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/balancete_sintetico Balancete Sintético
     * @apiName balancete_sintetico
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"Campo1":"Descri\u00e7\u00e3o da Conta","Campo2":"Valor","Campo3":" "},{"Campo1":"CONDOM\u00cdNIO","Campo2":" ","Campo3":" "},{"Campo1":"R E C E I T A S","Campo2":"","Campo3":""}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_sintetico?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/balancete_sintetico_model', 'balancete_sintetico');

		$data = $this->_encode_items($this->balancete_sintetico->fetch($this->login, $this->date));

		echo json_encode($data);
		exit;
	}
}