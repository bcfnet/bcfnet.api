<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Graficos extends Abstract_controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/graficos Gráficos
     * @apiName graficos
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {Number} n Quantidade de Meses a serem mostradas. O padrão é 12 últimos meses.
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"Mes_Referencia":"07\/2018","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-4959.39"},{"Conta":"CONCESSIONARIAS","Valor":"-8549.65"},{"Conta":"CONTRATOS","Valor":"-30060.8"}]},{"Mes_Referencia":"06\/2018","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-14138.14"},{"Conta":"CONCESSIONARIAS","Valor":"-7968.68"},{"Conta":"CONTRATOS","Valor":"-31988.22"},{"Conta":"MATERIAIS","Valor":"-620.71"},{"Conta":"PESSOAL","Valor":"-43.9"}]},{"Mes_Referencia":"05\/2018","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-14255.09"},{"Conta":"CONCESSIONARIAS","Valor":"-7098.89"},{"Conta":"CONTRATOS","Valor":"-30677.57"},{"Conta":"MATERIAIS","Valor":"-284"},{"Conta":"PESSOAL","Valor":"-43.9"}]},{"Mes_Referencia":"04\/2018","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-18214.8"},{"Conta":"CONCESSIONARIAS","Valor":"-7664.39"},{"Conta":"CONTRATOS","Valor":"-46033.13"},{"Conta":"MATERIAIS","Valor":"-535.9"},{"Conta":"PESSOAL","Valor":"-43.9"}]},{"Mes_Referencia":"03\/2018","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-14985.84"},{"Conta":"CONCESSIONARIAS","Valor":"-5732.59"},{"Conta":"CONTRATOS","Valor":"-11874"},{"Conta":"MATERIAIS","Valor":"-295"},{"Conta":"PESSOAL","Valor":"-950.17"}]},{"Mes_Referencia":"02\/2018","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-13210.94"},{"Conta":"CONCESSIONARIAS","Valor":"-9204.72"},{"Conta":"CONTRATOS","Valor":"-31986.44"},{"Conta":"MATERIAIS","Valor":"-514"},{"Conta":"PESSOAL","Valor":"-950.17"}]},{"Mes_Referencia":"01\/2018","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-12385.74"},{"Conta":"CONCESSIONARIAS","Valor":"-12883.74"},{"Conta":"CONTRATOS","Valor":"-29253.51"},{"Conta":"MATERIAIS","Valor":"-727.2"},{"Conta":"PESSOAL","Valor":"-1626.51"}]},{"Mes_Referencia":"12\/2017","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-12728.01"},{"Conta":"CONCESSIONARIAS","Valor":"-3101.45"},{"Conta":"CONTRATOS","Valor":"-23562.04"},{"Conta":"MATERIAIS","Valor":"-328.6"},{"Conta":"PESSOAL","Valor":"-950.17"}]},{"Mes_Referencia":"11\/2017","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-27569.61"},{"Conta":"CONCESSIONARIAS","Valor":"-4949.78"},{"Conta":"CONTRATOS","Valor":"-21474.17"},{"Conta":"PESSOAL","Valor":"-1822.73"}]},{"Mes_Referencia":"10\/2017","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-13609.24"},{"Conta":"CONCESSIONARIAS","Valor":"-9895.04"},{"Conta":"CONTRATOS","Valor":"-1874"},{"Conta":"PESSOAL","Valor":"-43.9"}]},{"Mes_Referencia":"09\/2017","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-15833.31"},{"Conta":"CONCESSIONARIAS","Valor":"-5020.67"},{"Conta":"CONTRATOS","Valor":"-1874"},{"Conta":"PESSOAL","Valor":"-43.9"}]},{"Mes_Referencia":"08\/2017","Dados":[{"Conta":"ADMINISTRATIVAS","Valor":"-10834.39"},{"Conta":"CONCESSIONARIAS","Valor":"-5669.7"},{"Conta":"CONTRATOS","Valor":"-1874"}]}]
     * 
     * @apiVersion 0.1.0
     * 
     * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/graficos?login=10001&n=12&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
        $this->load->model('financeiro/graficos_model', 'graficos');
        
        $n = empty($this->input->get('n')) ? 12 : $this->input->get('n');

		$data = $this->_encode_items($this->graficos->fetch($this->login, $n));

		echo json_encode($data);
		exit;
	}
}
