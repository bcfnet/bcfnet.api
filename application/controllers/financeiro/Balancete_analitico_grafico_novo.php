<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Balancete_analitico_grafico_novo extends Abstract_controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/balancete_analitico_grafico_novo Balancete Analítico NOVO (Gráfico)
     * @apiName balancete_analitico_grafico_novo
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} type Tipo de consulta: PESSOAL, ADMINISTRATIVAS, CONCESSIONARIAS, CONTRATOS, MATERIAIS
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"Descricao":"0011-AGUA E ESGOTO 08\/2018 - CEDAE - CIA. ESTADUAL AGUAS E ESGOTO (11992736) NF: 119927360818","Classificacao":"204003 - AGUA E ESGOTO","Natureza_da_Classificao":"D","ValorCC":"-1255.65","URL":""},{"Descricao":"0013-G\u00c1S 07\/2018 - CEG-CIA. DISTRIB. GAS DO RIO DE JANEIRO (05029400)","Classificacao":"204004 - GAS","Natureza_da_Classificao":"D","ValorCC":"-37.25","URL":""}]
     * 
     * @apiVersion 0.1.0
     * 
     * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_analitico_grafico_novo?login=10001&month=8&year=2018&type=CONCESSIONARIAS&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/balancete_analitico_grafico_novo_model', 'balancete_analitico_grafico');

		$data = $this->_encode_items($this->balancete_analitico_grafico->fetch($this->login, $this->date, $this->input->get('type')));

		echo json_encode($data);
		exit;
	}
}
