<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Balancete_analitico_grafico extends Abstract_controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/balancete_analitico_grafico Balancete Analítico (Gráfico)
     * @apiName balancete_analitico_grafico
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} type Tipo de consulta: C ou D
     * @apiParam {String} unit Unidade para consulta
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"CodigoConta":"001","DescricaoConta":"CONDOM\u00d6NIO","CodigoClassificacao":"101001","DescricaoClassificacao":"CONDOMINIO","DiaLancamento":"01","Descricao":"01 0003-CONDOM\u00d6NIO 03\/2018","Valor":"4183.54"},{"CodigoConta":"001","DescricaoConta":"CONDOM\u00d6NIO","CodigoClassificacao":"101001","DescricaoClassificacao":"CONDOMINIO","DiaLancamento":"02","Descricao":"02 0003-CONDOM\u00d6NIO 03\/2018","Valor":"3646.35"}]
     * 
     * @apiVersion 0.1.0
     * 
     * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_analitico_grafico?login=10001&month=03&year=2018&type=C&unit=101&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/balancete_analitico_grafico_model', 'balancete_analitico_grafico');

		$data = $this->_encode_items($this->balancete_analitico_grafico->fetch($this->login, $this->date, $this->input->get('type'), $this->input->get('unit')));

		echo json_encode($data);
		exit;
	}
}
