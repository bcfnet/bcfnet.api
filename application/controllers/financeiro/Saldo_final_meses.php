<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Saldo_final_meses extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/saldo_final_meses Saldo Final Meses
     * @apiName saldo_final_meses
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {Number} total Quantidade de meses para consulta
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * [{"MesAno":"04\/2017","SaldoAnterior":"8195.17","TotalDebito":"15067.41","TotalCredito":"17238.33","SaldoFinal":"10366.09"},{"MesAno":"05\/2017","SaldoAnterior":"10366.09","TotalDebito":"18705.84","TotalCredito":"25340.96","SaldoFinal":"17001.21"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/saldo_final_meses?login=10001&month=03&year=2018&total=12&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/saldo_final_meses_model', 'saldo_final_meses');

		$data = $this->_encode_items($this->saldo_final_meses->fetch($this->login, $this->date, $this->input->get('total')));

		echo json_encode($data);
		exit;
	}
}