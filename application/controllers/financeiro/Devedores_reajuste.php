<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Devedores_reajuste extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/devedores_reajuste Devedores Reajuste
     * @apiName devedores_reajuste
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"Campo01":"","Campo02":"SUB-TOTAL","Campo03":"0,00","Campo04":"","Campo05":"0,00","Campo06":"0,00","Campo07":"","Campo08":"0,00","Campo09":"0,00","Campo10":"0,00"},{"Campo01":"","Campo02":"TOTAL GERAL","Campo03":"0,00","Campo04":"","Campo05":"0,00","Campo06":"0,00","Campo07":"","Campo08":"0,00","Campo09":"0,00","Campo10":"0,00"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/devedores?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/devedores_reajuste_model', 'devedores');
		$data = $this->_encode_items($this->devedores->fetch($this->login, $this->date));

		echo json_encode($data);
	}
}
