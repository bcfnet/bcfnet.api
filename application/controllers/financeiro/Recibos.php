<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Recibos extends Abstract_controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/recibos Recibos
     * @apiName recibos
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"Codigo":"0001","Complemento":"101","NumeroRecibo":"5829679","DataVencimento":"Jun 5 2018 12:00:00:000AM","InstrucaoRecibo":" "},{"Codigo":"0002","Complemento":"201","NumeroRecibo":"5829680","DataVencimento":"Jun 5 2018 12:00:00:000AM","InstrucaoRecibo":" "}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/recibos?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		if(empty($this->login)){
			exit;
		}
		$this->load->model('financeiro/Recibos_model', 'recibos');

		$data = $this->_encode_items($this->recibos->fetch($this->login));

		echo json_encode($data);
		exit;
	}
}
