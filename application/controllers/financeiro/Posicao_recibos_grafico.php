<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Posicao_recibos_grafico extends Abstract_controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/posicao_recibos_grafico Posição Recibos (Gráfico)
     * @apiName posicao_recibos_grafico
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"QuantidadeNaoPago":"0","QuantidadePago":"5","TotalNaoPago":"0","TotalPago":"21580.31"}]
     * 
     * @apiVersion 0.1.0
     * 
     * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/posicao_recibos_grafico?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/posicao_recibos_grafico_model', 'posicao_recibos_grafico');

		$data = $this->_encode_items($this->posicao_recibos_grafico->fetch($this->login, $this->date, $this->input->get('paid')));

		echo json_encode($data);
		exit;
	}
}
