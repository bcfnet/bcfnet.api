<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Posicao_recibos extends Abstract_controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/posicao_recibos Posição Recibos
     * @apiName posicao_recibos
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"NumeroRecibo":"5113508","DataVencimento":"05\/01\/2017","DataPagamento":"05\/01\/2017","ValorRecibo":"3.659,51","ValorPago":"3.649,53"},{"NumeroRecibo":"5167367","DataVencimento":"05\/02\/2017","DataPagamento":"03\/05\/2017","ValorRecibo":"3.881,40","ValorPago":"3.871,42"},{"NumeroRecibo":"5206753","DataVencimento":"05\/03\/2017","DataPagamento":"06\/03\/2017","ValorRecibo":"3.659,51","ValorPago":"3.649,53"},{"NumeroRecibo":"5259352","DataVencimento":"05\/04\/2017","DataPagamento":"31\/03\/2017","ValorRecibo":"4.316,42","ValorPago":"4.306,44"},{"NumeroRecibo":"5286750","DataVencimento":"05\/05\/2017","DataPagamento":"05\/05\/2017","ValorRecibo":"4.316,42","ValorPago":"4.306,44"},{"NumeroRecibo":"5326631","DataVencimento":"05\/06\/2017","DataPagamento":"05\/06\/2017","ValorRecibo":"4.316,42","ValorPago":"4.316,42"},{"NumeroRecibo":"5372776","DataVencimento":"05\/07\/2017","DataPagamento":"05\/07\/2017","ValorRecibo":"4.316,42","ValorPago":"4.306,44"},{"NumeroRecibo":"5417298","DataVencimento":"05\/08\/2017","DataPagamento":"07\/08\/2017","ValorRecibo":"4.316,42","ValorPago":"4.306,44"},{"NumeroRecibo":"5457550","DataVencimento":"05\/09\/2017","DataPagamento":"05\/09\/2017","ValorRecibo":"4.316,42","ValorPago":"4.306,44"},{"NumeroRecibo":"5489675","DataVencimento":"05\/10\/2017","DataPagamento":"05\/10\/2017","ValorRecibo":"4.316,42","ValorPago":"4.316,42"},{"NumeroRecibo":"5543030","DataVencimento":"05\/11\/2017","DataPagamento":"06\/11\/2017","ValorRecibo":"4.316,42","ValorPago":"4.316,42"},{"NumeroRecibo":"5586339","DataVencimento":"05\/12\/2017","DataPagamento":"05\/12\/2017","ValorRecibo":"4.316,42","ValorPago":"4.316,42"}]
     * 
     * @apiVersion 0.1.0
     * 
     * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/posicao_recibos?login=10001&year=2018&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/posicao_recibos_model', 'posicao_recibos');

		$data = $this->_encode_items($this->posicao_recibos->fetch($this->login, $this->input->get('year')));

		echo json_encode($data);
		exit;
	}
}
