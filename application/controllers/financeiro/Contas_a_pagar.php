<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Contas_a_pagar extends Abstract_Controller {

	protected $permissions = array(1);

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/contas_a_pagar Contas a Pagar
     * @apiName contas_a_pagar
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"Lanamento_CP":"3250809","DataVencimento":"May 21 2018 12:00:00:000AM","Vencimento":"21\/05\/2018","Historico":"LUZ E FOR\u00c7A LIGHT SERV. DE ELETRICIDADE S\/A (10019811335)","ReferenciaParcela":"04\/2018","Valor":"1585.41","Natureza":"D","URL":" "}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/contas_a_pagar?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/contas_a_pagar_model', 'contas_a_pagar');

		$data = $this->_encode_items($this->contas_a_pagar->fetch($this->login));

		echo json_encode($data);
	}
}
