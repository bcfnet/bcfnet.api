<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Saldo_atualizado extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/saldo_atualizado Saldo Atualizado
     * @apiName saldo_atualizado
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * [{"CodigoCliente":"00001","MesAno":"05\/2018","CodigoConta":"001","Conta":"001-CONDOM\u00cdNIO","SaldoInicial":"-70330.42","TotalD":"6869.06","TotalC":"16834.28","SaldoFinal":"-60365.2","Tipo":"0","TipoSaldoFinal":"D"}]
     * 
     * @apiVersion 0.1.0
	 * 
	 * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/saldo_atualizado?login=10001&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/saldo_atualizado_model', 'saldo_atualizado');

		$data = $this->_encode_items($this->saldo_atualizado->fetch($this->login));

		echo json_encode($data);
		exit;
	}
}