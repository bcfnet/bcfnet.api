<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Posicao_recibos_grafico_analitico extends Abstract_controller {

	public function __construct() {
		parent::__construct();
	}

	/**
     * @api {get} /financeiro/posicao_recibos_grafico_analitico Posição Recibos (Gráfico Analítico)
     * @apiName posicao_recibos_grafico_analitico
     * @apiGroup financeiro
     * 
     * @apiParam {Number} login Login do condomínio
	 * @apiParam {Number} month Mês para consulta
	 * @apiParam {Number} year Ano para consulta
     * @apiParam {String} paid Listar recibos pagos: S ou N
     * @apiParam {String} token JWT_TOKEN
     * 
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     * [{"NumeroRecibo":"5708430","DataVencimento":"Mar 5 2018 12:00:00:000AM","DataBaixa":"Mar 5 2018 12:00:00:000AM","CodigoUnidade":"0001","ComplementoUnidade":"101","ContratoLocacao":"","CodigoImovel":"","EnderecoImovel":" ","ValorRecibo":"4316.42"}]
     * 
     * @apiVersion 0.1.0
     * 
     * @apiExample {curl} Usage:
     * curl -i http://condolog.com.br/bcfnet/api/financeiro/posicao_recibos_grafico_analitico?login=10001&month=03&year=2018&paid=S&token=$YOUR_JWT_TOKEN
     */
	public function index_get()	{
		$this->load->model('financeiro/posicao_recibos_grafico_analitico_model', 'posicao_recibos_grafico_analitico');

		$data = $this->_encode_items($this->posicao_recibos_grafico_analitico->fetch($this->login, $this->date, $this->input->get('paid')));

		echo json_encode($data);
		exit;
	}
}
