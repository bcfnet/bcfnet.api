<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/controllers/Abstract_controller.php';

class Login extends Abstract_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index_post()	{
		$login = $this->input->post('login');
		$password = $this->input->post('password');

		$this->load->model('login_model');

		$loginData = $this->login_model->fetch($login, $password);
		
		if (isset($loginData[0])) {
			$user = $loginData[0];
			
			if ($user->SituacaoCliente == 'A' && (strtolower($user->Senha) == strtolower($password) || strtolower($user->SenhaUnidadeIM) == strtolower($password))) {
				$data = array(
					'success' => TRUE,
					'codigo' => $user->CodigoCliente,
					'condominio' => $user->NomeCliente,
					'gerente' => $user->NomeGerente,
					'telefone_gerente' => $user->Telefone,
					'ramal_gerente' => $user->Ramal,
					'email_gerente' => $user->E_Mail,
					'sindico' => $user->NomeSindico,
					'condomino' => $user->NomeCondomino,
					'permissao' => isset($user->Permissao) ? $user->Permissao : '', 
					'token' => JWT::encode(array(
						'id' => $user->CodigoCliente,
						'login' => $login
					), JWT_SECRET_KEY)
				);
				$data = json_encode($this->_encode_items($data));

				echo $data;
				exit;
			}
		} else {

		echo json_encode(array('success' => FALSE));
		exit;
		}
	}
}
