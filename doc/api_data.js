define({ "api": [
  {
    "type": "get",
    "url": "/administrativo/administrativo",
    "title": "Administrativo",
    "name": "administrativo",
    "group": "administrativo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "subcategoria",
            "description": "<p>Subcategoria (Ex.: Convocação de Assembleia)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"id\":\"2895082\",\"sequencial\":\"\",\"campo_tipo\":\"4\",\"login_internet\":\"10001\",\"arvore\":\"Administrativo#Convoca\\u00c3\\u00a7\\u00c3\\u00a3o de Assembl\\u00c3\\u00a9ias#Edital de Convoca\\u00c3\\u00a7\\u00c3\\u00a3o para AGO no dia 01\\/03\\/2018\",\"data_referencia\":\"\",\"permissao\":\"P\",\"data_expiracao\":\"25\\/02\\/2019\",\"data_inclusao\":\"26\\/02\\/2018\",\"data_inicial\":\"\",\"data_final\":\"\",\"status\":\"A\",\"link_arquivo\":\"uploads\\/3f5944edb66c3598397f249767da8ed2.pdf\",\"fromSite\":\"1\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/administrativo/administrativo?login=10001&token=$YOUR_JWT_TOKEN&subcategoria=Atas%20de%20Assembléia",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/administrativo/Administrativo.php",
    "groupTitle": "administrativo"
  },
  {
    "type": "get",
    "url": "/administrativo/arquivo",
    "title": "Arquivo",
    "name": "arquivo",
    "group": "administrativo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID do arquivo (opcional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"id\":\"2895082\",\"sequencial\":\"\",\"campo_tipo\":\"4\",\"login_internet\":\"10001\",\"arvore\":\"Administrativo#Convoca\\u00c3\\u00a7\\u00c3\\u00a3o de Assembl\\u00c3\\u00a9ias#Edital de Convoca\\u00c3\\u00a7\\u00c3\\u00a3o para AGO no dia 01\\/03\\/2018\",\"data_referencia\":\"\",\"permissao\":\"P\",\"data_expiracao\":\"25\\/02\\/2019\",\"data_inclusao\":\"26\\/02\\/2018\",\"data_inicial\":\"\",\"data_final\":\"\",\"status\":\"A\",\"link_arquivo\":\"uploads\\/3f5944edb66c3598397f249767da8ed2.pdf\",\"fromSite\":\"1\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/administrativo/arquivo?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/administrativo/Arquivo.php",
    "groupTitle": "administrativo"
  },
  {
    "type": "post",
    "url": "/administrativo/arquivo/create",
    "title": "Arquivo - Criar",
    "name": "arquivo_create",
    "group": "administrativo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pasta",
            "description": "<p>Pasta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tipo",
            "description": "<p>Tipo do Arquivo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "descricao",
            "description": "<p>Descrição</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "permissao",
            "description": "<p>Permissão (Exemplo: P)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data_expiracao",
            "description": "<p>Data de Expiração (Formato: DD/MM/YYYY)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data_inicial",
            "description": "<p>Data Inicial (Formato: DD/MM/YYYY)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data_final",
            "description": "<p>Data Final (Formato: DD/MM/YYYY)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data_inclusao",
            "description": "<p>Data Inclusão (Formato: DD/MM/YYYY)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Status (Formato: A ou C)</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "arquivo",
            "description": "<p>Arquivo (File) (Opcional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"id\":\"2922825\",\"status\":\"1\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/administrativo/arquivo/create?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/administrativo/Arquivo.php",
    "groupTitle": "administrativo"
  },
  {
    "type": "post",
    "url": "/administrativo/arquivo/delete",
    "title": "Arquivo - Deletar",
    "name": "arquivo_delete",
    "group": "administrativo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>ID do Arquivo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"1\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/administrativo/arquivo/delete?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/administrativo/Arquivo.php",
    "groupTitle": "administrativo"
  },
  {
    "type": "post",
    "url": "/administrativo/arquivo/update",
    "title": "Arquivo - Atualizar",
    "name": "arquivo_update",
    "group": "administrativo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID do Arquivo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pasta",
            "description": "<p>Pasta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tipo",
            "description": "<p>Tipo do Arquivo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "descricao",
            "description": "<p>Descrição</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "permissao",
            "description": "<p>Permissão (Exemplo: P)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data_expiracao",
            "description": "<p>Data de Expiração (Formato: DD/MM/YYYY)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data_inicial",
            "description": "<p>Data Inicial (Formato: DD/MM/YYYY)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data_final",
            "description": "<p>Data Final (Formato: DD/MM/YYYY)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data_inclusao",
            "description": "<p>Data Inclusão (Formato: DD/MM/YYYY)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Status (Formato: A ou C)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "arquivo",
            "description": "<p>Arquivo (File) (Opcional)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"1\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/administrativo/arquivo/update?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/administrativo/Arquivo.php",
    "groupTitle": "administrativo"
  },
  {
    "type": "get",
    "url": "/administrativo/departamento_pessoal",
    "title": "Departamento Pessoal",
    "name": "departamento_pessoal",
    "group": "administrativo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "subcategoria",
            "description": "<p>Subcategoria (Ex.: Informações)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n [{\"id\":\"1629655\",\"sequencial\":\"000002\",\"campo_tipo\":\"4\",\"login_internet\":\"10001\",\"arvore\":\"Departamento Pessoal#Informa\\u00c3\\u00a7\\u00c3\\u00b5es#Boletim de Horas Extras\",\"data_referencia\":\"\",\"permissao\":\"R\",\"data_expiracao\":\"09\\/07\\/2019\",\"data_inclusao\":\"\",\"data_inicial\":\" \",\"data_final\":\" \",\"status\":\"A\",\"link_arquivo\":\"htmls\\/0001BHE.PDF\",\"fromSite\":\"0\"},{\"id\":\"1629656\",\"sequencial\":\"000003\",\"campo_tipo\":\"4\",\"login_internet\":\"10001\",\"arvore\":\"Departamento Pessoal#Informa\\u00c3\\u00a7\\u00c3\\u00b5es#Escala de revezamento\",\"data_referencia\":\"\",\"permissao\":\"R\",\"data_expiracao\":\"09\\/07\\/2019\",\"data_inclusao\":\"\",\"data_inicial\":\" \",\"data_final\":\" \",\"status\":\"A\",\"link_arquivo\":\"htmls\\/0001EDR.PDF\",\"fromSite\":\"0\"},{\"id\":\"1629657\",\"sequencial\":\"000004\",\"campo_tipo\":\"4\",\"login_internet\":\"10001\",\"arvore\":\"Departamento Pessoal#Informa\\u00c3\\u00a7\\u00c3\\u00b5es#Ficha Cadastral\",\"data_referencia\":\"\",\"permissao\":\"R\",\"data_expiracao\":\"09\\/07\\/2019\",\"data_inclusao\":\"\",\"data_inicial\":\" \",\"data_final\":\" \",\"status\":\"A\",\"link_arquivo\":\"htmls\\/0001FCD.PDF\",\"fromSite\":\"0\"},{\"id\":\"1629658\",\"sequencial\":\"000005\",\"campo_tipo\":\"4\",\"login_internet\":\"10001\",\"arvore\":\"Departamento Pessoal#Informa\\u00c3\\u00a7\\u00c3\\u00b5es#Folha de Pagamento\",\"data_referencia\":\"\",\"permissao\":\"R\",\"data_expiracao\":\"09\\/07\\/2019\",\"data_inclusao\":\"\",\"data_inicial\":\" \",\"data_final\":\" \",\"status\":\"A\",\"link_arquivo\":\"htmls\\/0001FPG.PDF\",\"fromSite\":\"0\"},{\"id\":\"1629659\",\"sequencial\":\"000006\",\"campo_tipo\":\"4\",\"login_internet\":\"10001\",\"arvore\":\"Departamento Pessoal#Informa\\u00c3\\u00a7\\u00c3\\u00b5es#Quadro de Hor\\u00c3\\u00a1rios\",\"data_referencia\":\"\",\"permissao\":\"R\",\"data_expiracao\":\"09\\/07\\/2019\",\"data_inclusao\":\"\",\"data_inicial\":\" \",\"data_final\":\" \",\"status\":\"A\",\"link_arquivo\":\"htmls\\/0001QHT.PDF\",\"fromSite\":\"0\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/administrativo/departamento_pessoal?login=10001&token=$YOUR_JWT_TOKEN&subcategoria=Informações",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/administrativo/Departamento_pessoal.php",
    "groupTitle": "administrativo"
  },
  {
    "type": "get",
    "url": "/administrativo/juridico",
    "title": "Juridico",
    "name": "juridico",
    "group": "administrativo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "subcategoria",
            "description": "<p>Subcategoria (Ex.: Acompanhamento de Processos)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"id\":\"2895082\",\"sequencial\":\"\",\"campo_tipo\":\"4\",\"login_internet\":\"10001\",\"arvore\":\"Administrativo#Convoca\\u00c3\\u00a7\\u00c3\\u00a3o de Assembl\\u00c3\\u00a9ias#Edital de Convoca\\u00c3\\u00a7\\u00c3\\u00a3o para AGO no dia 01\\/03\\/2018\",\"data_referencia\":\"\",\"permissao\":\"P\",\"data_expiracao\":\"25\\/02\\/2019\",\"data_inclusao\":\"26\\/02\\/2018\",\"data_inicial\":\"\",\"data_final\":\"\",\"status\":\"A\",\"link_arquivo\":\"uploads\\/3f5944edb66c3598397f249767da8ed2.pdf\",\"fromSite\":\"1\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/administrativo/juridico?login=10001&token=$YOUR_JWT_TOKEN&subcategoria=Acompanhamento%20de%20Processos",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/administrativo/Juridico.php",
    "groupTitle": "administrativo"
  },
  {
    "type": "post",
    "url": "/condominio/album/create",
    "title": "Álbum - Criar",
    "name": "album_create",
    "group": "album",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "File[]",
            "optional": false,
            "field": "arquivo",
            "description": "<p>Arquivo (File)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"Toda(s) a(s) foto(s) foram publicadas com sucesso.\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/album/create?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Album.php",
    "groupTitle": "album"
  },
  {
    "type": "post",
    "url": "/condominio/album/delete",
    "title": "Album - Deletar",
    "name": "album_delete",
    "group": "album",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>ID do Arquivo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"A foto foi 299 deletada com sucesso.\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/administrativo/arquivo/delete?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Album.php",
    "groupTitle": "album"
  },
  {
    "type": "post",
    "url": "/condominio/aviso/update",
    "title": "Aviso - Atualizar",
    "name": "atualizar_aviso",
    "group": "aviso",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ID",
            "description": "<p>ID do Aviso</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "titulo",
            "description": "<p>Título</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mensagem",
            "description": "<p>Mensagem</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data_expiracao",
            "description": "<p>Data de Expiração (Formato: (01/07/2019))</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "visualizacao",
            "description": "<p>Visualização (todos = Todos; sindicos = Somente para os Síndicos e condominos = Somente para os Condóminos)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"O aviso $id foi atualizado.\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/aviso/create?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Aviso.php",
    "groupTitle": "aviso"
  },
  {
    "type": "get",
    "url": "/condominio/aviso",
    "title": "Aviso",
    "name": "aviso",
    "group": "aviso",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do administrador</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ID",
            "description": "<p>do aviso (opcional) - É usado caso queira retornar somente um registro</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"id\":\"325\",\"titulo\":\"Teste Titulo\",\"data_inclusao\":\"26\\/07\\/2018\",\"data_expiracao\":\"10\\/10\\/2018\",\"visualizacao\":\"todos\",\"mensagem\":\"Esse \\u00c3\\u00a9 um teste somente...\",\"condominio\":\"10001\",\"condomino\":\"\"},{\"id\":\"326\",\"titulo\":\"Teste Titulo\",\"data_inclusao\":\"26\\/07\\/2018\",\"data_expiracao\":\"10\\/10\\/2018\",\"visualizacao\":\"\",\"mensagem\":\"Esse \\u00c3\\u00a9 um teste somente...\",\"condominio\":\"10001\",\"condomino\":\"\"},{\"id\":\"335\",\"titulo\":\"NOVO TITULO 100\",\"data_inclusao\":\"27\\/07\\/2018\",\"data_expiracao\":\"10\\/10\\/2018\",\"visualizacao\":\"todos\",\"mensagem\":\"MENSAGEM NOVO\",\"condominio\":\"10001\",\"condomino\":\"\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/aviso?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Aviso.php",
    "groupTitle": "aviso"
  },
  {
    "type": "post",
    "url": "/condominio/aviso/create",
    "title": "Aviso - Criar",
    "name": "criar_aviso",
    "group": "aviso",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "titulo",
            "description": "<p>Título</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mensagem",
            "description": "<p>Mensagem</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "condominio",
            "description": "<p>Login do Condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "condomino",
            "description": "<p>Login do Condômino</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data_expiracao",
            "description": "<p>Data de Expiração (Formato: (01/07/2019))</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "visualizacao",
            "description": "<p>Visualização (todos = Todos; sindicos = Somente para os Síndicos e condominos = Somente para os Condóminos)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"O aviso foi publicado.\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/aviso/create?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Aviso.php",
    "groupTitle": "aviso"
  },
  {
    "type": "post",
    "url": "/condominio/aviso/update",
    "title": "Aviso - Deletar",
    "name": "deletar_aviso",
    "group": "aviso",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ID",
            "description": "<p>ID do Aviso</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"O aviso $id foi deletado.\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/aviso/delete?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Aviso.php",
    "groupTitle": "aviso"
  },
  {
    "type": "get",
    "url": "/condominio/aviso/get_total_avisos_administradora",
    "title": "Aviso - Quantidade de avisos da Administradora",
    "name": "total_avisos_administradora_aviso",
    "group": "aviso",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"Total\":\"1\",\"Avisos\":[{\"id\":\"313\",\"titulo\":\"TESTANDO\",\"data_inclusao\":\"05\\/02\\/2018\",\"data_expiracao\":\"01\\/10\\/2018\",\"visualizacao\":\"todos\",\"mensagem\":\"\",\"condominio\":\"\",\"condomino\":\"\"}]}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/aviso/get_total_avisos_administradora?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Aviso.php",
    "groupTitle": "aviso"
  },
  {
    "type": "get",
    "url": "/condominio/aviso/get_total_avisos_condominio",
    "title": "Aviso - Quantidade de avisos do Condomínio",
    "name": "total_avisos_condominio_aviso",
    "group": "aviso",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"Total\":\"0\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/aviso/get_total_avisos_condominio?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Aviso.php",
    "groupTitle": "aviso"
  },
  {
    "type": "update",
    "url": "/cadastro/dados_perfil",
    "title": "Dados Perfil",
    "name": "dados_perfil",
    "group": "cadastro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "CodigoCliente",
            "description": "<p>Código Cliente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"CodigoCliente\":\"00001\",\"NomeCliente\":\"DONA NINA\",\"CPF_CNPJ\":\"01412380707\",\"E_Mail\":\"carlos.eduardo@bcfadm.com.br\",\"Telefone\":\"3543-290\",\"Celular\":\"\",\"Senha\":\"\",\"CEP\":\"\",\"Tipo_logradouro\":\"\",\"Enderecos\":\"\",\"Numero\":\"\",\"Complemento\":\"\",\"Bairro\":\"\",\"Estado\":\"\",\"Cidade\":\"\",\"dt_update\":\"2018-04-18 13:11:56\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/cadastro/dados_perfil/?CodigoCliente=00001&login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/cadastro/Dados_perfil.php",
    "groupTitle": "cadastro"
  },
  {
    "type": "update",
    "url": "/cadastro/dados_perfil",
    "title": "Dados Perfil - Atualizar",
    "name": "dados_perfil_update",
    "group": "cadastro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "CodigoCliente",
            "description": "<p>Código Cliente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "NomeCliente",
            "description": "<p>Nome do Cliente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "CPF_CNPJ",
            "description": "<p>CPF ou CNPJ do Cliente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "E_Mail",
            "description": "<p>E-mail para contato</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Telefone",
            "description": "<p>Telefone para contato</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Celular",
            "description": "<p>Celular para contato</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Senha",
            "description": "<p>Senha de acesso</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "CEP",
            "description": "<p>CEP</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Tipo_logradouro",
            "description": "<p>Tipo de Logradouro</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Enderecos",
            "description": "<p>Endereço</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Numero",
            "description": "<p>Número da residência</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Complemento",
            "description": "<p>Complemento da residência</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Bairro",
            "description": "<p>Bairro em que reside</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Estado",
            "description": "<p>Estado em que reside</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Cidade",
            "description": "<p>Cidade em que recide</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"CodigoCliente\":\"00001\",\"error\":\"0\", \"message\": \"O Dado de Perfil foi atualizado\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/cadastro/dados_perfil/update?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/cadastro/Dados_perfil.php",
    "groupTitle": "cadastro"
  },
  {
    "type": "update",
    "url": "/cadastro/formas_pagamento",
    "title": "Forma de Pagamento",
    "name": "formas_pagamento",
    "group": "cadastro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "CodigoCliente",
            "description": "<p>Código Cliente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/cadastro/formas_pagamento/?CodigoCliente=00001&login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/cadastro/Formas_pagamento.php",
    "groupTitle": "cadastro"
  },
  {
    "type": "update",
    "url": "/cadastro/formas_pagamento",
    "title": "Forma de Pagamento - Atualizar",
    "name": "formas_pagamento_update",
    "group": "cadastro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "CodigoCliente",
            "description": "<p>Código Cliente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "NomeCliente",
            "description": "<p>Nome do Cliente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "CPF_CNPJ",
            "description": "<p>CPF ou CNPJ do Cliente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Banco",
            "description": "<p>Nome do Banco (Ex.: Santander, Bradesco)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Banco_Digito",
            "description": "<p>Dígito do Banco (033, representa o Santander)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Agencia",
            "description": "<p>Agência do Titular da Conta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Digito_Agencia",
            "description": "<p>Dígito da Agência do Titular da Conta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Conta",
            "description": "<p>Conta Corrente do Titular</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Digito_Conta",
            "description": "<p>Dígito da Conta Corrente do Titular</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"CodigoCliente\":\"00001\",\"error\":\"0\", \"message\": \"A Forma de Pagamento foi atualizada\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/cadastro/formas_pagamento/update?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/cadastro/Formas_pagamento.php",
    "groupTitle": "cadastro"
  },
  {
    "type": "get",
    "url": "/cadastro/retorna_unidades",
    "title": "Listar unidades do condomínio",
    "name": "retorna_unidades",
    "group": "cadastro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"CodigoCliente\":\"00001\",\"NomeCliente\":\"DONA NINA\",\"EnderecoCondominio\":\"AV. HENRIQUE DUMONT, N\\u00a7 152\",\"Senha\":\"12WW\",\"NomeSindico\":\"ANA CHRISTINA ADRI\\u00c7O RODRIGUES CARVALHO\",\"Email\":\"a.chriscarvalho@gmail.com\",\"Tel_Sindico\":\"9 8661-2960\",\"Codigo_da_Unidade\":\"0001\",\"Complemento_Unidade\":\"101\",\"Nome_do_Cliente\":\"DANIEL BAR E OUTROS\",\"LoginUnidade\":\"200010001\",\"SenhaUnidade\":\"4041\",\"e_MailCondomino\":\"robertobar@globo.com\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/cadastro/retorna_unidades?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/cadastro/Retorna_unidades.php",
    "groupTitle": "cadastro"
  },
  {
    "type": "get",
    "url": "/condominio/album",
    "title": "Álbum",
    "name": "album",
    "group": "condominio",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do administrador</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"CodigoFilial\":\"01\",\"NomeFilial\":\"Centro\",\"CodigoGerente\":\"0125\",\"NomeGerente\":\"Cl\\u00e1udio Bittencourt\",\"Telefone\":\"3543-2908\",\"E_Mail\":\"claudio.bittencourt@bcfadm.com.br\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/album?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Album.php",
    "groupTitle": "condominio"
  },
  {
    "type": "get",
    "url": "/condominio/condominios/get",
    "title": "Condomínios - Listar Condomínios",
    "name": "condominios",
    "group": "condominio",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do administrador</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ID",
            "description": "<p>do Condomínio (opcional) - Caso deseja listar somente um específico</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"id\":\"11\",\"sequencial\":\"000536\",\"campo_tipo\":\"1\",\"login_internet\":\"10040\",\"nome\":\"14 DEGRAUS\",\"situacao\":\"A\",\"endereco\":\"RUA FREI LEANDRO, 16\",\"bairro\":\"JARDIM BOT\\u00c3\\u0082NICO\",\"cidade\":\"RIO DE JANEIRO\",\"estado\":\"RJ\",\"cep\":\"22470210\",\"telefone\":\"\",\"cnpj\":\"68583210000193\",\"login_sindico\":\"200400010\",\"senha\":\"8066\",\"gerente\":\"0167\",\"nome_sindico\":\"LEONARDO VILAIN JO\\u00c3\\u0083O\",\"logotipo\":\"\",\"email_preenchido\":\"\",\"telefone_preenchido\":\"\",\"show_welcome\":\"\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/condominios/get?login=bcfadm&id=11&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Condominios.php",
    "groupTitle": "condominio"
  },
  {
    "type": "get",
    "url": "/condominio/condominios",
    "title": "Condomínios",
    "name": "condominios",
    "group": "condominio",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do administrador</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"CodigoFilial\":\"01\",\"NomeFilial\":\"Centro\",\"CodigoGerente\":\"0125\",\"NomeGerente\":\"Cl\\u00e1udio Bittencourt\",\"Telefone\":\"3543-2908\",\"E_Mail\":\"claudio.bittencourt@bcfadm.com.br\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/condominios?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Condominios.php",
    "groupTitle": "condominio"
  },
  {
    "type": "get",
    "url": "/condominio/dados_do_gerente",
    "title": "Dados do gerente",
    "name": "dados_do_gerente",
    "group": "condominio",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"CodigoFilial\":\"01\",\"NomeFilial\":\"Centro\",\"CodigoGerente\":\"0125\",\"NomeGerente\":\"Cl\\u00e1udio Bittencourt\",\"Telefone\":\"3543-2908\",\"E_Mail\":\"claudio.bittencourt@bcfadm.com.br\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/dados_do_gerente?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Dados_do_gerente.php",
    "groupTitle": "condominio"
  },
  {
    "type": "get",
    "url": "/condominio/graficos",
    "title": "Gráficos",
    "name": "graficos",
    "group": "condominio",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"id\":\"339115\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"06\\/2018\",\"valor\":\"614,84\"},{\"id\":\"339116\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"06\\/2018\",\"valor\":\"1620,59\"},{\"id\":\"337958\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"05\\/2018\",\"valor\":\"1585,41\"},{\"id\":\"337957\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"05\\/2018\",\"valor\":\"635,32\"},{\"id\":\"336808\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"04\\/2018\",\"valor\":\"1685,12\"},{\"id\":\"336807\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"04\\/2018\",\"valor\":\"2493,34\"},{\"id\":\"335683\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"03\\/2018\",\"valor\":\"2197,52\"},{\"id\":\"335684\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"03\\/2018\",\"valor\":\"1339,69\"},{\"id\":\"334554\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"02\\/2018\",\"valor\":\"1533,55\"},{\"id\":\"334553\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"02\\/2018\",\"valor\":\"1807,79\"},{\"id\":\"333433\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"01\\/2018\",\"valor\":\"1468,10\"},{\"id\":\"333434\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"01\\/2018\",\"valor\":\"1412,13\"},{\"id\":\"332303\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"12\\/2017\",\"valor\":\"1449,81\"},{\"id\":\"332304\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"12\\/2017\",\"valor\":\"65,23\"},{\"id\":\"331190\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"11\\/2017\",\"valor\":\"0,00\"},{\"id\":\"331189\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"11\\/2017\",\"valor\":\"1170,26\"},{\"id\":\"330035\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"10\\/2017\",\"valor\":\"556,01\"},{\"id\":\"330036\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"10\\/2017\",\"valor\":\"1311,88\"},{\"id\":\"328893\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"09\\/2017\",\"valor\":\"545,11\"},{\"id\":\"328894\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"09\\/2017\",\"valor\":\"1329,73\"},{\"id\":\"327761\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"08\\/2017\",\"valor\":\"545,03\"},{\"id\":\"327762\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"08\\/2017\",\"valor\":\"1231,08\"},{\"id\":\"326615\",\"login_condominio\":\"10001\",\"titulo\":\"AGUA E ESGOTO\",\"mes_referencia\":\"07\\/2017\",\"valor\":\"526,24\"},{\"id\":\"326616\",\"login_condominio\":\"10001\",\"titulo\":\"LUZ E FOR\\u00c3\\u0087A\",\"mes_referencia\":\"07\\/2017\",\"valor\":\"1292,40\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/graficos?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Graficos.php",
    "groupTitle": "condominio"
  },
  {
    "type": "post",
    "url": "/esqueci_minha_senha",
    "title": "Esqueci Minha Senha",
    "name": "esqueci_minha_senha",
    "group": "esqueci_minha_senha",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do Usuário</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\"success\":true, \"error\": \"\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -X POST -F 'login=10001' -i http://condolog.com.br/bcfnet/api/esqueci_minha_senha?token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/Esqueci_minha_senha.php",
    "groupTitle": "esqueci_minha_senha"
  },
  {
    "type": "get",
    "url": "/financeiro/balancete_analitico",
    "title": "Balancete Analítico",
    "name": "balancete_analitico",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Campo1\":\"Dt Complemento\",\"Campo2\":\"Valor\",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"001 - CONDOM\\u00cdNIO\",\"Campo2\":\" \",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"R E C E I T A S\",\"Campo2\":\"\",\"Campo3\":\"\",\"Campo4\":\"0\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_analitico?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Balancete_analitico.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/balancete_analitico_grafico",
    "title": "Balancete Analítico (Gráfico)",
    "name": "balancete_analitico_grafico",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Tipo de consulta: C ou D</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "unit",
            "description": "<p>Unidade para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"CodigoConta\":\"001\",\"DescricaoConta\":\"CONDOM\\u00d6NIO\",\"CodigoClassificacao\":\"101001\",\"DescricaoClassificacao\":\"CONDOMINIO\",\"DiaLancamento\":\"01\",\"Descricao\":\"01 0003-CONDOM\\u00d6NIO 03\\/2018\",\"Valor\":\"4183.54\"},{\"CodigoConta\":\"001\",\"DescricaoConta\":\"CONDOM\\u00d6NIO\",\"CodigoClassificacao\":\"101001\",\"DescricaoClassificacao\":\"CONDOMINIO\",\"DiaLancamento\":\"02\",\"Descricao\":\"02 0003-CONDOM\\u00d6NIO 03\\/2018\",\"Valor\":\"3646.35\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_analitico_grafico?login=10001&month=03&year=2018&type=C&unit=101&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Balancete_analitico_grafico.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/balancete_analitico_grafico_novo",
    "title": "Balancete Analítico NOVO (Gráfico)",
    "name": "balancete_analitico_grafico_novo",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Tipo de consulta: PESSOAL, ADMINISTRATIVAS, CONCESSIONARIAS, CONTRATOS, MATERIAIS</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Descricao\":\"0011-AGUA E ESGOTO 08\\/2018 - CEDAE - CIA. ESTADUAL AGUAS E ESGOTO (11992736) NF: 119927360818\",\"Classificacao\":\"204003 - AGUA E ESGOTO\",\"Natureza_da_Classificao\":\"D\",\"ValorCC\":\"-1255.65\",\"URL\":\"\"},{\"Descricao\":\"0013-G\\u00c1S 07\\/2018 - CEG-CIA. DISTRIB. GAS DO RIO DE JANEIRO (05029400)\",\"Classificacao\":\"204004 - GAS\",\"Natureza_da_Classificao\":\"D\",\"ValorCC\":\"-37.25\",\"URL\":\"\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_analitico_grafico_novo?login=10001&month=8&year=2018&type=CONCESSIONARIAS&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Balancete_analitico_grafico_novo.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/balancete_anual",
    "title": "Balancete Anual",
    "name": "balancete_anual",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Campo1\":\"Dt Complemento\",\"Campo2\":\"Valor\",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"001 - CONDOM\\u00cdNIO\",\"Campo2\":\" \",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"R E C E I T A S\",\"Campo2\":\"\",\"Campo3\":\"\",\"Campo4\":\"0\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_anual?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Balancete_anual.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/balancete_comparativo",
    "title": "Balancete Comparativo",
    "name": "balancete_comparativo",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Campo1\":\"Dt Complemento\",\"Campo2\":\"Valor\",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"001 - CONDOM\\u00cdNIO\",\"Campo2\":\" \",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"R E C E I T A S\",\"Campo2\":\"\",\"Campo3\":\"\",\"Campo4\":\"0\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_comparativo?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Balancete_comparativo.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/balancete_regpag",
    "title": "Balancete RecPag",
    "name": "balancete_recpag",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"ClassificacaoAnteior\":\"101\",\"Descicao\":\"ORDIN\\u00b5RIAS\",\"Valor\":\"21532.19\"},{\"ClassificacaoAnteior\":\"102\",\"Descicao\":\"EXTRAORDINARIAS\",\"Valor\":\"35000\"},{\"ClassificacaoAnteior\":\"110\",\"Descicao\":\"APLICACOES FINANCEIRAS\",\"Valor\":\"5.31\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_recpag?login=10001&month=03&year=2018&type=C&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Balancete_recpag.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/balancete_regpag_operacional",
    "title": "Balancete RecPag Operacional",
    "name": "balancete_recpag_operacional",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"ClassificacaoAnteior\":\"101\",\"Descicao\":\"ORDIN\\u00b5RIAS\",\"Valor\":\"21532.19\"},{\"ClassificacaoAnteior\":\"102\",\"Descicao\":\"EXTRAORDINARIAS\",\"Valor\":\"35000\"},{\"ClassificacaoAnteior\":\"110\",\"Descicao\":\"APLICACOES FINANCEIRAS\",\"Valor\":\"5.31\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_recpag_operacional?login=10001&month=03&year=2018&type=C&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Balancete_recpag_operacional.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/balancete_semi_sintetico",
    "title": "Balancete Semi Sintético",
    "name": "balancete_semi_sintetico",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Tipo de consulta: C ou D</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"ClassificacaoAnteior\":\"101\",\"Descicao\":\"ORDIN\\u00b5RIAS\",\"Valor\":\"21532.19\"},{\"ClassificacaoAnteior\":\"102\",\"Descicao\":\"EXTRAORDINARIAS\",\"Valor\":\"35000\"},{\"ClassificacaoAnteior\":\"110\",\"Descicao\":\"APLICACOES FINANCEIRAS\",\"Valor\":\"5.31\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_semi_sintetico?login=10001&month=03&year=2018&type=C&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Balancete_semi_sintetico.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/balancete_sintetico",
    "title": "Balancete Sintético",
    "name": "balancete_sintetico",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Campo1\":\"Descri\\u00e7\\u00e3o da Conta\",\"Campo2\":\"Valor\",\"Campo3\":\" \"},{\"Campo1\":\"CONDOM\\u00cdNIO\",\"Campo2\":\" \",\"Campo3\":\" \"},{\"Campo1\":\"R E C E I T A S\",\"Campo2\":\"\",\"Campo3\":\"\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/balancete_sintetico?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Balancete_sintetico.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/contas_a_pagar",
    "title": "Contas a Pagar",
    "name": "contas_a_pagar",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Lanamento_CP\":\"3250809\",\"DataVencimento\":\"May 21 2018 12:00:00:000AM\",\"Vencimento\":\"21\\/05\\/2018\",\"Historico\":\"LUZ E FOR\\u00c7A LIGHT SERV. DE ELETRICIDADE S\\/A (10019811335)\",\"ReferenciaParcela\":\"04\\/2018\",\"Valor\":\"1585.41\",\"Natureza\":\"D\",\"URL\":\" \"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/contas_a_pagar?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Contas_a_pagar.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/devedores",
    "title": "Devedores",
    "name": "devedores",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Campo1\":\"Total de Unidades:\",\"Campo2\":\"0\",\"Campo3\":\" \",\"Campo4\":\"Total do Condom\\u00ednio\",\"Campo5\":\"0,00\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/devedores?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Devedores.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/devedores_reajuste",
    "title": "Devedores Reajuste",
    "name": "devedores_reajuste",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Campo01\":\"\",\"Campo02\":\"SUB-TOTAL\",\"Campo03\":\"0,00\",\"Campo04\":\"\",\"Campo05\":\"0,00\",\"Campo06\":\"0,00\",\"Campo07\":\"\",\"Campo08\":\"0,00\",\"Campo09\":\"0,00\",\"Campo10\":\"0,00\"},{\"Campo01\":\"\",\"Campo02\":\"TOTAL GERAL\",\"Campo03\":\"0,00\",\"Campo04\":\"\",\"Campo05\":\"0,00\",\"Campo06\":\"0,00\",\"Campo07\":\"\",\"Campo08\":\"0,00\",\"Campo09\":\"0,00\",\"Campo10\":\"0,00\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/devedores?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Devedores_reajuste.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/emissao_recibos",
    "title": "Emissão Recibos",
    "name": "emissao_recibos",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>Número do recibo para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Campo1\":\"Descri\\u00e7\\u00e3o da Conta\",\"Campo2\":\"Valor\",\"Campo3\":\" \"},{\"Campo1\":\"CONDOM\\u00cdNIO\",\"Campo2\":\" \",\"Campo3\":\" \"},{\"Campo1\":\"R E C E I T A S\",\"Campo2\":\"\",\"Campo3\":\"\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/emissao_recibos?login=10001&number=1031293&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Emissao_recibos.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/extrato_conta_corrente_condominio",
    "title": "Extrato Conta Corrente Condominio",
    "name": "extrato_conta_corrente_condominio",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Campo1\":\"Dt Complemento\",\"Campo2\":\"Valor\",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"001 - CONDOM\\u00cdNIO\",\"Campo2\":\" \",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"R E C E I T A S\",\"Campo2\":\"\",\"Campo3\":\"\",\"Campo4\":\"0\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/extrato_conta_corrente_condominio?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Extrato_conta_corrente_condominio.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/extrato_conta_corrente_locacao",
    "title": "Extrato Conta Corrente locacao",
    "name": "extrato_conta_corrente_locacao",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Campo1\":\"Dt Complemento\",\"Campo2\":\"Valor\",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"001 - CONDOM\\u00cdNIO\",\"Campo2\":\" \",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"R E C E I T A S\",\"Campo2\":\"\",\"Campo3\":\"\",\"Campo4\":\"0\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/extrato_conta_corrente_locacao?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Extrato_conta_corrente_locacao.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/graficos",
    "title": "Gráficos",
    "name": "graficos",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "n",
            "description": "<p>Quantidade de Meses a serem mostradas. O padrão é 12 últimos meses.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Mes_Referencia\":\"07\\/2018\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-4959.39\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-8549.65\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-30060.8\"}]},{\"Mes_Referencia\":\"06\\/2018\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-14138.14\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-7968.68\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-31988.22\"},{\"Conta\":\"MATERIAIS\",\"Valor\":\"-620.71\"},{\"Conta\":\"PESSOAL\",\"Valor\":\"-43.9\"}]},{\"Mes_Referencia\":\"05\\/2018\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-14255.09\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-7098.89\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-30677.57\"},{\"Conta\":\"MATERIAIS\",\"Valor\":\"-284\"},{\"Conta\":\"PESSOAL\",\"Valor\":\"-43.9\"}]},{\"Mes_Referencia\":\"04\\/2018\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-18214.8\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-7664.39\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-46033.13\"},{\"Conta\":\"MATERIAIS\",\"Valor\":\"-535.9\"},{\"Conta\":\"PESSOAL\",\"Valor\":\"-43.9\"}]},{\"Mes_Referencia\":\"03\\/2018\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-14985.84\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-5732.59\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-11874\"},{\"Conta\":\"MATERIAIS\",\"Valor\":\"-295\"},{\"Conta\":\"PESSOAL\",\"Valor\":\"-950.17\"}]},{\"Mes_Referencia\":\"02\\/2018\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-13210.94\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-9204.72\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-31986.44\"},{\"Conta\":\"MATERIAIS\",\"Valor\":\"-514\"},{\"Conta\":\"PESSOAL\",\"Valor\":\"-950.17\"}]},{\"Mes_Referencia\":\"01\\/2018\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-12385.74\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-12883.74\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-29253.51\"},{\"Conta\":\"MATERIAIS\",\"Valor\":\"-727.2\"},{\"Conta\":\"PESSOAL\",\"Valor\":\"-1626.51\"}]},{\"Mes_Referencia\":\"12\\/2017\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-12728.01\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-3101.45\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-23562.04\"},{\"Conta\":\"MATERIAIS\",\"Valor\":\"-328.6\"},{\"Conta\":\"PESSOAL\",\"Valor\":\"-950.17\"}]},{\"Mes_Referencia\":\"11\\/2017\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-27569.61\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-4949.78\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-21474.17\"},{\"Conta\":\"PESSOAL\",\"Valor\":\"-1822.73\"}]},{\"Mes_Referencia\":\"10\\/2017\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-13609.24\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-9895.04\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-1874\"},{\"Conta\":\"PESSOAL\",\"Valor\":\"-43.9\"}]},{\"Mes_Referencia\":\"09\\/2017\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-15833.31\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-5020.67\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-1874\"},{\"Conta\":\"PESSOAL\",\"Valor\":\"-43.9\"}]},{\"Mes_Referencia\":\"08\\/2017\",\"Dados\":[{\"Conta\":\"ADMINISTRATIVAS\",\"Valor\":\"-10834.39\"},{\"Conta\":\"CONCESSIONARIAS\",\"Valor\":\"-5669.7\"},{\"Conta\":\"CONTRATOS\",\"Valor\":\"-1874\"}]}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/graficos?login=10001&n=12&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Graficos.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/minhas_faturas",
    "title": "Minhas Faturas",
    "name": "minhas_faturas",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Campo1\":\"Dt Complemento\",\"Campo2\":\"Valor\",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"001 - CONDOM\\u00cdNIO\",\"Campo2\":\" \",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"R E C E I T A S\",\"Campo2\":\"\",\"Campo3\":\"\",\"Campo4\":\"0\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/minhas_faturas?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Minhas_faturas.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/pasta_digital",
    "title": "Pasta Digital",
    "name": "pasta_digital",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Campo1\":\"Dt Complemento\",\"Campo2\":\"Valor\",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"001 - CONDOM\\u00cdNIO\",\"Campo2\":\" \",\"Campo3\":\" \",\"Campo4\":\" \"},{\"Campo1\":\"R E C E I T A S\",\"Campo2\":\"\",\"Campo3\":\"\",\"Campo4\":\"0\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/pasta_digital?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Pasta_digital.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/posicao_recibos",
    "title": "Posição Recibos",
    "name": "posicao_recibos",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"NumeroRecibo\":\"5113508\",\"DataVencimento\":\"05\\/01\\/2017\",\"DataPagamento\":\"05\\/01\\/2017\",\"ValorRecibo\":\"3.659,51\",\"ValorPago\":\"3.649,53\"},{\"NumeroRecibo\":\"5167367\",\"DataVencimento\":\"05\\/02\\/2017\",\"DataPagamento\":\"03\\/05\\/2017\",\"ValorRecibo\":\"3.881,40\",\"ValorPago\":\"3.871,42\"},{\"NumeroRecibo\":\"5206753\",\"DataVencimento\":\"05\\/03\\/2017\",\"DataPagamento\":\"06\\/03\\/2017\",\"ValorRecibo\":\"3.659,51\",\"ValorPago\":\"3.649,53\"},{\"NumeroRecibo\":\"5259352\",\"DataVencimento\":\"05\\/04\\/2017\",\"DataPagamento\":\"31\\/03\\/2017\",\"ValorRecibo\":\"4.316,42\",\"ValorPago\":\"4.306,44\"},{\"NumeroRecibo\":\"5286750\",\"DataVencimento\":\"05\\/05\\/2017\",\"DataPagamento\":\"05\\/05\\/2017\",\"ValorRecibo\":\"4.316,42\",\"ValorPago\":\"4.306,44\"},{\"NumeroRecibo\":\"5326631\",\"DataVencimento\":\"05\\/06\\/2017\",\"DataPagamento\":\"05\\/06\\/2017\",\"ValorRecibo\":\"4.316,42\",\"ValorPago\":\"4.316,42\"},{\"NumeroRecibo\":\"5372776\",\"DataVencimento\":\"05\\/07\\/2017\",\"DataPagamento\":\"05\\/07\\/2017\",\"ValorRecibo\":\"4.316,42\",\"ValorPago\":\"4.306,44\"},{\"NumeroRecibo\":\"5417298\",\"DataVencimento\":\"05\\/08\\/2017\",\"DataPagamento\":\"07\\/08\\/2017\",\"ValorRecibo\":\"4.316,42\",\"ValorPago\":\"4.306,44\"},{\"NumeroRecibo\":\"5457550\",\"DataVencimento\":\"05\\/09\\/2017\",\"DataPagamento\":\"05\\/09\\/2017\",\"ValorRecibo\":\"4.316,42\",\"ValorPago\":\"4.306,44\"},{\"NumeroRecibo\":\"5489675\",\"DataVencimento\":\"05\\/10\\/2017\",\"DataPagamento\":\"05\\/10\\/2017\",\"ValorRecibo\":\"4.316,42\",\"ValorPago\":\"4.316,42\"},{\"NumeroRecibo\":\"5543030\",\"DataVencimento\":\"05\\/11\\/2017\",\"DataPagamento\":\"06\\/11\\/2017\",\"ValorRecibo\":\"4.316,42\",\"ValorPago\":\"4.316,42\"},{\"NumeroRecibo\":\"5586339\",\"DataVencimento\":\"05\\/12\\/2017\",\"DataPagamento\":\"05\\/12\\/2017\",\"ValorRecibo\":\"4.316,42\",\"ValorPago\":\"4.316,42\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/posicao_recibos?login=10001&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Posicao_recibos.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/posicao_recibos_grafico",
    "title": "Posição Recibos (Gráfico)",
    "name": "posicao_recibos_grafico",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"QuantidadeNaoPago\":\"0\",\"QuantidadePago\":\"5\",\"TotalNaoPago\":\"0\",\"TotalPago\":\"21580.31\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/posicao_recibos_grafico?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Posicao_recibos_grafico.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/posicao_recibos_grafico_analitico",
    "title": "Posição Recibos (Gráfico Analítico)",
    "name": "posicao_recibos_grafico_analitico",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "paid",
            "description": "<p>Listar recibos pagos: S ou N</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"NumeroRecibo\":\"5708430\",\"DataVencimento\":\"Mar 5 2018 12:00:00:000AM\",\"DataBaixa\":\"Mar 5 2018 12:00:00:000AM\",\"CodigoUnidade\":\"0001\",\"ComplementoUnidade\":\"101\",\"ContratoLocacao\":\"\",\"CodigoImovel\":\"\",\"EnderecoImovel\":\" \",\"ValorRecibo\":\"4316.42\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/posicao_recibos_grafico_analitico?login=10001&month=03&year=2018&paid=S&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Posicao_recibos_grafico_analitico.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/recibos",
    "title": "Recibos",
    "name": "recibos",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"Codigo\":\"0001\",\"Complemento\":\"101\",\"NumeroRecibo\":\"5829679\",\"DataVencimento\":\"Jun 5 2018 12:00:00:000AM\",\"InstrucaoRecibo\":\" \"},{\"Codigo\":\"0002\",\"Complemento\":\"201\",\"NumeroRecibo\":\"5829680\",\"DataVencimento\":\"Jun 5 2018 12:00:00:000AM\",\"InstrucaoRecibo\":\" \"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/recibos?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Recibos.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/saldo_atualizado",
    "title": "Saldo Atualizado",
    "name": "saldo_atualizado",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[{\"CodigoCliente\":\"00001\",\"MesAno\":\"05\\/2018\",\"CodigoConta\":\"001\",\"Conta\":\"001-CONDOM\\u00cdNIO\",\"SaldoInicial\":\"-70330.42\",\"TotalD\":\"6869.06\",\"TotalC\":\"16834.28\",\"SaldoFinal\":\"-60365.2\",\"Tipo\":\"0\",\"TipoSaldoFinal\":\"D\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/saldo_atualizado?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Saldo_atualizado.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/saldo_final_atualizado",
    "title": "Saldo Final Atualizado",
    "name": "saldo_final_atualizado",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[{\"MesAno\":\"03\\/2018\",\"SaldoAnterior\":\"9605.43\",\"TotalDebito\":\"86237.54\",\"TotalCredito\":\"96592.3\",\"SaldoFinal\":\"19960.19\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/saldo_final_atualizado?login=10001&month=03&year=2018&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Saldo_final_atualizado.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/saldo_final_meses",
    "title": "Saldo Final Meses",
    "name": "saldo_final_meses",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": "<p>Quantidade de meses para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[{\"MesAno\":\"04\\/2017\",\"SaldoAnterior\":\"8195.17\",\"TotalDebito\":\"15067.41\",\"TotalCredito\":\"17238.33\",\"SaldoFinal\":\"10366.09\"},{\"MesAno\":\"05\\/2017\",\"SaldoAnterior\":\"10366.09\",\"TotalDebito\":\"18705.84\",\"TotalCredito\":\"25340.96\",\"SaldoFinal\":\"17001.21\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/saldo_final_meses?login=10001&month=03&year=2018&total=12&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Saldo_final_meses.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/financeiro/saldo_final_seis_meses",
    "title": "Saldo Final 6 Meses",
    "name": "saldo_final_seis_meses",
    "group": "financeiro",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mês para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Ano para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": "<p>Quantidade de meses para consulta</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[{\"MesAno\":\"04\\/2017\",\"SaldoAnterior\":\"8195.17\",\"TotalDebito\":\"15067.41\",\"TotalCredito\":\"17238.33\",\"SaldoFinal\":\"10366.09\"},{\"MesAno\":\"05\\/2017\",\"SaldoAnterior\":\"10366.09\",\"TotalDebito\":\"18705.84\",\"TotalCredito\":\"25340.96\",\"SaldoFinal\":\"17001.21\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/financeiro/saldo_final_seis_meses?login=10001&month=03&year=2018&total=12&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/financeiro/Saldo_final_seis_meses.php",
    "groupTitle": "financeiro"
  },
  {
    "type": "get",
    "url": "/condominio/gerente",
    "title": "Gerente",
    "name": "gerente",
    "group": "gerente",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do Administrador</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"id\":\"13\",\"campo_tipo\":\"7\",\"identificacao\":\"0076\",\"nome\":\"Wagner Peron Bastos\",\"endereco\":\"\",\"complemento\":\"\",\"bairro\":\"\",\"municipio\":\"\",\"estado\":\"\",\"cep\":\"\",\"tipo_endereco\":\"\",\"email\":\"wagner.bastos@bcfadm.com.br\",\"telefone_residencial\":\"\",\"telefone_comercial\":\"\",\"telefone_movel\":\"3543-2906\",\"fax\":\"\",\"login\":\"\",\"senha\":\"\",\"clientes\":\"a:883:{i:0;s:5:\\\"10540\\\";i:1;s:5:\\\"10077\\\";i:2;s:5:\\\"10115\\\";i:3;s:5:\\\"10236\\\";i:4;s:5:\\\"10540\\\";i:5;s:5:\\\"10612\\\";i:6;s:5:\\\"10669\\\";i:7;s:5:\\\"10674\\\";i:8;s:5:\\\"10697\\\";i:9;s:5:\\\"10704\\\";i:10;s:5:\\\"10708\\\";i:11;s:5:\\\"10713\\\";i:12;s:5:\\\"10717\\\";i:13;s:5:\\\"10718\\\";i:14;s:5:\\\"10720\\\";i:15;s:5:\\\"10722\\\";i:16;s:5:\\\"10723\\\";i:17;s:5:\\\"10724\\\";i:18;s:5:\\\"10726\\\";i:19;s:5:\\\"10730\\\";i:20;s:5:\\\"10735\\\";i:21;s:5:\\\"10736\\\";i:22;s:5:\\\"10737\\\";i:23;s:5:\\\"10738\\\";i:24;s:5:\\\"10739\\\";i:25;s:5:\\\"10740\\\";i:26;s:5:\\\"10743\\\";i:27;s:5:\\\"10746\\\";i:28;s:5:\\\"10747\\\";i:29;s:5:\\\"10750\\\";i:30;s:5:\\\"10752\\\";i:31;s:5:\\\"10757\\\";i:32;s:5:\\\"10758\\\";i:33;s:5:\\\"10759\\\";i:34;s:5:\\\"10760\\\";i:35;s:5:\\\"10762\\\";i:36;s:5:\\\"10764\\\";i:37;s:5:\\\"10770\\\";i:38;s:5:\\\"10789\\\";i:39;s:5:\\\"10855\\\";i:40;s:5:\\\"10925\\\";i:41;s:5:\\\"10944\\\";i:42;s:5:\\\"10077\\\";i:43;s:5:\\\"10115\\\";i:44;s:5:\\\"10236\\\";i:45;s:5:\\\"10077\\\";i:46;s:5:\\\"10115\\\";i:47;s:5:\\\"10236\\\";i:48;s:5:\\\"10077\\\";i:49;s:5:\\\"10115\\\";i:50;s:5:\\\"10236\\\";i:51;s:5:\\\"10540\\\";i:52;s:5:\\\"10612\\\";i:53;s:5:\\\"10669\\\";i:54;s:5:\\\"10674\\\";i:55;s:5:\\\"10697\\\";i:56;s:5:\\\"10704\\\";i:57;s:5:\\\"10708\\\";i:58;s:5:\\\"10713\\\";i:59;s:5:\\\"10717\\\";i:60;s:5:\\\"10718\\\";i:61;s:5:\\\"10720\\\";i:62;s:5:\\\"10722\\\";i:63;s:5:\\\"10723\\\";i:64;s:5:\\\"10724\\\";i:65;s:5:\\\"10726\\\";i:66;s:5:\\\"10730\\\";i:67;s:5:\\\"10735\\\";i:68;s:5:\\\"10736\\\";i:69;s:5:\\\"10737\\\";i:70;s:5:\\\"10738\\\";i:71;s:5:\\\"10739\\\";i:72;s:5:\\\"10740\\\";i:73;s:5:\\\"10743\\\";i:74;s:5:\\\"10746\\\";i:75;s:5:\\\"10747\\\";i:76;s:5:\\\"10750\\\";i:77;s:5:\\\"10752\\\";i:78;s:5:\\\"10757\\\";i:79;s:5:\\\"10758\\\";i:80;s:5:\\\"10759\\\";i:81;s:5:\\\"10760\\\";i:82;s:5:\\\"10762\\\";i:83;s:5:\\\"10764\\\";i:84;s:5:\\\"10770\\\";i:85;s:5:\\\"10789\\\";i:86;s:5:\\\"10855\\\";i:87;s:5:\\\"10925\\\";i:88;s:5:\\\"10944\\\";i:89;s:5:\\\"10077\\\";i:90;s:5:\\\"10115\\\";i:91;s:5:\\\"10236\\\";i:92;s:5:\\\"10540\\\";i:93;s:5:\\\"10612\\\";i:94;s:5:\\\"10077\\\";i:95;s:5:\\\"10115\\\";i:96;s:5:\\\"10236\\\";i:97;s:5:\\\"10540\\\";i:98;s:5:\\\"10612\\\";i:99;s:5:\\\"10669\\\";i:100;s:5:\\\"10674\\\";i:101;s:5:\\\"10697\\\";i:102;s:5:\\\"10704\\\";i:103;s:5:\\\"10708\\\";i:104;s:5:\\\"10713\\\";i:105;s:5:\\\"10717\\\";i:106;s:5:\\\"10718\\\";i:107;s:5:\\\"10720\\\";i:108;s:5:\\\"10722\\\";i:109;s:5:\\\"10723\\\";i:110;s:5:\\\"10724\\\";i:111;s:5:\\\"10726\\\";i:112;s:5:\\\"10730\\\";i:113;s:5:\\\"10735\\\";i:114;s:5:\\\"10736\\\";i:115;s:5:\\\"10737\\\";i:116;s:5:\\\"10738\\\";i:117;s:5:\\\"10739\\\";i:118;s:5:\\\"10740\\\";i:119;s:5:\\\"10743\\\";i:120;s:5:\\\"10746\\\";i:121;s:5:\\\"10747\\\";i:122;s:5:\\\"10750\\\";i:123;s:5:\\\"10752\\\";i:124;s:5:\\\"10757\\\";i:125;s:5:\\\"10758\\\";i:126;s:5:\\\"10759\\\";i:127;s:5:\\\"10760\\\";i:128;s:5:\\\"10762\\\";i:129;s:5:\\\"10764\\\";i:130;s:5:\\\"10770\\\";i:131;s:5:\\\"10789\\\";i:132;s:5:\\\"10855\\\";i:133;s:5:\\\"10925\\\";i:134;s:5:\\\"10944\\\";i:135;s:5:\\\"10077\\\";i:136;s:5:\\\"10115\\\";i:137;s:5:\\\"10236\\\";i:138;s:5:\\\"10540\\\";i:139;s:5:\\\"10612\\\";i:140;s:5:\\\"10669\\\";i:141;s:5:\\\"10674\\\";i:142;s:5:\\\"10697\\\";i:143;s:5:\\\"10704\\\";i:144;s:5:\\\"10708\\\";i:145;s:5:\\\"10713\\\";i:146;s:5:\\\"10717\\\";i:147;s:5:\\\"10718\\\";i:148;s:5:\\\"10720\\\";i:149;s:5:\\\"10722\\\";i:150;s:5:\\\"10723\\\";i:151;s:5:\\\"10724\\\";i:152;s:5:\\\"10726\\\";i:153;s:5:\\\"10730\\\";i:154;s:5:\\\"10735\\\";i:155;s:5:\\\"10736\\\";i:156;s:5:\\\"10737\\\";i:157;s:5:\\\"10738\\\";i:158;s:5:\\\"10739\\\";i:159;s:5:\\\"10740\\\";i:160;s:5:\\\"10743\\\";i:161;s:5:\\\"10746\\\";i:162;s:5:\\\"10747\\\";i:163;s:5:\\\"10750\\\";i:164;s:5:\\\"10752\\\";i:165;s:5:\\\"10757\\\";i:166;s:5:\\\"10758\\\";i:167;s:5:\\\"10759\\\";i:168;s:5:\\\"10760\\\";i:169;s:5:\\\"10762\\\";i:170;s:5:\\\"10764\\\";i:171;s:5:\\\"10770\\\";i:172;s:5:\\\"10789\\\";i:173;s:5:\\\"10855\\\";i:174;s:5:\\\"10925\\\";i:175;s:5:\\\"10944\\\";i:176;s:5:\\\"10077\\\";i:177;s:5:\\\"10115\\\";i:178;s:5:\\\"10236\\\";i:179;s:5:\\\"10540\\\";i:180;s:5:\\\"10612\\\";i:181;s:5:\\\"10669\\\";i:182;s:5:\\\"10674\\\";i:183;s:5:\\\"10697\\\";i:184;s:5:\\\"10704\\\";i:185;s:5:\\\"10708\\\";i:186;s:5:\\\"10713\\\";i:187;s:5:\\\"10717\\\";i:188;s:5:\\\"10718\\\";i:189;s:5:\\\"10720\\\";i:190;s:5:\\\"10722\\\";i:191;s:5:\\\"10723\\\";i:192;s:5:\\\"10724\\\";i:193;s:5:\\\"10726\\\";i:194;s:5:\\\"10730\\\";i:195;s:5:\\\"10735\\\";i:196;s:5:\\\"10736\\\";i:197;s:5:\\\"10737\\\";i:198;s:5:\\\"10738\\\";i:199;s:5:\\\"10739\\\";i:200;s:5:\\\"10740\\\";i:201;s:5:\\\"10743\\\";i:202;s:5:\\\"10746\\\";i:203;s:5:\\\"10747\\\";i:204;s:5:\\\"10750\\\";i:205;s:5:\\\"10752\\\";i:206;s:5:\\\"10757\\\";i:207;s:5:\\\"10758\\\";i:208;s:5:\\\"10759\\\";i:209;s:5:\\\"10760\\\";i:210;s:5:\\\"10762\\\";i:211;s:5:\\\"10764\\\";i:212;s:5:\\\"10770\\\";i:213;s:5:\\\"10789\\\";i:214;s:5:\\\"10855\\\";i:215;s:5:\\\"10925\\\";i:216;s:5:\\\"10944\\\";i:217;s:5:\\\"10077\\\";i:218;s:5:\\\"10115\\\";i:219;s:5:\\\"10236\\\";i:220;s:5:\\\"10077\\\";i:221;s:5:\\\"10115\\\";i:222;s:5:\\\"10236\\\";i:223;s:5:\\\"10077\\\";i:224;s:5:\\\"10115\\\";i:225;s:5:\\\"10236\\\";i:226;s:5:\\\"10540\\\";i:227;s:5:\\\"10612\\\";i:228;s:5:\\\"10669\\\";i:229;s:5:\\\"10674\\\";i:230;s:5:\\\"10697\\\";i:231;s:5:\\\"10704\\\";i:232;s:5:\\\"10708\\\";i:233;s:5:\\\"10713\\\";i:234;s:5:\\\"10717\\\";i:235;s:5:\\\"10718\\\";i:236;s:5:\\\"10720\\\";i:237;s:5:\\\"10722\\\";i:238;s:5:\\\"10723\\\";i:239;s:5:\\\"10724\\\";i:240;s:5:\\\"10726\\\";i:241;s:5:\\\"10730\\\";i:242;s:5:\\\"10735\\\";i:243;s:5:\\\"10736\\\";i:244;s:5:\\\"10737\\\";i:245;s:5:\\\"10738\\\";i:246;s:5:\\\"10739\\\";i:247;s:5:\\\"10740\\\";i:248;s:5:\\\"10743\\\";i:249;s:5:\\\"10746\\\";i:250;s:5:\\\"10747\\\";i:251;s:5:\\\"10750\\\";i:252;s:5:\\\"10752\\\";i:253;s:5:\\\"10757\\\";i:254;s:5:\\\"10758\\\";i:255;s:5:\\\"10759\\\";i:256;s:5:\\\"10760\\\";i:257;s:5:\\\"10762\\\";i:258;s:5:\\\"10764\\\";i:259;s:5:\\\"10770\\\";i:260;s:5:\\\"10789\\\";i:261;s:5:\\\"10855\\\";i:262;s:5:\\\"10925\\\";i:263;s:5:\\\"10944\\\";i:264;s:5:\\\"10077\\\";i:265;s:5:\\\"10115\\\";i:266;s:5:\\\"10236\\\";i:267;s:5:\\\"10540\\\";i:268;s:5:\\\"10612\\\";i:269;s:5:\\\"10669\\\";i:270;s:5:\\\"10674\\\";i:271;s:5:\\\"10697\\\";i:272;s:5:\\\"10704\\\";i:273;s:5:\\\"10708\\\";i:274;s:5:\\\"10713\\\";i:275;s:5:\\\"10717\\\";i:276;s:5:\\\"10718\\\";i:277;s:5:\\\"10720\\\";i:278;s:5:\\\"10722\\\";i:279;s:5:\\\"10723\\\";i:280;s:5:\\\"10724\\\";i:281;s:5:\\\"10726\\\";i:282;s:5:\\\"10730\\\";i:283;s:5:\\\"10735\\\";i:284;s:5:\\\"10736\\\";i:285;s:5:\\\"10737\\\";i:286;s:5:\\\"10738\\\";i:287;s:5:\\\"10739\\\";i:288;s:5:\\\"10740\\\";i:289;s:5:\\\"10743\\\";i:290;s:5:\\\"10746\\\";i:291;s:5:\\\"10747\\\";i:292;s:5:\\\"10750\\\";i:293;s:5:\\\"10752\\\";i:294;s:5:\\\"10757\\\";i:295;s:5:\\\"10758\\\";i:296;s:5:\\\"10759\\\";i:297;s:5:\\\"10760\\\";i:298;s:5:\\\"10762\\\";i:299;s:5:\\\"10764\\\";i:300;s:5:\\\"10770\\\";i:301;s:5:\\\"10789\\\";i:302;s:5:\\\"10855\\\";i:303;s:5:\\\"10925\\\";i:304;s:5:\\\"10944\\\";i:305;s:5:\\\"10077\\\";i:306;s:5:\\\"10115\\\";i:307;s:5:\\\"10236\\\";i:308;s:5:\\\"10540\\\";i:309;s:5:\\\"10612\\\";i:310;s:5:\\\"10669\\\";i:311;s:5:\\\"10674\\\";i:312;s:5:\\\"10697\\\";i:313;s:5:\\\"10704\\\";i:314;s:5:\\\"10708\\\";i:315;s:5:\\\"10713\\\";i:316;s:5:\\\"10717\\\";i:317;s:5:\\\"10718\\\";i:318;s:5:\\\"10720\\\";i:319;s:5:\\\"10722\\\";i:320;s:5:\\\"10723\\\";i:321;s:5:\\\"10724\\\";i:322;s:5:\\\"10726\\\";i:323;s:5:\\\"10730\\\";i:324;s:5:\\\"10735\\\";i:325;s:5:\\\"10736\\\";i:326;s:5:\\\"10737\\\";i:327;s:5:\\\"10738\\\";i:328;s:5:\\\"10739\\\";i:329;s:5:\\\"10740\\\";i:330;s:5:\\\"10743\\\";i:331;s:5:\\\"10746\\\";i:332;s:5:\\\"10747\\\";i:333;s:5:\\\"10750\\\";i:334;s:5:\\\"10752\\\";i:335;s:5:\\\"10757\\\";i:336;s:5:\\\"10758\\\";i:337;s:5:\\\"10759\\\";i:338;s:5:\\\"10760\\\";i:339;s:5:\\\"10762\\\";i:340;s:5:\\\"10764\\\";i:341;s:5:\\\"10770\\\";i:342;s:5:\\\"10789\\\";i:343;s:5:\\\"10855\\\";i:344;s:5:\\\"10925\\\";i:345;s:5:\\\"10944\\\";i:346;s:5:\\\"10077\\\";i:347;s:5:\\\"10115\\\";i:348;s:5:\\\"10236\\\";i:349;s:5:\\\"10077\\\";i:350;s:5:\\\"10077\\\";i:351;s:5:\\\"10115\\\";i:352;s:5:\\\"10115\\\";i:353;s:5:\\\"10236\\\";i:354;s:5:\\\"10236\\\";i:355;s:5:\\\"10540\\\";i:356;s:5:\\\"10612\\\";i:357;s:5:\\\"10669\\\";i:358;s:5:\\\"10674\\\";i:359;s:5:\\\"10697\\\";i:360;s:5:\\\"10704\\\";i:361;s:5:\\\"10708\\\";i:362;s:5:\\\"10713\\\";i:363;s:5:\\\"10717\\\";i:364;s:5:\\\"10718\\\";i:365;s:5:\\\"10720\\\";i:366;s:5:\\\"10722\\\";i:367;s:5:\\\"10723\\\";i:368;s:5:\\\"10724\\\";i:369;s:5:\\\"10726\\\";i:370;s:5:\\\"10540\\\";i:371;s:5:\\\"10730\\\";i:372;s:5:\\\"10735\\\";i:373;s:5:\\\"10736\\\";i:374;s:5:\\\"10737\\\";i:375;s:5:\\\"10738\\\";i:376;s:5:\\\"10739\\\";i:377;s:5:\\\"10740\\\";i:378;s:5:\\\"10743\\\";i:379;s:5:\\\"10746\\\";i:380;s:5:\\\"10747\\\";i:381;s:5:\\\"10750\\\";i:382;s:5:\\\"10752\\\";i:383;s:5:\\\"10757\\\";i:384;s:5:\\\"10758\\\";i:385;s:5:\\\"10759\\\";i:386;s:5:\\\"10760\\\";i:387;s:5:\\\"10762\\\";i:388;s:5:\\\"10764\\\";i:389;s:5:\\\"10770\\\";i:390;s:5:\\\"10612\\\";i:391;s:5:\\\"10669\\\";i:392;s:5:\\\"10674\\\";i:393;s:5:\\\"10697\\\";i:394;s:5:\\\"10704\\\";i:395;s:5:\\\"10708\\\";i:396;s:5:\\\"10713\\\";i:397;s:5:\\\"10717\\\";i:398;s:5:\\\"10718\\\";i:399;s:5:\\\"10720\\\";i:400;s:5:\\\"10722\\\";i:401;s:5:\\\"10723\\\";i:402;s:5:\\\"10724\\\";i:403;s:5:\\\"10726\\\";i:404;s:5:\\\"10730\\\";i:405;s:5:\\\"10735\\\";i:406;s:5:\\\"10736\\\";i:407;s:5:\\\"10737\\\";i:408;s:5:\\\"10738\\\";i:409;s:5:\\\"10739\\\";i:410;s:5:\\\"10740\\\";i:411;s:5:\\\"10743\\\";i:412;s:5:\\\"10746\\\";i:413;s:5:\\\"10747\\\";i:414;s:5:\\\"10750\\\";i:415;s:5:\\\"10752\\\";i:416;s:5:\\\"10757\\\";i:417;s:5:\\\"10758\\\";i:418;s:5:\\\"10759\\\";i:419;s:5:\\\"10760\\\";i:420;s:5:\\\"10762\\\";i:421;s:5:\\\"10764\\\";i:422;s:5:\\\"10770\\\";i:423;s:5:\\\"10789\\\";i:424;s:5:\\\"10077\\\";i:425;s:5:\\\"10115\\\";i:426;s:5:\\\"10236\\\";i:427;s:5:\\\"10077\\\";i:428;s:5:\\\"10115\\\";i:429;s:5:\\\"10236\\\";i:430;s:5:\\\"10540\\\";i:431;s:5:\\\"10612\\\";i:432;s:5:\\\"10669\\\";i:433;s:5:\\\"10674\\\";i:434;s:5:\\\"10697\\\";i:435;s:5:\\\"10704\\\";i:436;s:5:\\\"10708\\\";i:437;s:5:\\\"10713\\\";i:438;s:5:\\\"10717\\\";i:439;s:5:\\\"10718\\\";i:440;s:5:\\\"10720\\\";i:441;s:5:\\\"10722\\\";i:442;s:5:\\\"10723\\\";i:443;s:5:\\\"10724\\\";i:444;s:5:\\\"10726\\\";i:445;s:5:\\\"10730\\\";i:446;s:5:\\\"10735\\\";i:447;s:5:\\\"10736\\\";i:448;s:5:\\\"10737\\\";i:449;s:5:\\\"10738\\\";i:450;s:5:\\\"10739\\\";i:451;s:5:\\\"10740\\\";i:452;s:5:\\\"10743\\\";i:453;s:5:\\\"10746\\\";i:454;s:5:\\\"10747\\\";i:455;s:5:\\\"10750\\\";i:456;s:5:\\\"10752\\\";i:457;s:5:\\\"10757\\\";i:458;s:5:\\\"10758\\\";i:459;s:5:\\\"10759\\\";i:460;s:5:\\\"10760\\\";i:461;s:5:\\\"10762\\\";i:462;s:5:\\\"10764\\\";i:463;s:5:\\\"10770\\\";i:464;s:5:\\\"10789\\\";i:465;s:5:\\\"10855\\\";i:466;s:5:\\\"10925\\\";i:467;s:5:\\\"10944\\\";i:468;s:5:\\\"10077\\\";i:469;s:5:\\\"10115\\\";i:470;s:5:\\\"10236\\\";i:471;s:5:\\\"10540\\\";i:472;s:5:\\\"10612\\\";i:473;s:5:\\\"10669\\\";i:474;s:5:\\\"10674\\\";i:475;s:5:\\\"10697\\\";i:476;s:5:\\\"10704\\\";i:477;s:5:\\\"10708\\\";i:478;s:5:\\\"10713\\\";i:479;s:5:\\\"10717\\\";i:480;s:5:\\\"10718\\\";i:481;s:5:\\\"10720\\\";i:482;s:5:\\\"10722\\\";i:483;s:5:\\\"10723\\\";i:484;s:5:\\\"10724\\\";i:485;s:5:\\\"10726\\\";i:486;s:5:\\\"10730\\\";i:487;s:5:\\\"10735\\\";i:488;s:5:\\\"10736\\\";i:489;s:5:\\\"10737\\\";i:490;s:5:\\\"10738\\\";i:491;s:5:\\\"10739\\\";i:492;s:5:\\\"10740\\\";i:493;s:5:\\\"10743\\\";i:494;s:5:\\\"10746\\\";i:495;s:5:\\\"10747\\\";i:496;s:5:\\\"10750\\\";i:497;s:5:\\\"10752\\\";i:498;s:5:\\\"10757\\\";i:499;s:5:\\\"10758\\\";i:500;s:5:\\\"10759\\\";i:501;s:5:\\\"10760\\\";i:502;s:5:\\\"10762\\\";i:503;s:5:\\\"10764\\\";i:504;s:5:\\\"10770\\\";i:505;s:5:\\\"10789\\\";i:506;s:5:\\\"10077\\\";i:507;s:5:\\\"10115\\\";i:508;s:5:\\\"10236\\\";i:509;s:5:\\\"10540\\\";i:510;s:5:\\\"10612\\\";i:511;s:5:\\\"10669\\\";i:512;s:5:\\\"10674\\\";i:513;s:5:\\\"10697\\\";i:514;s:5:\\\"10704\\\";i:515;s:5:\\\"10708\\\";i:516;s:5:\\\"10713\\\";i:517;s:5:\\\"10717\\\";i:518;s:5:\\\"10718\\\";i:519;s:5:\\\"10720\\\";i:520;s:5:\\\"10722\\\";i:521;s:5:\\\"10723\\\";i:522;s:5:\\\"10724\\\";i:523;s:5:\\\"10726\\\";i:524;s:5:\\\"10730\\\";i:525;s:5:\\\"10735\\\";i:526;s:5:\\\"10736\\\";i:527;s:5:\\\"10737\\\";i:528;s:5:\\\"10738\\\";i:529;s:5:\\\"10739\\\";i:530;s:5:\\\"10740\\\";i:531;s:5:\\\"10743\\\";i:532;s:5:\\\"10746\\\";i:533;s:5:\\\"10747\\\";i:534;s:5:\\\"10750\\\";i:535;s:5:\\\"10752\\\";i:536;s:5:\\\"10757\\\";i:537;s:5:\\\"10758\\\";i:538;s:5:\\\"10759\\\";i:539;s:5:\\\"10760\\\";i:540;s:5:\\\"10762\\\";i:541;s:5:\\\"10764\\\";i:542;s:5:\\\"10770\\\";i:543;s:5:\\\"10789\\\";i:544;s:5:\\\"10855\\\";i:545;s:5:\\\"10925\\\";i:546;s:5:\\\"10944\\\";i:547;s:5:\\\"10077\\\";i:548;s:5:\\\"10115\\\";i:549;s:5:\\\"10236\\\";i:550;s:5:\\\"10077\\\";i:551;s:5:\\\"10077\\\";i:552;s:5:\\\"10115\\\";i:553;s:5:\\\"10077\\\";i:554;s:5:\\\"10115\\\";i:555;s:5:\\\"10115\\\";i:556;s:5:\\\"10236\\\";i:557;s:5:\\\"10236\\\";i:558;s:5:\\\"10236\\\";i:559;s:5:\\\"10540\\\";i:560;s:5:\\\"10612\\\";i:561;s:5:\\\"10669\\\";i:562;s:5:\\\"10674\\\";i:563;s:5:\\\"10697\\\";i:564;s:5:\\\"10704\\\";i:565;s:5:\\\"10708\\\";i:566;s:5:\\\"10540\\\";i:567;s:5:\\\"10713\\\";i:568;s:5:\\\"10717\\\";i:569;s:5:\\\"10718\\\";i:570;s:5:\\\"10720\\\";i:571;s:5:\\\"10722\\\";i:572;s:5:\\\"10723\\\";i:573;s:5:\\\"10724\\\";i:574;s:5:\\\"10726\\\";i:575;s:5:\\\"10540\\\";i:576;s:5:\\\"10730\\\";i:577;s:5:\\\"10735\\\";i:578;s:5:\\\"10736\\\";i:579;s:5:\\\"10737\\\";i:580;s:5:\\\"10738\\\";i:581;s:5:\\\"10540\\\";i:582;s:5:\\\"10739\\\";i:583;s:5:\\\"10740\\\";i:584;s:5:\\\"10743\\\";i:585;s:5:\\\"10746\\\";i:586;s:5:\\\"10747\\\";i:587;s:5:\\\"10750\\\";i:588;s:5:\\\"10752\\\";i:589;s:5:\\\"10077\\\";i:590;s:5:\\\"10115\\\";i:591;s:5:\\\"10236\\\";i:592;s:5:\\\"10540\\\";i:593;s:5:\\\"10612\\\";i:594;s:5:\\\"10669\\\";i:595;s:5:\\\"10674\\\";i:596;s:5:\\\"10697\\\";i:597;s:5:\\\"10704\\\";i:598;s:5:\\\"10708\\\";i:599;s:5:\\\"10713\\\";i:600;s:5:\\\"10717\\\";i:601;s:5:\\\"10718\\\";i:602;s:5:\\\"10720\\\";i:603;s:5:\\\"10722\\\";i:604;s:5:\\\"10723\\\";i:605;s:5:\\\"10724\\\";i:606;s:5:\\\"10726\\\";i:607;s:5:\\\"10730\\\";i:608;s:5:\\\"10735\\\";i:609;s:5:\\\"10736\\\";i:610;s:5:\\\"10737\\\";i:611;s:5:\\\"10738\\\";i:612;s:5:\\\"10739\\\";i:613;s:5:\\\"10740\\\";i:614;s:5:\\\"10743\\\";i:615;s:5:\\\"10746\\\";i:616;s:5:\\\"10747\\\";i:617;s:5:\\\"10750\\\";i:618;s:5:\\\"10752\\\";i:619;s:5:\\\"10757\\\";i:620;s:5:\\\"10758\\\";i:621;s:5:\\\"10759\\\";i:622;s:5:\\\"10760\\\";i:623;s:5:\\\"10762\\\";i:624;s:5:\\\"10764\\\";i:625;s:5:\\\"10770\\\";i:626;s:5:\\\"10789\\\";i:627;s:5:\\\"10855\\\";i:628;s:5:\\\"10925\\\";i:629;s:5:\\\"10944\\\";i:630;s:5:\\\"10077\\\";i:631;s:5:\\\"10115\\\";i:632;s:5:\\\"10236\\\";i:633;s:5:\\\"10540\\\";i:634;s:5:\\\"10612\\\";i:635;s:5:\\\"10669\\\";i:636;s:5:\\\"10674\\\";i:637;s:5:\\\"10697\\\";i:638;s:5:\\\"10704\\\";i:639;s:5:\\\"10708\\\";i:640;s:5:\\\"10713\\\";i:641;s:5:\\\"10717\\\";i:642;s:5:\\\"10718\\\";i:643;s:5:\\\"10720\\\";i:644;s:5:\\\"10722\\\";i:645;s:5:\\\"10723\\\";i:646;s:5:\\\"10724\\\";i:647;s:5:\\\"10726\\\";i:648;s:5:\\\"10730\\\";i:649;s:5:\\\"10735\\\";i:650;s:5:\\\"10736\\\";i:651;s:5:\\\"10737\\\";i:652;s:5:\\\"10738\\\";i:653;s:5:\\\"10739\\\";i:654;s:5:\\\"10740\\\";i:655;s:5:\\\"10743\\\";i:656;s:5:\\\"10746\\\";i:657;s:5:\\\"10747\\\";i:658;s:5:\\\"10750\\\";i:659;s:5:\\\"10752\\\";i:660;s:5:\\\"10757\\\";i:661;s:5:\\\"10758\\\";i:662;s:5:\\\"10759\\\";i:663;s:5:\\\"10760\\\";i:664;s:5:\\\"10762\\\";i:665;s:5:\\\"10764\\\";i:666;s:5:\\\"10770\\\";i:667;s:5:\\\"10789\\\";i:668;s:5:\\\"10855\\\";i:669;s:5:\\\"10925\\\";i:670;s:5:\\\"10944\\\";i:671;s:5:\\\"10077\\\";i:672;s:5:\\\"10115\\\";i:673;s:5:\\\"10236\\\";i:674;s:5:\\\"10077\\\";i:675;s:5:\\\"10115\\\";i:676;s:5:\\\"10236\\\";i:677;s:5:\\\"10540\\\";i:678;s:5:\\\"10612\\\";i:679;s:5:\\\"10669\\\";i:680;s:5:\\\"10674\\\";i:681;s:5:\\\"10697\\\";i:682;s:5:\\\"10704\\\";i:683;s:5:\\\"10708\\\";i:684;s:5:\\\"10713\\\";i:685;s:5:\\\"10717\\\";i:686;s:5:\\\"10718\\\";i:687;s:5:\\\"10720\\\";i:688;s:5:\\\"10722\\\";i:689;s:5:\\\"10723\\\";i:690;s:5:\\\"10724\\\";i:691;s:5:\\\"10726\\\";i:692;s:5:\\\"10730\\\";i:693;s:5:\\\"10735\\\";i:694;s:5:\\\"10736\\\";i:695;s:5:\\\"10737\\\";i:696;s:5:\\\"10738\\\";i:697;s:5:\\\"10739\\\";i:698;s:5:\\\"10740\\\";i:699;s:5:\\\"10743\\\";i:700;s:5:\\\"10746\\\";i:701;s:5:\\\"10747\\\";i:702;s:5:\\\"10750\\\";i:703;s:5:\\\"10752\\\";i:704;s:5:\\\"10757\\\";i:705;s:5:\\\"10758\\\";i:706;s:5:\\\"10759\\\";i:707;s:5:\\\"10760\\\";i:708;s:5:\\\"10762\\\";i:709;s:5:\\\"10764\\\";i:710;s:5:\\\"10770\\\";i:711;s:5:\\\"10789\\\";i:712;s:5:\\\"10855\\\";i:713;s:5:\\\"10925\\\";i:714;s:5:\\\"10944\\\";i:715;s:5:\\\"10077\\\";i:716;s:5:\\\"10115\\\";i:717;s:5:\\\"10236\\\";i:718;s:5:\\\"10540\\\";i:719;s:5:\\\"10612\\\";i:720;s:5:\\\"10669\\\";i:721;s:5:\\\"10674\\\";i:722;s:5:\\\"10697\\\";i:723;s:5:\\\"10704\\\";i:724;s:5:\\\"10708\\\";i:725;s:5:\\\"10713\\\";i:726;s:5:\\\"10717\\\";i:727;s:5:\\\"10718\\\";i:728;s:5:\\\"10720\\\";i:729;s:5:\\\"10722\\\";i:730;s:5:\\\"10723\\\";i:731;s:5:\\\"10724\\\";i:732;s:5:\\\"10726\\\";i:733;s:5:\\\"10730\\\";i:734;s:5:\\\"10735\\\";i:735;s:5:\\\"10736\\\";i:736;s:5:\\\"10737\\\";i:737;s:5:\\\"10738\\\";i:738;s:5:\\\"10739\\\";i:739;s:5:\\\"10740\\\";i:740;s:5:\\\"10743\\\";i:741;s:5:\\\"10746\\\";i:742;s:5:\\\"10747\\\";i:743;s:5:\\\"10750\\\";i:744;s:5:\\\"10752\\\";i:745;s:5:\\\"10757\\\";i:746;s:5:\\\"10758\\\";i:747;s:5:\\\"10759\\\";i:748;s:5:\\\"10760\\\";i:749;s:5:\\\"10762\\\";i:750;s:5:\\\"10764\\\";i:751;s:5:\\\"10770\\\";i:752;s:5:\\\"10789\\\";i:753;s:5:\\\"10855\\\";i:754;s:5:\\\"10925\\\";i:755;s:5:\\\"10944\\\";i:756;s:5:\\\"10077\\\";i:757;s:5:\\\"10115\\\";i:758;s:5:\\\"10236\\\";i:759;s:5:\\\"10540\\\";i:760;s:5:\\\"10612\\\";i:761;s:5:\\\"10669\\\";i:762;s:5:\\\"10674\\\";i:763;s:5:\\\"10697\\\";i:764;s:5:\\\"10704\\\";i:765;s:5:\\\"10708\\\";i:766;s:5:\\\"10713\\\";i:767;s:5:\\\"10717\\\";i:768;s:5:\\\"10718\\\";i:769;s:5:\\\"10720\\\";i:770;s:5:\\\"10722\\\";i:771;s:5:\\\"10723\\\";i:772;s:5:\\\"10724\\\";i:773;s:5:\\\"10726\\\";i:774;s:5:\\\"10730\\\";i:775;s:5:\\\"10735\\\";i:776;s:5:\\\"10736\\\";i:777;s:5:\\\"10737\\\";i:778;s:5:\\\"10738\\\";i:779;s:5:\\\"10739\\\";i:780;s:5:\\\"10740\\\";i:781;s:5:\\\"10743\\\";i:782;s:5:\\\"10746\\\";i:783;s:5:\\\"10747\\\";i:784;s:5:\\\"10750\\\";i:785;s:5:\\\"10752\\\";i:786;s:5:\\\"10757\\\";i:787;s:5:\\\"10758\\\";i:788;s:5:\\\"10759\\\";i:789;s:5:\\\"10760\\\";i:790;s:5:\\\"10762\\\";i:791;s:5:\\\"10764\\\";i:792;s:5:\\\"10770\\\";i:793;s:5:\\\"10789\\\";i:794;s:5:\\\"10855\\\";i:795;s:5:\\\"10925\\\";i:796;s:5:\\\"10944\\\";i:797;s:5:\\\"10077\\\";i:798;s:5:\\\"10115\\\";i:799;s:5:\\\"10236\\\";i:800;s:5:\\\"10540\\\";i:801;s:5:\\\"10612\\\";i:802;s:5:\\\"10669\\\";i:803;s:5:\\\"10674\\\";i:804;s:5:\\\"10697\\\";i:805;s:5:\\\"10704\\\";i:806;s:5:\\\"10708\\\";i:807;s:5:\\\"10713\\\";i:808;s:5:\\\"10717\\\";i:809;s:5:\\\"10718\\\";i:810;s:5:\\\"10720\\\";i:811;s:5:\\\"10722\\\";i:812;s:5:\\\"10723\\\";i:813;s:5:\\\"10724\\\";i:814;s:5:\\\"10726\\\";i:815;s:5:\\\"10730\\\";i:816;s:5:\\\"10735\\\";i:817;s:5:\\\"10736\\\";i:818;s:5:\\\"10737\\\";i:819;s:5:\\\"10738\\\";i:820;s:5:\\\"10739\\\";i:821;s:5:\\\"10740\\\";i:822;s:5:\\\"10743\\\";i:823;s:5:\\\"10746\\\";i:824;s:5:\\\"10747\\\";i:825;s:5:\\\"10750\\\";i:826;s:5:\\\"10752\\\";i:827;s:5:\\\"10757\\\";i:828;s:5:\\\"10758\\\";i:829;s:5:\\\"10759\\\";i:830;s:5:\\\"10760\\\";i:831;s:5:\\\"10762\\\";i:832;s:5:\\\"10764\\\";i:833;s:5:\\\"10770\\\";i:834;s:5:\\\"10789\\\";i:835;s:5:\\\"10855\\\";i:836;s:5:\\\"10925\\\";i:837;s:5:\\\"10944\\\";i:838;s:5:\\\"10077\\\";i:839;s:5:\\\"10115\\\";i:840;s:5:\\\"10236\\\";i:841;s:5:\\\"10540\\\";i:842;s:5:\\\"10612\\\";i:843;s:5:\\\"10669\\\";i:844;s:5:\\\"10674\\\";i:845;s:5:\\\"10697\\\";i:846;s:5:\\\"10704\\\";i:847;s:5:\\\"10708\\\";i:848;s:5:\\\"10713\\\";i:849;s:5:\\\"10717\\\";i:850;s:5:\\\"10718\\\";i:851;s:5:\\\"10720\\\";i:852;s:5:\\\"10722\\\";i:853;s:5:\\\"10723\\\";i:854;s:5:\\\"10724\\\";i:855;s:5:\\\"10726\\\";i:856;s:5:\\\"10730\\\";i:857;s:5:\\\"10735\\\";i:858;s:5:\\\"10736\\\";i:859;s:5:\\\"10737\\\";i:860;s:5:\\\"10738\\\";i:861;s:5:\\\"10739\\\";i:862;s:5:\\\"10740\\\";i:863;s:5:\\\"10743\\\";i:864;s:5:\\\"10746\\\";i:865;s:5:\\\"10747\\\";i:866;s:5:\\\"10750\\\";i:867;s:5:\\\"10752\\\";i:868;s:5:\\\"10757\\\";i:869;s:5:\\\"10758\\\";i:870;s:5:\\\"10759\\\";i:871;s:5:\\\"10760\\\";i:872;s:5:\\\"10762\\\";i:873;s:5:\\\"10764\\\";i:874;s:5:\\\"10770\\\";i:875;s:5:\\\"10789\\\";i:876;s:5:\\\"10855\\\";i:877;s:5:\\\"10925\\\";i:878;s:5:\\\"10944\\\";i:879;s:5:\\\"10077\\\";i:880;s:5:\\\"10115\\\";i:881;s:5:\\\"10236\\\";i:882;s:5:\\\"10540\\\";}\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/gerente?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Gerente.php",
    "groupTitle": "gerente"
  },
  {
    "type": "post",
    "url": "/condominios/gerente/create",
    "title": "Gerente - Criar",
    "name": "gerente_create",
    "group": "gerente",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do Administrador. Somente Administrador pode cadastrar gerentes</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nome",
            "description": "<p>Nome</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "endereco",
            "description": "<p>Endereço</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacao",
            "description": "<p>Identificação (Exemplo: 0250)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "complemento",
            "description": "<p>Complemento</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "bairro",
            "description": "<p>Bairro</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "municipio",
            "description": "<p>Município</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "estado",
            "description": "<p>Estado</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cep",
            "description": "<p>CEP</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tipo_endereco",
            "description": "<p>Tipo do Endereço (Exemplo: Comercial ou Residencial)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>E-mail do Gerente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefone_residencial",
            "description": "<p>Telefone Residencial</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefone_comercial",
            "description": "<p>Telefone Comercial</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefone_movel",
            "description": "<p>Telefone Móvel</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fax",
            "description": "<p>Fax</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "login_gerente",
            "description": "<p>Login do Gerente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "senha",
            "description": "<p>Senha do Gerente</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "clientes",
            "description": "<p>Clientes (Array de IDs de Condomínios)</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "principal",
            "description": "<p>Gerente Principal? 0 = Não. 1 = Sim.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"O Gerente foi cadastrado com sucesso.\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/gerente/create?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Gerente.php",
    "groupTitle": "gerente"
  },
  {
    "type": "get",
    "url": "/condominio/gerente/delete",
    "title": "Gerente - Deletar",
    "name": "gerente_delete",
    "group": "gerente",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do Administrador</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>ID do gerente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"O Gerente foi deletado com sucesso.\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/gerente/delete?login=bcfadm&id=91&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Gerente.php",
    "groupTitle": "gerente"
  },
  {
    "type": "get",
    "url": "/condominio/gerente",
    "title": "Gerente - Filtrar por Condomínio",
    "name": "gerente_filter",
    "group": "gerente",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do Administrador</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ID",
            "description": "<p>do Condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"id\":\"13\",\"campo_tipo\":\"7\",\"identificacao\":\"0076\",\"nome\":\"Wagner Peron Bastos\",\"endereco\":\"\",\"complemento\":\"\",\"bairro\":\"\",\"municipio\":\"\",\"estado\":\"\",\"cep\":\"\",\"tipo_endereco\":\"\",\"email\":\"wagner.bastos@bcfadm.com.br\",\"telefone_residencial\":\"\",\"telefone_comercial\":\"\",\"telefone_movel\":\"3543-2906\",\"fax\":\"\",\"login\":\"\",\"senha\":\"\",\"clientes\":\"a:883:{i:0;s:5:\\\"10540\\\";i:1;s:5:\\\"10077\\\";i:2;s:5:\\\"10115\\\";i:3;s:5:\\\"10236\\\";i:4;s:5:\\\"10540\\\";i:5;s:5:\\\"10612\\\";i:6;s:5:\\\"10669\\\";i:7;s:5:\\\"10674\\\";i:8;s:5:\\\"10697\\\";i:9;s:5:\\\"10704\\\";i:10;s:5:\\\"10708\\\";i:11;s:5:\\\"10713\\\";i:12;s:5:\\\"10717\\\";i:13;s:5:\\\"10718\\\";i:14;s:5:\\\"10720\\\";i:15;s:5:\\\"10722\\\";i:16;s:5:\\\"10723\\\";i:17;s:5:\\\"10724\\\";i:18;s:5:\\\"10726\\\";i:19;s:5:\\\"10730\\\";i:20;s:5:\\\"10735\\\";i:21;s:5:\\\"10736\\\";i:22;s:5:\\\"10737\\\";i:23;s:5:\\\"10738\\\";i:24;s:5:\\\"10739\\\";i:25;s:5:\\\"10740\\\";i:26;s:5:\\\"10743\\\";i:27;s:5:\\\"10746\\\";i:28;s:5:\\\"10747\\\";i:29;s:5:\\\"10750\\\";i:30;s:5:\\\"10752\\\";i:31;s:5:\\\"10757\\\";i:32;s:5:\\\"10758\\\";i:33;s:5:\\\"10759\\\";i:34;s:5:\\\"10760\\\";i:35;s:5:\\\"10762\\\";i:36;s:5:\\\"10764\\\";i:37;s:5:\\\"10770\\\";i:38;s:5:\\\"10789\\\";i:39;s:5:\\\"10855\\\";i:40;s:5:\\\"10925\\\";i:41;s:5:\\\"10944\\\";i:42;s:5:\\\"10077\\\";i:43;s:5:\\\"10115\\\";i:44;s:5:\\\"10236\\\";i:45;s:5:\\\"10077\\\";i:46;s:5:\\\"10115\\\";i:47;s:5:\\\"10236\\\";i:48;s:5:\\\"10077\\\";i:49;s:5:\\\"10115\\\";i:50;s:5:\\\"10236\\\";i:51;s:5:\\\"10540\\\";i:52;s:5:\\\"10612\\\";i:53;s:5:\\\"10669\\\";i:54;s:5:\\\"10674\\\";i:55;s:5:\\\"10697\\\";i:56;s:5:\\\"10704\\\";i:57;s:5:\\\"10708\\\";i:58;s:5:\\\"10713\\\";i:59;s:5:\\\"10717\\\";i:60;s:5:\\\"10718\\\";i:61;s:5:\\\"10720\\\";i:62;s:5:\\\"10722\\\";i:63;s:5:\\\"10723\\\";i:64;s:5:\\\"10724\\\";i:65;s:5:\\\"10726\\\";i:66;s:5:\\\"10730\\\";i:67;s:5:\\\"10735\\\";i:68;s:5:\\\"10736\\\";i:69;s:5:\\\"10737\\\";i:70;s:5:\\\"10738\\\";i:71;s:5:\\\"10739\\\";i:72;s:5:\\\"10740\\\";i:73;s:5:\\\"10743\\\";i:74;s:5:\\\"10746\\\";i:75;s:5:\\\"10747\\\";i:76;s:5:\\\"10750\\\";i:77;s:5:\\\"10752\\\";i:78;s:5:\\\"10757\\\";i:79;s:5:\\\"10758\\\";i:80;s:5:\\\"10759\\\";i:81;s:5:\\\"10760\\\";i:82;s:5:\\\"10762\\\";i:83;s:5:\\\"10764\\\";i:84;s:5:\\\"10770\\\";i:85;s:5:\\\"10789\\\";i:86;s:5:\\\"10855\\\";i:87;s:5:\\\"10925\\\";i:88;s:5:\\\"10944\\\";i:89;s:5:\\\"10077\\\";i:90;s:5:\\\"10115\\\";i:91;s:5:\\\"10236\\\";i:92;s:5:\\\"10540\\\";i:93;s:5:\\\"10612\\\";i:94;s:5:\\\"10077\\\";i:95;s:5:\\\"10115\\\";i:96;s:5:\\\"10236\\\";i:97;s:5:\\\"10540\\\";i:98;s:5:\\\"10612\\\";i:99;s:5:\\\"10669\\\";i:100;s:5:\\\"10674\\\";i:101;s:5:\\\"10697\\\";i:102;s:5:\\\"10704\\\";i:103;s:5:\\\"10708\\\";i:104;s:5:\\\"10713\\\";i:105;s:5:\\\"10717\\\";i:106;s:5:\\\"10718\\\";i:107;s:5:\\\"10720\\\";i:108;s:5:\\\"10722\\\";i:109;s:5:\\\"10723\\\";i:110;s:5:\\\"10724\\\";i:111;s:5:\\\"10726\\\";i:112;s:5:\\\"10730\\\";i:113;s:5:\\\"10735\\\";i:114;s:5:\\\"10736\\\";i:115;s:5:\\\"10737\\\";i:116;s:5:\\\"10738\\\";i:117;s:5:\\\"10739\\\";i:118;s:5:\\\"10740\\\";i:119;s:5:\\\"10743\\\";i:120;s:5:\\\"10746\\\";i:121;s:5:\\\"10747\\\";i:122;s:5:\\\"10750\\\";i:123;s:5:\\\"10752\\\";i:124;s:5:\\\"10757\\\";i:125;s:5:\\\"10758\\\";i:126;s:5:\\\"10759\\\";i:127;s:5:\\\"10760\\\";i:128;s:5:\\\"10762\\\";i:129;s:5:\\\"10764\\\";i:130;s:5:\\\"10770\\\";i:131;s:5:\\\"10789\\\";i:132;s:5:\\\"10855\\\";i:133;s:5:\\\"10925\\\";i:134;s:5:\\\"10944\\\";i:135;s:5:\\\"10077\\\";i:136;s:5:\\\"10115\\\";i:137;s:5:\\\"10236\\\";i:138;s:5:\\\"10540\\\";i:139;s:5:\\\"10612\\\";i:140;s:5:\\\"10669\\\";i:141;s:5:\\\"10674\\\";i:142;s:5:\\\"10697\\\";i:143;s:5:\\\"10704\\\";i:144;s:5:\\\"10708\\\";i:145;s:5:\\\"10713\\\";i:146;s:5:\\\"10717\\\";i:147;s:5:\\\"10718\\\";i:148;s:5:\\\"10720\\\";i:149;s:5:\\\"10722\\\";i:150;s:5:\\\"10723\\\";i:151;s:5:\\\"10724\\\";i:152;s:5:\\\"10726\\\";i:153;s:5:\\\"10730\\\";i:154;s:5:\\\"10735\\\";i:155;s:5:\\\"10736\\\";i:156;s:5:\\\"10737\\\";i:157;s:5:\\\"10738\\\";i:158;s:5:\\\"10739\\\";i:159;s:5:\\\"10740\\\";i:160;s:5:\\\"10743\\\";i:161;s:5:\\\"10746\\\";i:162;s:5:\\\"10747\\\";i:163;s:5:\\\"10750\\\";i:164;s:5:\\\"10752\\\";i:165;s:5:\\\"10757\\\";i:166;s:5:\\\"10758\\\";i:167;s:5:\\\"10759\\\";i:168;s:5:\\\"10760\\\";i:169;s:5:\\\"10762\\\";i:170;s:5:\\\"10764\\\";i:171;s:5:\\\"10770\\\";i:172;s:5:\\\"10789\\\";i:173;s:5:\\\"10855\\\";i:174;s:5:\\\"10925\\\";i:175;s:5:\\\"10944\\\";i:176;s:5:\\\"10077\\\";i:177;s:5:\\\"10115\\\";i:178;s:5:\\\"10236\\\";i:179;s:5:\\\"10540\\\";i:180;s:5:\\\"10612\\\";i:181;s:5:\\\"10669\\\";i:182;s:5:\\\"10674\\\";i:183;s:5:\\\"10697\\\";i:184;s:5:\\\"10704\\\";i:185;s:5:\\\"10708\\\";i:186;s:5:\\\"10713\\\";i:187;s:5:\\\"10717\\\";i:188;s:5:\\\"10718\\\";i:189;s:5:\\\"10720\\\";i:190;s:5:\\\"10722\\\";i:191;s:5:\\\"10723\\\";i:192;s:5:\\\"10724\\\";i:193;s:5:\\\"10726\\\";i:194;s:5:\\\"10730\\\";i:195;s:5:\\\"10735\\\";i:196;s:5:\\\"10736\\\";i:197;s:5:\\\"10737\\\";i:198;s:5:\\\"10738\\\";i:199;s:5:\\\"10739\\\";i:200;s:5:\\\"10740\\\";i:201;s:5:\\\"10743\\\";i:202;s:5:\\\"10746\\\";i:203;s:5:\\\"10747\\\";i:204;s:5:\\\"10750\\\";i:205;s:5:\\\"10752\\\";i:206;s:5:\\\"10757\\\";i:207;s:5:\\\"10758\\\";i:208;s:5:\\\"10759\\\";i:209;s:5:\\\"10760\\\";i:210;s:5:\\\"10762\\\";i:211;s:5:\\\"10764\\\";i:212;s:5:\\\"10770\\\";i:213;s:5:\\\"10789\\\";i:214;s:5:\\\"10855\\\";i:215;s:5:\\\"10925\\\";i:216;s:5:\\\"10944\\\";i:217;s:5:\\\"10077\\\";i:218;s:5:\\\"10115\\\";i:219;s:5:\\\"10236\\\";i:220;s:5:\\\"10077\\\";i:221;s:5:\\\"10115\\\";i:222;s:5:\\\"10236\\\";i:223;s:5:\\\"10077\\\";i:224;s:5:\\\"10115\\\";i:225;s:5:\\\"10236\\\";i:226;s:5:\\\"10540\\\";i:227;s:5:\\\"10612\\\";i:228;s:5:\\\"10669\\\";i:229;s:5:\\\"10674\\\";i:230;s:5:\\\"10697\\\";i:231;s:5:\\\"10704\\\";i:232;s:5:\\\"10708\\\";i:233;s:5:\\\"10713\\\";i:234;s:5:\\\"10717\\\";i:235;s:5:\\\"10718\\\";i:236;s:5:\\\"10720\\\";i:237;s:5:\\\"10722\\\";i:238;s:5:\\\"10723\\\";i:239;s:5:\\\"10724\\\";i:240;s:5:\\\"10726\\\";i:241;s:5:\\\"10730\\\";i:242;s:5:\\\"10735\\\";i:243;s:5:\\\"10736\\\";i:244;s:5:\\\"10737\\\";i:245;s:5:\\\"10738\\\";i:246;s:5:\\\"10739\\\";i:247;s:5:\\\"10740\\\";i:248;s:5:\\\"10743\\\";i:249;s:5:\\\"10746\\\";i:250;s:5:\\\"10747\\\";i:251;s:5:\\\"10750\\\";i:252;s:5:\\\"10752\\\";i:253;s:5:\\\"10757\\\";i:254;s:5:\\\"10758\\\";i:255;s:5:\\\"10759\\\";i:256;s:5:\\\"10760\\\";i:257;s:5:\\\"10762\\\";i:258;s:5:\\\"10764\\\";i:259;s:5:\\\"10770\\\";i:260;s:5:\\\"10789\\\";i:261;s:5:\\\"10855\\\";i:262;s:5:\\\"10925\\\";i:263;s:5:\\\"10944\\\";i:264;s:5:\\\"10077\\\";i:265;s:5:\\\"10115\\\";i:266;s:5:\\\"10236\\\";i:267;s:5:\\\"10540\\\";i:268;s:5:\\\"10612\\\";i:269;s:5:\\\"10669\\\";i:270;s:5:\\\"10674\\\";i:271;s:5:\\\"10697\\\";i:272;s:5:\\\"10704\\\";i:273;s:5:\\\"10708\\\";i:274;s:5:\\\"10713\\\";i:275;s:5:\\\"10717\\\";i:276;s:5:\\\"10718\\\";i:277;s:5:\\\"10720\\\";i:278;s:5:\\\"10722\\\";i:279;s:5:\\\"10723\\\";i:280;s:5:\\\"10724\\\";i:281;s:5:\\\"10726\\\";i:282;s:5:\\\"10730\\\";i:283;s:5:\\\"10735\\\";i:284;s:5:\\\"10736\\\";i:285;s:5:\\\"10737\\\";i:286;s:5:\\\"10738\\\";i:287;s:5:\\\"10739\\\";i:288;s:5:\\\"10740\\\";i:289;s:5:\\\"10743\\\";i:290;s:5:\\\"10746\\\";i:291;s:5:\\\"10747\\\";i:292;s:5:\\\"10750\\\";i:293;s:5:\\\"10752\\\";i:294;s:5:\\\"10757\\\";i:295;s:5:\\\"10758\\\";i:296;s:5:\\\"10759\\\";i:297;s:5:\\\"10760\\\";i:298;s:5:\\\"10762\\\";i:299;s:5:\\\"10764\\\";i:300;s:5:\\\"10770\\\";i:301;s:5:\\\"10789\\\";i:302;s:5:\\\"10855\\\";i:303;s:5:\\\"10925\\\";i:304;s:5:\\\"10944\\\";i:305;s:5:\\\"10077\\\";i:306;s:5:\\\"10115\\\";i:307;s:5:\\\"10236\\\";i:308;s:5:\\\"10540\\\";i:309;s:5:\\\"10612\\\";i:310;s:5:\\\"10669\\\";i:311;s:5:\\\"10674\\\";i:312;s:5:\\\"10697\\\";i:313;s:5:\\\"10704\\\";i:314;s:5:\\\"10708\\\";i:315;s:5:\\\"10713\\\";i:316;s:5:\\\"10717\\\";i:317;s:5:\\\"10718\\\";i:318;s:5:\\\"10720\\\";i:319;s:5:\\\"10722\\\";i:320;s:5:\\\"10723\\\";i:321;s:5:\\\"10724\\\";i:322;s:5:\\\"10726\\\";i:323;s:5:\\\"10730\\\";i:324;s:5:\\\"10735\\\";i:325;s:5:\\\"10736\\\";i:326;s:5:\\\"10737\\\";i:327;s:5:\\\"10738\\\";i:328;s:5:\\\"10739\\\";i:329;s:5:\\\"10740\\\";i:330;s:5:\\\"10743\\\";i:331;s:5:\\\"10746\\\";i:332;s:5:\\\"10747\\\";i:333;s:5:\\\"10750\\\";i:334;s:5:\\\"10752\\\";i:335;s:5:\\\"10757\\\";i:336;s:5:\\\"10758\\\";i:337;s:5:\\\"10759\\\";i:338;s:5:\\\"10760\\\";i:339;s:5:\\\"10762\\\";i:340;s:5:\\\"10764\\\";i:341;s:5:\\\"10770\\\";i:342;s:5:\\\"10789\\\";i:343;s:5:\\\"10855\\\";i:344;s:5:\\\"10925\\\";i:345;s:5:\\\"10944\\\";i:346;s:5:\\\"10077\\\";i:347;s:5:\\\"10115\\\";i:348;s:5:\\\"10236\\\";i:349;s:5:\\\"10077\\\";i:350;s:5:\\\"10077\\\";i:351;s:5:\\\"10115\\\";i:352;s:5:\\\"10115\\\";i:353;s:5:\\\"10236\\\";i:354;s:5:\\\"10236\\\";i:355;s:5:\\\"10540\\\";i:356;s:5:\\\"10612\\\";i:357;s:5:\\\"10669\\\";i:358;s:5:\\\"10674\\\";i:359;s:5:\\\"10697\\\";i:360;s:5:\\\"10704\\\";i:361;s:5:\\\"10708\\\";i:362;s:5:\\\"10713\\\";i:363;s:5:\\\"10717\\\";i:364;s:5:\\\"10718\\\";i:365;s:5:\\\"10720\\\";i:366;s:5:\\\"10722\\\";i:367;s:5:\\\"10723\\\";i:368;s:5:\\\"10724\\\";i:369;s:5:\\\"10726\\\";i:370;s:5:\\\"10540\\\";i:371;s:5:\\\"10730\\\";i:372;s:5:\\\"10735\\\";i:373;s:5:\\\"10736\\\";i:374;s:5:\\\"10737\\\";i:375;s:5:\\\"10738\\\";i:376;s:5:\\\"10739\\\";i:377;s:5:\\\"10740\\\";i:378;s:5:\\\"10743\\\";i:379;s:5:\\\"10746\\\";i:380;s:5:\\\"10747\\\";i:381;s:5:\\\"10750\\\";i:382;s:5:\\\"10752\\\";i:383;s:5:\\\"10757\\\";i:384;s:5:\\\"10758\\\";i:385;s:5:\\\"10759\\\";i:386;s:5:\\\"10760\\\";i:387;s:5:\\\"10762\\\";i:388;s:5:\\\"10764\\\";i:389;s:5:\\\"10770\\\";i:390;s:5:\\\"10612\\\";i:391;s:5:\\\"10669\\\";i:392;s:5:\\\"10674\\\";i:393;s:5:\\\"10697\\\";i:394;s:5:\\\"10704\\\";i:395;s:5:\\\"10708\\\";i:396;s:5:\\\"10713\\\";i:397;s:5:\\\"10717\\\";i:398;s:5:\\\"10718\\\";i:399;s:5:\\\"10720\\\";i:400;s:5:\\\"10722\\\";i:401;s:5:\\\"10723\\\";i:402;s:5:\\\"10724\\\";i:403;s:5:\\\"10726\\\";i:404;s:5:\\\"10730\\\";i:405;s:5:\\\"10735\\\";i:406;s:5:\\\"10736\\\";i:407;s:5:\\\"10737\\\";i:408;s:5:\\\"10738\\\";i:409;s:5:\\\"10739\\\";i:410;s:5:\\\"10740\\\";i:411;s:5:\\\"10743\\\";i:412;s:5:\\\"10746\\\";i:413;s:5:\\\"10747\\\";i:414;s:5:\\\"10750\\\";i:415;s:5:\\\"10752\\\";i:416;s:5:\\\"10757\\\";i:417;s:5:\\\"10758\\\";i:418;s:5:\\\"10759\\\";i:419;s:5:\\\"10760\\\";i:420;s:5:\\\"10762\\\";i:421;s:5:\\\"10764\\\";i:422;s:5:\\\"10770\\\";i:423;s:5:\\\"10789\\\";i:424;s:5:\\\"10077\\\";i:425;s:5:\\\"10115\\\";i:426;s:5:\\\"10236\\\";i:427;s:5:\\\"10077\\\";i:428;s:5:\\\"10115\\\";i:429;s:5:\\\"10236\\\";i:430;s:5:\\\"10540\\\";i:431;s:5:\\\"10612\\\";i:432;s:5:\\\"10669\\\";i:433;s:5:\\\"10674\\\";i:434;s:5:\\\"10697\\\";i:435;s:5:\\\"10704\\\";i:436;s:5:\\\"10708\\\";i:437;s:5:\\\"10713\\\";i:438;s:5:\\\"10717\\\";i:439;s:5:\\\"10718\\\";i:440;s:5:\\\"10720\\\";i:441;s:5:\\\"10722\\\";i:442;s:5:\\\"10723\\\";i:443;s:5:\\\"10724\\\";i:444;s:5:\\\"10726\\\";i:445;s:5:\\\"10730\\\";i:446;s:5:\\\"10735\\\";i:447;s:5:\\\"10736\\\";i:448;s:5:\\\"10737\\\";i:449;s:5:\\\"10738\\\";i:450;s:5:\\\"10739\\\";i:451;s:5:\\\"10740\\\";i:452;s:5:\\\"10743\\\";i:453;s:5:\\\"10746\\\";i:454;s:5:\\\"10747\\\";i:455;s:5:\\\"10750\\\";i:456;s:5:\\\"10752\\\";i:457;s:5:\\\"10757\\\";i:458;s:5:\\\"10758\\\";i:459;s:5:\\\"10759\\\";i:460;s:5:\\\"10760\\\";i:461;s:5:\\\"10762\\\";i:462;s:5:\\\"10764\\\";i:463;s:5:\\\"10770\\\";i:464;s:5:\\\"10789\\\";i:465;s:5:\\\"10855\\\";i:466;s:5:\\\"10925\\\";i:467;s:5:\\\"10944\\\";i:468;s:5:\\\"10077\\\";i:469;s:5:\\\"10115\\\";i:470;s:5:\\\"10236\\\";i:471;s:5:\\\"10540\\\";i:472;s:5:\\\"10612\\\";i:473;s:5:\\\"10669\\\";i:474;s:5:\\\"10674\\\";i:475;s:5:\\\"10697\\\";i:476;s:5:\\\"10704\\\";i:477;s:5:\\\"10708\\\";i:478;s:5:\\\"10713\\\";i:479;s:5:\\\"10717\\\";i:480;s:5:\\\"10718\\\";i:481;s:5:\\\"10720\\\";i:482;s:5:\\\"10722\\\";i:483;s:5:\\\"10723\\\";i:484;s:5:\\\"10724\\\";i:485;s:5:\\\"10726\\\";i:486;s:5:\\\"10730\\\";i:487;s:5:\\\"10735\\\";i:488;s:5:\\\"10736\\\";i:489;s:5:\\\"10737\\\";i:490;s:5:\\\"10738\\\";i:491;s:5:\\\"10739\\\";i:492;s:5:\\\"10740\\\";i:493;s:5:\\\"10743\\\";i:494;s:5:\\\"10746\\\";i:495;s:5:\\\"10747\\\";i:496;s:5:\\\"10750\\\";i:497;s:5:\\\"10752\\\";i:498;s:5:\\\"10757\\\";i:499;s:5:\\\"10758\\\";i:500;s:5:\\\"10759\\\";i:501;s:5:\\\"10760\\\";i:502;s:5:\\\"10762\\\";i:503;s:5:\\\"10764\\\";i:504;s:5:\\\"10770\\\";i:505;s:5:\\\"10789\\\";i:506;s:5:\\\"10077\\\";i:507;s:5:\\\"10115\\\";i:508;s:5:\\\"10236\\\";i:509;s:5:\\\"10540\\\";i:510;s:5:\\\"10612\\\";i:511;s:5:\\\"10669\\\";i:512;s:5:\\\"10674\\\";i:513;s:5:\\\"10697\\\";i:514;s:5:\\\"10704\\\";i:515;s:5:\\\"10708\\\";i:516;s:5:\\\"10713\\\";i:517;s:5:\\\"10717\\\";i:518;s:5:\\\"10718\\\";i:519;s:5:\\\"10720\\\";i:520;s:5:\\\"10722\\\";i:521;s:5:\\\"10723\\\";i:522;s:5:\\\"10724\\\";i:523;s:5:\\\"10726\\\";i:524;s:5:\\\"10730\\\";i:525;s:5:\\\"10735\\\";i:526;s:5:\\\"10736\\\";i:527;s:5:\\\"10737\\\";i:528;s:5:\\\"10738\\\";i:529;s:5:\\\"10739\\\";i:530;s:5:\\\"10740\\\";i:531;s:5:\\\"10743\\\";i:532;s:5:\\\"10746\\\";i:533;s:5:\\\"10747\\\";i:534;s:5:\\\"10750\\\";i:535;s:5:\\\"10752\\\";i:536;s:5:\\\"10757\\\";i:537;s:5:\\\"10758\\\";i:538;s:5:\\\"10759\\\";i:539;s:5:\\\"10760\\\";i:540;s:5:\\\"10762\\\";i:541;s:5:\\\"10764\\\";i:542;s:5:\\\"10770\\\";i:543;s:5:\\\"10789\\\";i:544;s:5:\\\"10855\\\";i:545;s:5:\\\"10925\\\";i:546;s:5:\\\"10944\\\";i:547;s:5:\\\"10077\\\";i:548;s:5:\\\"10115\\\";i:549;s:5:\\\"10236\\\";i:550;s:5:\\\"10077\\\";i:551;s:5:\\\"10077\\\";i:552;s:5:\\\"10115\\\";i:553;s:5:\\\"10077\\\";i:554;s:5:\\\"10115\\\";i:555;s:5:\\\"10115\\\";i:556;s:5:\\\"10236\\\";i:557;s:5:\\\"10236\\\";i:558;s:5:\\\"10236\\\";i:559;s:5:\\\"10540\\\";i:560;s:5:\\\"10612\\\";i:561;s:5:\\\"10669\\\";i:562;s:5:\\\"10674\\\";i:563;s:5:\\\"10697\\\";i:564;s:5:\\\"10704\\\";i:565;s:5:\\\"10708\\\";i:566;s:5:\\\"10540\\\";i:567;s:5:\\\"10713\\\";i:568;s:5:\\\"10717\\\";i:569;s:5:\\\"10718\\\";i:570;s:5:\\\"10720\\\";i:571;s:5:\\\"10722\\\";i:572;s:5:\\\"10723\\\";i:573;s:5:\\\"10724\\\";i:574;s:5:\\\"10726\\\";i:575;s:5:\\\"10540\\\";i:576;s:5:\\\"10730\\\";i:577;s:5:\\\"10735\\\";i:578;s:5:\\\"10736\\\";i:579;s:5:\\\"10737\\\";i:580;s:5:\\\"10738\\\";i:581;s:5:\\\"10540\\\";i:582;s:5:\\\"10739\\\";i:583;s:5:\\\"10740\\\";i:584;s:5:\\\"10743\\\";i:585;s:5:\\\"10746\\\";i:586;s:5:\\\"10747\\\";i:587;s:5:\\\"10750\\\";i:588;s:5:\\\"10752\\\";i:589;s:5:\\\"10077\\\";i:590;s:5:\\\"10115\\\";i:591;s:5:\\\"10236\\\";i:592;s:5:\\\"10540\\\";i:593;s:5:\\\"10612\\\";i:594;s:5:\\\"10669\\\";i:595;s:5:\\\"10674\\\";i:596;s:5:\\\"10697\\\";i:597;s:5:\\\"10704\\\";i:598;s:5:\\\"10708\\\";i:599;s:5:\\\"10713\\\";i:600;s:5:\\\"10717\\\";i:601;s:5:\\\"10718\\\";i:602;s:5:\\\"10720\\\";i:603;s:5:\\\"10722\\\";i:604;s:5:\\\"10723\\\";i:605;s:5:\\\"10724\\\";i:606;s:5:\\\"10726\\\";i:607;s:5:\\\"10730\\\";i:608;s:5:\\\"10735\\\";i:609;s:5:\\\"10736\\\";i:610;s:5:\\\"10737\\\";i:611;s:5:\\\"10738\\\";i:612;s:5:\\\"10739\\\";i:613;s:5:\\\"10740\\\";i:614;s:5:\\\"10743\\\";i:615;s:5:\\\"10746\\\";i:616;s:5:\\\"10747\\\";i:617;s:5:\\\"10750\\\";i:618;s:5:\\\"10752\\\";i:619;s:5:\\\"10757\\\";i:620;s:5:\\\"10758\\\";i:621;s:5:\\\"10759\\\";i:622;s:5:\\\"10760\\\";i:623;s:5:\\\"10762\\\";i:624;s:5:\\\"10764\\\";i:625;s:5:\\\"10770\\\";i:626;s:5:\\\"10789\\\";i:627;s:5:\\\"10855\\\";i:628;s:5:\\\"10925\\\";i:629;s:5:\\\"10944\\\";i:630;s:5:\\\"10077\\\";i:631;s:5:\\\"10115\\\";i:632;s:5:\\\"10236\\\";i:633;s:5:\\\"10540\\\";i:634;s:5:\\\"10612\\\";i:635;s:5:\\\"10669\\\";i:636;s:5:\\\"10674\\\";i:637;s:5:\\\"10697\\\";i:638;s:5:\\\"10704\\\";i:639;s:5:\\\"10708\\\";i:640;s:5:\\\"10713\\\";i:641;s:5:\\\"10717\\\";i:642;s:5:\\\"10718\\\";i:643;s:5:\\\"10720\\\";i:644;s:5:\\\"10722\\\";i:645;s:5:\\\"10723\\\";i:646;s:5:\\\"10724\\\";i:647;s:5:\\\"10726\\\";i:648;s:5:\\\"10730\\\";i:649;s:5:\\\"10735\\\";i:650;s:5:\\\"10736\\\";i:651;s:5:\\\"10737\\\";i:652;s:5:\\\"10738\\\";i:653;s:5:\\\"10739\\\";i:654;s:5:\\\"10740\\\";i:655;s:5:\\\"10743\\\";i:656;s:5:\\\"10746\\\";i:657;s:5:\\\"10747\\\";i:658;s:5:\\\"10750\\\";i:659;s:5:\\\"10752\\\";i:660;s:5:\\\"10757\\\";i:661;s:5:\\\"10758\\\";i:662;s:5:\\\"10759\\\";i:663;s:5:\\\"10760\\\";i:664;s:5:\\\"10762\\\";i:665;s:5:\\\"10764\\\";i:666;s:5:\\\"10770\\\";i:667;s:5:\\\"10789\\\";i:668;s:5:\\\"10855\\\";i:669;s:5:\\\"10925\\\";i:670;s:5:\\\"10944\\\";i:671;s:5:\\\"10077\\\";i:672;s:5:\\\"10115\\\";i:673;s:5:\\\"10236\\\";i:674;s:5:\\\"10077\\\";i:675;s:5:\\\"10115\\\";i:676;s:5:\\\"10236\\\";i:677;s:5:\\\"10540\\\";i:678;s:5:\\\"10612\\\";i:679;s:5:\\\"10669\\\";i:680;s:5:\\\"10674\\\";i:681;s:5:\\\"10697\\\";i:682;s:5:\\\"10704\\\";i:683;s:5:\\\"10708\\\";i:684;s:5:\\\"10713\\\";i:685;s:5:\\\"10717\\\";i:686;s:5:\\\"10718\\\";i:687;s:5:\\\"10720\\\";i:688;s:5:\\\"10722\\\";i:689;s:5:\\\"10723\\\";i:690;s:5:\\\"10724\\\";i:691;s:5:\\\"10726\\\";i:692;s:5:\\\"10730\\\";i:693;s:5:\\\"10735\\\";i:694;s:5:\\\"10736\\\";i:695;s:5:\\\"10737\\\";i:696;s:5:\\\"10738\\\";i:697;s:5:\\\"10739\\\";i:698;s:5:\\\"10740\\\";i:699;s:5:\\\"10743\\\";i:700;s:5:\\\"10746\\\";i:701;s:5:\\\"10747\\\";i:702;s:5:\\\"10750\\\";i:703;s:5:\\\"10752\\\";i:704;s:5:\\\"10757\\\";i:705;s:5:\\\"10758\\\";i:706;s:5:\\\"10759\\\";i:707;s:5:\\\"10760\\\";i:708;s:5:\\\"10762\\\";i:709;s:5:\\\"10764\\\";i:710;s:5:\\\"10770\\\";i:711;s:5:\\\"10789\\\";i:712;s:5:\\\"10855\\\";i:713;s:5:\\\"10925\\\";i:714;s:5:\\\"10944\\\";i:715;s:5:\\\"10077\\\";i:716;s:5:\\\"10115\\\";i:717;s:5:\\\"10236\\\";i:718;s:5:\\\"10540\\\";i:719;s:5:\\\"10612\\\";i:720;s:5:\\\"10669\\\";i:721;s:5:\\\"10674\\\";i:722;s:5:\\\"10697\\\";i:723;s:5:\\\"10704\\\";i:724;s:5:\\\"10708\\\";i:725;s:5:\\\"10713\\\";i:726;s:5:\\\"10717\\\";i:727;s:5:\\\"10718\\\";i:728;s:5:\\\"10720\\\";i:729;s:5:\\\"10722\\\";i:730;s:5:\\\"10723\\\";i:731;s:5:\\\"10724\\\";i:732;s:5:\\\"10726\\\";i:733;s:5:\\\"10730\\\";i:734;s:5:\\\"10735\\\";i:735;s:5:\\\"10736\\\";i:736;s:5:\\\"10737\\\";i:737;s:5:\\\"10738\\\";i:738;s:5:\\\"10739\\\";i:739;s:5:\\\"10740\\\";i:740;s:5:\\\"10743\\\";i:741;s:5:\\\"10746\\\";i:742;s:5:\\\"10747\\\";i:743;s:5:\\\"10750\\\";i:744;s:5:\\\"10752\\\";i:745;s:5:\\\"10757\\\";i:746;s:5:\\\"10758\\\";i:747;s:5:\\\"10759\\\";i:748;s:5:\\\"10760\\\";i:749;s:5:\\\"10762\\\";i:750;s:5:\\\"10764\\\";i:751;s:5:\\\"10770\\\";i:752;s:5:\\\"10789\\\";i:753;s:5:\\\"10855\\\";i:754;s:5:\\\"10925\\\";i:755;s:5:\\\"10944\\\";i:756;s:5:\\\"10077\\\";i:757;s:5:\\\"10115\\\";i:758;s:5:\\\"10236\\\";i:759;s:5:\\\"10540\\\";i:760;s:5:\\\"10612\\\";i:761;s:5:\\\"10669\\\";i:762;s:5:\\\"10674\\\";i:763;s:5:\\\"10697\\\";i:764;s:5:\\\"10704\\\";i:765;s:5:\\\"10708\\\";i:766;s:5:\\\"10713\\\";i:767;s:5:\\\"10717\\\";i:768;s:5:\\\"10718\\\";i:769;s:5:\\\"10720\\\";i:770;s:5:\\\"10722\\\";i:771;s:5:\\\"10723\\\";i:772;s:5:\\\"10724\\\";i:773;s:5:\\\"10726\\\";i:774;s:5:\\\"10730\\\";i:775;s:5:\\\"10735\\\";i:776;s:5:\\\"10736\\\";i:777;s:5:\\\"10737\\\";i:778;s:5:\\\"10738\\\";i:779;s:5:\\\"10739\\\";i:780;s:5:\\\"10740\\\";i:781;s:5:\\\"10743\\\";i:782;s:5:\\\"10746\\\";i:783;s:5:\\\"10747\\\";i:784;s:5:\\\"10750\\\";i:785;s:5:\\\"10752\\\";i:786;s:5:\\\"10757\\\";i:787;s:5:\\\"10758\\\";i:788;s:5:\\\"10759\\\";i:789;s:5:\\\"10760\\\";i:790;s:5:\\\"10762\\\";i:791;s:5:\\\"10764\\\";i:792;s:5:\\\"10770\\\";i:793;s:5:\\\"10789\\\";i:794;s:5:\\\"10855\\\";i:795;s:5:\\\"10925\\\";i:796;s:5:\\\"10944\\\";i:797;s:5:\\\"10077\\\";i:798;s:5:\\\"10115\\\";i:799;s:5:\\\"10236\\\";i:800;s:5:\\\"10540\\\";i:801;s:5:\\\"10612\\\";i:802;s:5:\\\"10669\\\";i:803;s:5:\\\"10674\\\";i:804;s:5:\\\"10697\\\";i:805;s:5:\\\"10704\\\";i:806;s:5:\\\"10708\\\";i:807;s:5:\\\"10713\\\";i:808;s:5:\\\"10717\\\";i:809;s:5:\\\"10718\\\";i:810;s:5:\\\"10720\\\";i:811;s:5:\\\"10722\\\";i:812;s:5:\\\"10723\\\";i:813;s:5:\\\"10724\\\";i:814;s:5:\\\"10726\\\";i:815;s:5:\\\"10730\\\";i:816;s:5:\\\"10735\\\";i:817;s:5:\\\"10736\\\";i:818;s:5:\\\"10737\\\";i:819;s:5:\\\"10738\\\";i:820;s:5:\\\"10739\\\";i:821;s:5:\\\"10740\\\";i:822;s:5:\\\"10743\\\";i:823;s:5:\\\"10746\\\";i:824;s:5:\\\"10747\\\";i:825;s:5:\\\"10750\\\";i:826;s:5:\\\"10752\\\";i:827;s:5:\\\"10757\\\";i:828;s:5:\\\"10758\\\";i:829;s:5:\\\"10759\\\";i:830;s:5:\\\"10760\\\";i:831;s:5:\\\"10762\\\";i:832;s:5:\\\"10764\\\";i:833;s:5:\\\"10770\\\";i:834;s:5:\\\"10789\\\";i:835;s:5:\\\"10855\\\";i:836;s:5:\\\"10925\\\";i:837;s:5:\\\"10944\\\";i:838;s:5:\\\"10077\\\";i:839;s:5:\\\"10115\\\";i:840;s:5:\\\"10236\\\";i:841;s:5:\\\"10540\\\";i:842;s:5:\\\"10612\\\";i:843;s:5:\\\"10669\\\";i:844;s:5:\\\"10674\\\";i:845;s:5:\\\"10697\\\";i:846;s:5:\\\"10704\\\";i:847;s:5:\\\"10708\\\";i:848;s:5:\\\"10713\\\";i:849;s:5:\\\"10717\\\";i:850;s:5:\\\"10718\\\";i:851;s:5:\\\"10720\\\";i:852;s:5:\\\"10722\\\";i:853;s:5:\\\"10723\\\";i:854;s:5:\\\"10724\\\";i:855;s:5:\\\"10726\\\";i:856;s:5:\\\"10730\\\";i:857;s:5:\\\"10735\\\";i:858;s:5:\\\"10736\\\";i:859;s:5:\\\"10737\\\";i:860;s:5:\\\"10738\\\";i:861;s:5:\\\"10739\\\";i:862;s:5:\\\"10740\\\";i:863;s:5:\\\"10743\\\";i:864;s:5:\\\"10746\\\";i:865;s:5:\\\"10747\\\";i:866;s:5:\\\"10750\\\";i:867;s:5:\\\"10752\\\";i:868;s:5:\\\"10757\\\";i:869;s:5:\\\"10758\\\";i:870;s:5:\\\"10759\\\";i:871;s:5:\\\"10760\\\";i:872;s:5:\\\"10762\\\";i:873;s:5:\\\"10764\\\";i:874;s:5:\\\"10770\\\";i:875;s:5:\\\"10789\\\";i:876;s:5:\\\"10855\\\";i:877;s:5:\\\"10925\\\";i:878;s:5:\\\"10944\\\";i:879;s:5:\\\"10077\\\";i:880;s:5:\\\"10115\\\";i:881;s:5:\\\"10236\\\";i:882;s:5:\\\"10540\\\";}\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/gerente/filter?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Gerente.php",
    "groupTitle": "gerente"
  },
  {
    "type": "post",
    "url": "/condominio/gerente/update",
    "title": "Gerente - Atualizar",
    "name": "gerente_update",
    "group": "gerente",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do Administrador. Somente Administrador pode cadastrar gerentes</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nome",
            "description": "<p>Nome</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "endereco",
            "description": "<p>Endereço</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacao",
            "description": "<p>Identificação (Exemplo: 0250)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "complemento",
            "description": "<p>Complemento</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "bairro",
            "description": "<p>Bairro</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "municipio",
            "description": "<p>Município</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "estado",
            "description": "<p>Estado</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cep",
            "description": "<p>CEP</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tipo_endereco",
            "description": "<p>Tipo do Endereço (Exemplo: Comercial ou Residencial)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>E-mail do Gerente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefone_residencial",
            "description": "<p>Telefone Residencial</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefone_comercial",
            "description": "<p>Telefone Comercial</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefone_movel",
            "description": "<p>Telefone Móvel</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fax",
            "description": "<p>Fax</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "login_gerente",
            "description": "<p>Login do Gerente</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "senha",
            "description": "<p>Senha do Gerente</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "clientes",
            "description": "<p>Clientes (Array de IDs de Condomínios)</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "principal",
            "description": "<p>Gerente Principal? 0 = Não. 1 = Sim.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"O Gerente foi atualizado.\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/gerente/update?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Gerente.php",
    "groupTitle": "gerente"
  },
  {
    "type": "post",
    "url": "/condominio/mensagem/create",
    "title": "Mensagem - Criar",
    "name": "criar_mensagem",
    "group": "mensagem",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "destinatario",
            "description": "<p>Destinatário</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nome",
            "description": "<p>Nome Completo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>E-mail</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assunto",
            "description": "<p>Assunto</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mensagem",
            "description": "<p>Mensagem</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "link_arquivo",
            "description": "<p>Arquivo (File só são aceitos nos formatos JPG ou PNG)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "mensagem_id",
            "description": "<p>ID da mensagem principal, criando um relacionamento 1:1.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "lida",
            "description": "<p>Lida (Opcional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"A Mensagem foi publicada.\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/mensagem/create?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Mensagem.php",
    "groupTitle": "mensagem"
  },
  {
    "type": "post",
    "url": "/condominio/mensagem/delete",
    "title": "Mensagem - Deletar",
    "name": "deletar_mensagem",
    "group": "mensagem",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ID",
            "description": "<p>da Mensagem</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "all",
            "description": "<p>Deseja deletar todas as mensagens? 0 = Não, 1 = Sim.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "force",
            "description": "<p>Forçar deletar a mensagem? 0 = Não, 1 = Sim. O Não é o Padrão.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"error\":\"0\",\"status\":\"A mensagem $id foi deletada.\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/mensagem/delete?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Mensagem.php",
    "groupTitle": "mensagem"
  },
  {
    "type": "get",
    "url": "/condominio/mensagem",
    "title": "Mensagem",
    "name": "mensagem",
    "group": "mensagem",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do Remetente (Obrigatório)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "destinatario",
            "description": "<p>Login do Destinatário (Obrigatório)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID da Mensagem (opcional)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "deleted",
            "description": "<p>Status da Mensagem</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n[{\"id\":\"325\",\"titulo\":\"Teste Titulo\",\"data_inclusao\":\"26\\/07\\/2018\",\"data_expiracao\":\"10\\/10\\/2018\",\"visualizacao\":\"todos\",\"mensagem\":\"Esse \\u00c3\\u00a9 um teste somente...\",\"condominio\":\"10001\",\"condomino\":\"\"},{\"id\":\"326\",\"titulo\":\"Teste Titulo\",\"data_inclusao\":\"26\\/07\\/2018\",\"data_expiracao\":\"10\\/10\\/2018\",\"visualizacao\":\"\",\"mensagem\":\"Esse \\u00c3\\u00a9 um teste somente...\",\"condominio\":\"10001\",\"condomino\":\"\"},{\"id\":\"335\",\"titulo\":\"NOVO TITULO 100\",\"data_inclusao\":\"27\\/07\\/2018\",\"data_expiracao\":\"10\\/10\\/2018\",\"visualizacao\":\"todos\",\"mensagem\":\"MENSAGEM NOVO\",\"condominio\":\"10001\",\"condomino\":\"\"}]",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/condominio/mensagem?destinatario=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/condominio/Mensagem.php",
    "groupTitle": "mensagem"
  },
  {
    "type": "post",
    "url": "/swap",
    "title": "Swap - Troca de Usuários temporária",
    "name": "swap",
    "group": "swap",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "login_internet",
            "description": "<p>Login do condomínio</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Status do Switch (Precisa ser 'S' se NÃO QUISER RETORNAR COMO ADMINISTRADOR ou 'B' - BACK PARA RETORNAR COMO ADMINISTRADOR)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_TOKEN</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response:",
          "content": "HTTP/1.1 200 OK\n{\"success\":\"1\",\"codigo\":\"00001\",\"condominio\":\"DONA NINA\",\"gerente\":\"Cl\\u00c3\\u00a1udio Bittencourt\",\"telefone_gerente\":\"3543-2908\",\"ramal_gerente\":\"\",\"email_gerente\":\"claudio.bittencourt@bcfadm.com.br\",\"sindico\":\"ANA CHRISTINA ADRI\\u00c3\\u0083O RODRIGUES CARVALHO\",\"condomino\":\"\",\"token\":\"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjAwMDAxIiwibG9naW4iOiIxMDAwMSJ9.VNg4MR8RuQcyuGZex_zD5OqjyPDSAW1odfnwTkQeGY8\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.1.0",
    "examples": [
      {
        "title": "Usage:",
        "content": "curl -i http://condolog.com.br/bcfnet/api/swap?login=10001&token=$YOUR_JWT_TOKEN",
        "type": "curl"
      }
    ],
    "filename": "application/controllers/Swap.php",
    "groupTitle": "swap"
  }
] });
