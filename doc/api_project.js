define({
  "name": "BCFNet API Documentation",
  "version": "0.1.0",
  "description": "Documentação para API utilizada no sistema BCFNet, da BCF Administradora de Bens.",
  "title": "BCFNet API Documentation",
  "url": "http://condolog.com.br/bcfnet/api",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-08-10T16:27:00.338Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
